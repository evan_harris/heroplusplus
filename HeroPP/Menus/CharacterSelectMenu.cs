﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class CharacterSelectMenu : Menu
    {
        // number of players selected in the main menu
        private int mNumPlayers;

        // array of players information
        private PlayerStatus[] mPlayers;
        private HeroOption[] mHeroes;

        // information on each player
        private struct PlayerStatus
        {
            public int PlayerChoice;
            public Color PlayerColor;
            public int Highlighted;
            public bool PlayerLocked;
            public bool ErrorDisplayed;
        }

        private struct HeroOption
        {
            public Color HeroColor;
            public bool Selected;
        }

        private string[] TEXTIMG = { "pos_RN_standing", "neg_RN_standing", "pos_RN_standing", "neg_RN_standing" };//"sung_RN_standing", "zander_RN" };
        private string[] PLAYERNUMIMG = { "player1", "player2", "player3", "player4" };
        private string READYIMG = "ready";
        private string NOTREADYIMG = "notready";
        private string ERRORBOXIMG = "selectionerror";
        private string SHADEIMG = "shadebutton";
        private string SELECTBKIMG = "heroselectbackground";
        private string SCROLLARROWUPIMG = "ArrowUp";
        private string SCROLLARROWDOWNIMG = "ArrowDown";

        private XNACS1Rectangle[] mShades;
        private XNACS1Rectangle[] mErrorBoxes;
        private XNACS1Rectangle[] mScrollArrows;
        private XNACS1Rectangle[] mReady;
        private XNACS1Rectangle[] mPlayerNum;

        public CharacterSelectMenu(GameController[] controls)
            : base(controls)
        {
        }

        public override void InitializeMenu()
        {


            // initialize background off world dimensions
            base.InitializeMenu();
            // and then adjust from defaults
            mBackground.Width *= 0.88f;
            mBackground.Height *= 0.88f;
            mBackground.Visible = false;
            mMenuWidth = mBackground.Width;
            mMenuHeight = mBackground.Height;
            mMenuCenterPos = mBackground.Center;
            mBackground.Texture = SELECTBKIMG;
            mBackground.AddToAutoDrawSet();
            mBackground.Visible = true;

            // create all the member arrays
            mReady = new XNACS1Rectangle[4];
            mPlayerNum = new XNACS1Rectangle[4];
            mPlayers = new PlayerStatus[4];
            mHeroes = new HeroOption[4];
            mErrorBoxes = new XNACS1Rectangle[4];
            mShades = new XNACS1Rectangle[4];
            mButtons = new MenuButton[4];
            mScrollArrows = new XNACS1Rectangle[8];

            // to start with all of the players are locked
           
            for (int i = 0; i < mButtons.Length; i++)
            {
                // set the hero status
                mHeroes[i].Selected = false;

                // make all the buttons and overlays
                mButtons[i] = new MenuButton();
                mErrorBoxes[i] = new XNACS1Rectangle();
                mShades[i] = new XNACS1Rectangle();
                mReady[i] = new XNACS1Rectangle();
                mPlayerNum[i] = new XNACS1Rectangle();

                // set their dimensions
                mErrorBoxes[i].Width = mShades[i].Width = mButtons[i].Width = mMenuWidth / 9f;
                mErrorBoxes[i].Height = mShades[i].Height = mButtons[i].Height = mButtons[i].Width * 1.29f;
                // set initial textures

                mButtons[i].Texture = TEXTIMG[i];
                mErrorBoxes[i].Texture = ERRORBOXIMG;

                mShades[i].Texture = SHADEIMG;

                // set all the shades to invisible to start with
                // let show() figure out which ones should be shown
                mShades[i].Visible = false;
            }

            // set the hero colors
            mHeroes[0].HeroColor = Color.Red;
            mHeroes[1].HeroColor = Color.Blue;
            mHeroes[2].HeroColor = Color.Green;
            mHeroes[3].HeroColor = Color.Yellow;


            // get a default choice, they all highlight a different hero
            for (int i = 0; i < mPlayers.Length; i++)
            {
                mPlayers[i].PlayerLocked = false;
                mPlayers[i].PlayerChoice = i; // no choice
                mPlayers[i].Highlighted = i; // highlighting a default
                mPlayers[i].ErrorDisplayed = false;
                mPlayers[i].PlayerColor = mHeroes[i].HeroColor;
            }



            // make the buttons and space them evenly
            mErrorBoxes[0].Center = mShades[0].Center = mButtons[0].Center = new Vector2(mMenuCenterPos.X - ((mMenuWidth / 5) * 1.5f), mMenuCenterPos.Y);
            mErrorBoxes[1].Center = mShades[1].Center = mButtons[1].Center = new Vector2(mMenuCenterPos.X - ((mMenuWidth / 5) * 0.5f), mMenuCenterPos.Y);
            mErrorBoxes[2].Center = mShades[2].Center = mButtons[2].Center = new Vector2(mMenuCenterPos.X + ((mMenuWidth / 5) * 0.5f), mMenuCenterPos.Y);
            mErrorBoxes[3].Center = mShades[3].Center = mButtons[3].Center = new Vector2(mMenuCenterPos.X + ((mMenuWidth / 5) * 1.5f), mMenuCenterPos.Y);

            ////////////////////////////////// Quick fix on errorbox position and color ////////////////////////////////
            for (int i = 0; i < mErrorBoxes.Length; i++)
                mErrorBoxes[i].CenterY -= mErrorBoxes[i].Height * 0.25f;


            for (int i = 0; i < mScrollArrows.Length; i++)
            {
                mScrollArrows[i] = new XNACS1Rectangle();
                if (i < mPlayers.Length)
                {
                    mScrollArrows[i].Center = mButtons[i].Center;
                    mScrollArrows[i].Height = mScrollArrows[i].Width = mButtons[i].Width / 4f;
                    mScrollArrows[i].CenterY += mButtons[i].Height * 0.66f;
                    mScrollArrows[i].Texture = SCROLLARROWUPIMG;
                }
                else
                {
                    // default dimensions positions of scrollarrows, ready, and playernum off of the buttons
                    mScrollArrows[i].Center = mPlayerNum[i - 4].Center = mReady[i - 4].Center = mButtons[i - 4].Center;
                    mScrollArrows[i].Height = mPlayerNum[i - 4].Height = mReady[i - 4].Height = mScrollArrows[i].Width = mPlayerNum[i - 4].Width = mReady[i - 4].Width = mButtons[i - 4].Width / 4f;
                    mScrollArrows[i].CenterY -= mButtons[i - 4].Height * 0.66f;

                    // adjust center of player numbers / ready indicator based on the button heights
                    mPlayerNum[i - 4].CenterY -= mButtons[i - 4].Height * 0.9f;
                    mReady[i - 4].CenterY -= mButtons[i - 4].Height * 1.5f;

                    // set textures
                    mScrollArrows[i].Texture = SCROLLARROWDOWNIMG;
                    mReady[i - 4].Texture = NOTREADYIMG;
                    mPlayerNum[i - 4].Texture = PLAYERNUMIMG[i - 4];
                    
                    // adjust ready status / player number width to match the button width
                    mReady[i - 4].Width = mButtons[i - 4].Width;
                    mPlayerNum[i - 4].Width = mButtons[i - 4].Width;

                    // give them twice the height of the arrows
                    mReady[i - 4].Height = mScrollArrows[i].Height * 2f;
                    mPlayerNum[i - 4].Height = mScrollArrows[i].Height * 2f;
                }
            }

            // Hide everything after it is initialized
            Hide();
        }

        protected override void Hide()
        {
            base.Hide();
            foreach (XNACS1Rectangle r in mScrollArrows)
                r.RemoveFromAutoDrawSet();
            for (int i = 0; i < mShades.Length; i++)
            {
                mErrorBoxes[i].RemoveFromAutoDrawSet();
                mShades[i].RemoveFromAutoDrawSet();
                mReady[i].RemoveFromAutoDrawSet();
                mPlayerNum[i].RemoveFromAutoDrawSet();
            }
        }

        private void Show()
        {
            mBackground.AddToAutoDrawSet();
            // show only the characters currently associated with each player
            for (int i = 0; i < mButtons.Length; i++)
            {
                mButtons[i].AddToAutoDrawSet();
                mShades[i].AddToAutoDrawSet();
                mReady[i].AddToAutoDrawSet();
                mPlayerNum[i].AddToAutoDrawSet();
                if (i > mNumPlayers - 1)
                {
                    mShades[i].Visible = false;
                    //mPlayerNum[i].Visible = mReady[i].Visible = false;
                    mButtons[i].TextureTintColor = Color.Black;
                }
                else
                {
                    mShades[i].Visible = false;
                    mPlayerNum[i].Visible = mReady[i].Visible = true;

                }
            }
            foreach (XNACS1Rectangle r in mScrollArrows)
                r.AddToAutoDrawSet();
        }

        public void StartMenu(int players)
        {
            mNumPlayers = players;
            for (int i = 0; i < mPlayers.Length; i++)
            {
                // if a selection box is unavailable it will be set to true
                // in order to make the update menu return the correct condition
                mPlayers[i].PlayerLocked = !(i < mNumPlayers);
                // choice coincides with the current hero displayed
                mPlayers[i].PlayerChoice = i;
                mPlayers[i].Highlighted = i;
                mPlayers[i].PlayerColor = mHeroes[i].HeroColor;
                mButtons[i].Texture = TEXTIMG[i];
                mReady[i].Texture = NOTREADYIMG;
                mPlayers[i].ErrorDisplayed = false;
                mHeroes[i].Selected = false;
            }
            // uses the sung and zander in player 3 and 4 slot (quick fix, should be blacked out anyway)
            //////////////////////////////////////////////////////////////////////////////////////////
            mButtons[2].Texture = "sung_RN_standing";
            mButtons[3].Texture = "zander_RN";
            mReady[2].Visible = false;
            mReady[3].Visible = false;
            //////////////////////////////////////////////////////////////////////////////////////////
            Show();
        }

        public override bool Update()
        {
            base.Update();

            // update players selection box and state based on input
            UpdatePlayers();

            // check for all locked status to return to main menu
            bool allLocked = true;
            for (int i = 0; i < mPlayers.Length; i++)
                if (!mPlayers[i].PlayerLocked)
                    allLocked = false;

            if (allLocked)
                Hide();

            return allLocked;
        }

        private void UpdatePlayers()
        {
            for (int i = 0; i < mNumPlayers; i++)
            {
                if (!mPlayers[i].PlayerLocked)
                {
                    if (mControls[i].LeftDir.Y < -0.25f && !mPlayers[i].ErrorDisplayed)
                        MoveDown(i);
                    if (mControls[i].LeftDir.Y > 0.25f && !mPlayers[i].ErrorDisplayed)
                        MoveUp(i);
                    if (mControls[i].mAPress)
                        Select(i);
                }
            }
        }

        private void MoveDown(int i)
        {
            if (mSelectionMoveCounter > mSelectionDelay)
            {
                mSelectionMoveCounter = 0;
                // rotate selectionbox status for player
                if (mPlayers[i].Highlighted == mPlayers.Length - 1)
                    mPlayers[i].Highlighted = 0;
                else
                    mPlayers[i].Highlighted++;

                // adjust the texture after the change
                mButtons[i].Texture = TEXTIMG[mPlayers[i].Highlighted];
            }
        }

        private void MoveUp(int i)
        {
            if (mSelectionMoveCounter > mSelectionDelay)
            {
                mSelectionMoveCounter = 0;
                // rotate selectionbox status for player
                if (mPlayers[i].Highlighted == 0)
                {
                    mPlayers[i].Highlighted = mPlayers.Length - 1;
                }
                else
                    mPlayers[i].Highlighted--;

                // change the texture to match the new hero
                mButtons[i].Texture = TEXTIMG[mPlayers[i].Highlighted];
            }
        }

        // player selects a hero (but they can't select one thats already selected)
        private void Select(int i)
        {
            if (mSelectionMoveCounter > mSelectionDelay)
            {
                mSelectionMoveCounter = 0;

                if (mPlayers[i].ErrorDisplayed)
                {
                    mPlayers[i].ErrorDisplayed = false;
                    mErrorBoxes[i].RemoveFromAutoDrawSet();
                    return;
                }

                // if the highlighted hero has been selected
                //if (!mHeroes[mPlayers[i].Highlighted].Selected)
                //{
                //    mPlayers[i].PlayerChoice = mPlayers[i].Highlighted;
                //    mPlayers[i].PlayerColor = mHeroes[mPlayers[i].Highlighted].HeroColor;
                //    mPlayers[i].PlayerLocked = true;
                //    mHeroes[mPlayers[i].PlayerChoice].Selected = true;
                //    mReady[i].Texture = READYIMG;
                //    mShades[i].TextureTintColor = Color.Red;
                //}

                // The following switch was implemented to quickly solve the issue of selecting the same character
                switch (mPlayers[i].Highlighted)
                {
                    case 0:
                        if (mHeroes[0].Selected || mHeroes[2].Selected)
                            DisplayError(i);
                        else
                        {
                            mPlayers[i].PlayerChoice = mPlayers[i].Highlighted;
                            mPlayers[i].PlayerColor = mHeroes[mPlayers[i].Highlighted].HeroColor;
                            mPlayers[i].PlayerLocked = true;
                            mHeroes[mPlayers[i].PlayerChoice].Selected = true;
                            mReady[i].Texture = READYIMG;
                            mShades[i].TextureTintColor = Color.Red;
                        }
                        break;
                    case 1:
                        if (mHeroes[1].Selected || mHeroes[3].Selected)
                            DisplayError(i);
                        else
                        {
                            mPlayers[i].PlayerChoice = mPlayers[i].Highlighted;
                            mPlayers[i].PlayerColor = mHeroes[mPlayers[i].Highlighted].HeroColor;
                            mPlayers[i].PlayerLocked = true;
                            mHeroes[mPlayers[i].PlayerChoice].Selected = true;
                            mReady[i].Texture = READYIMG;
                            mShades[i].TextureTintColor = Color.Red;
                        }
                        break;
                    case 2:
                        if (mHeroes[2].Selected || mHeroes[0].Selected)
                            DisplayError(i);
                        else
                        {
                            mPlayers[i].PlayerChoice = mPlayers[i].Highlighted;
                            mPlayers[i].PlayerColor = mHeroes[mPlayers[i].Highlighted].HeroColor;
                            mPlayers[i].PlayerLocked = true;
                            mHeroes[mPlayers[i].PlayerChoice].Selected = true;
                            mReady[i].Texture = READYIMG;
                            mShades[i].TextureTintColor = Color.Red;
                        }
                        break;
                    case 3:
                        if (mHeroes[3].Selected || mHeroes[1].Selected)
                            DisplayError(i);
                        else
                        {
                            mPlayers[i].PlayerChoice = mPlayers[i].Highlighted;
                            mPlayers[i].PlayerColor = mHeroes[mPlayers[i].Highlighted].HeroColor;
                            mPlayers[i].PlayerLocked = true;
                            mHeroes[mPlayers[i].PlayerChoice].Selected = true;
                            mReady[i].Texture = READYIMG;
                            mShades[i].TextureTintColor = Color.Red;
                        }
                        break;
                }

            }
        }

        private void DisplayError(int i)
        {
            mPlayers[i].ErrorDisplayed = true;
            mErrorBoxes[i].AddToAutoDrawSet();
        }

        public Color GetPlayerColor(int PlayerNumber)
        {
            return mPlayers[PlayerNumber -1].PlayerColor;
        }
    }
}
