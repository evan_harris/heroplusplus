﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class PauseMenu : Menu
    {
        // reference to the controller object given to constructor
        GameController control;
        // states for the menu
        public PauseState m_PauseState; // tells update what to do
        public MenuState m_MenuState { get; private set; } // tracks which menu we are in
        private int m_HighlightedColumn;
        private int m_MoveCounter;
        private const int m_Delay = 7;

        Vector2 m_MenuCenterPos;
        Vector2[] buttonCenter;
        float m_MenuWidth;
        float m_MenuHeight;

        // parts to draw a menu
        XNACS1Rectangle m_Background;
        XNACS1Rectangle[] m_Button;
        XNACS1Rectangle[] m_ShadeOver;
        XNACS1Rectangle[] m_Text;

        // temporary consts... needs to move to definition class
        const string BACKGROUND_IMG = "psback";
        const string BUTTON_IMG = "button";
        const string SHADEOVER_IMG = "shadebutton";
        string[] TEXT_IMG = { "return", "stats", "exit", "reset" };

        /// <summary>
        /// Tracks the state of the menu logic based on what is happening with the game
        /// </summary>
        public enum PauseState
        {
            Paused = 0,
            UnPaused = 1,
            Resetting = 2,
            Exiting = 3
        };

        /// <summary>
        /// Tracks the current menu in order to know what is being manipulated
        /// </summary>
        public enum MenuState
        {
            FrontMenu,
            StatsMenu
        };

        /// <summary>
        /// Builds all of the parts that will be drawn and initialize class members
        /// </summary>
        /// <param name="center"></param>
        public PauseMenu() : base(new GameController[] {null})
        {
        }
        public void setController(GameController con)
        {
            control = con;
        }

        public void InitializeMenu(Vector2 center)
        {
            // initialize states
            m_PauseState = PauseState.UnPaused;
            m_MenuState = MenuState.FrontMenu;
            m_HighlightedColumn = 0;
            m_MoveCounter = 0;

            // set up a position offset for all the parts based on the menu center
            m_MenuCenterPos = center;

            m_Button = new XNACS1Rectangle[4];
            m_ShadeOver = new XNACS1Rectangle[4];
            m_Text = new XNACS1Rectangle[4];

            // define menu dimensions from world dimensions
            m_MenuWidth = XNACS1Base.World.WorldDimension.X * 0.5f;
            m_MenuHeight = XNACS1Base.World.WorldDimension.Y * 0.5f;

            // map buttons evenly by spacing buttons/shading/text within the menu bounds
            buttonCenter = new Vector2[4];
            buttonCenter[0] = new Vector2(m_MenuCenterPos.X, m_MenuCenterPos.Y + m_MenuHeight * 0.33f);
            buttonCenter[1] = new Vector2(m_MenuCenterPos.X, m_MenuCenterPos.Y + m_MenuHeight * 0.11f);
            buttonCenter[2] = new Vector2(m_MenuCenterPos.X, m_MenuCenterPos.Y - m_MenuHeight * 0.11f);
            buttonCenter[3] = new Vector2(m_MenuCenterPos.X, m_MenuCenterPos.Y - m_MenuHeight * 0.33f);

            // base button/shade/text widths/heights on the dimensions of the window
            float buttonWidth = m_MenuWidth * 0.55f;
            float buttonHeight = m_MenuHeight * 0.22f;

            // create menu, buttons, text etc.
            m_Background = new XNACS1Rectangle(m_MenuCenterPos, m_MenuWidth, m_MenuHeight, BACKGROUND_IMG);
            for (int i = 0; i < m_Button.Length; i++)
            {
                // create buttons mapping to calculated spacing
                m_Button[i] = new XNACS1Rectangle(buttonCenter[i], buttonWidth, buttonHeight, BUTTON_IMG);
                // map text over buttons
                m_Text[i] = new XNACS1Rectangle(buttonCenter[i], buttonWidth, buttonHeight, TEXT_IMG[i]);
                // shade buttons
                m_ShadeOver[i] = new XNACS1Rectangle(buttonCenter[i], buttonWidth, buttonHeight, SHADEOVER_IMG);
                // except the first one (highlighted one)
                if (i == m_HighlightedColumn)
                    m_ShadeOver[i].Visible = false;
            }

            HidePauseMenu();
        }

        /// <summary>
        /// Tells the calling class if the game is Paused or not. 
        /// If the game is exiting or restarting the game is not paused by definition.
        /// </summary>
        /// <returns></returns>
        public bool IsPaused()
        {
            return this.m_PauseState == PauseState.Paused;
        }

        /// <summary>
        /// Updates the menu based on user control, given states
        /// </summary>
        public override bool Update()
        {
            m_MoveCounter++;
            switch (m_PauseState)
            {
                case PauseState.Paused:
                    if (m_MenuState == MenuState.FrontMenu)
                    {
                        Move();
                        Select();
                    }
                    break;
                case PauseState.Exiting:
                case PauseState.UnPaused:
                    HidePauseMenu();
                    return true;
                default:
                    return false;
            }
            return false;
        }

        /// <summary>
        /// Handles img updating in terms of shading and highlighting
        /// Also handles the tracker for which button will be pushed
        /// </summary>
        private void Move()
        {
            // move down
            if (control.LeftDir.Y < -0.1f)
            {
                if (m_MoveCounter > m_Delay)
                {
                    m_ShadeOver[m_HighlightedColumn].Visible = true;
                    // wrap around
                    if (m_HighlightedColumn == m_Button.Length - 1)
                        m_HighlightedColumn = 0;
                    // or just move down
                    else
                        m_HighlightedColumn++;

                    m_ShadeOver[m_HighlightedColumn].Visible = false;
                    m_MoveCounter = 0;
                }
            }

            // move up
            if (control.LeftDir.Y > 0.1f)
            {
                if (m_MoveCounter > m_Delay)
                {
                    m_ShadeOver[m_HighlightedColumn].Visible = true;
                    // wrap around
                    if (m_HighlightedColumn == 0)
                        m_HighlightedColumn = m_Button.Length - 1;
                    // or just move up
                    else
                        m_HighlightedColumn--;

                    m_ShadeOver[m_HighlightedColumn].Visible = false;
                    m_MoveCounter = 0;
                }
            }
        }

        // handles transition of states based on pressing the A and B buttons within the Pause Menu
        private void Select()
        {
            if (control.mAPress)
                switch (m_HighlightedColumn)
                {
                    case 0:
                        ResumeGame();
                        break;
                    case 1:
                        ResumeGame();
                        //m_MenuState = MenuState.StatsMenu;
                        break;
                    case 2:
                        ResumeGame();
                        m_PauseState = PauseState.Exiting;
                        break;
                    case 3:
                        ResumeGame();
                        m_PauseState = PauseState.Resetting;
                        break;
                }
            // this isn't an else if, because I'd rather the game not
            // reset/exit if a mad flurry of buttons were pushed
            if (control.mBPress)
                ResumeGame();
            if (control.mBackPress)
                ResumeGame();
        }
        /// <summary>
        /// Reinitializes the Pause Menu, Shows it, and sets the state to Paused.
        /// </summary>

        public void PauseGame()
        {
            if (IsPaused())
                return;
            ShowPauseMenu();
            m_PauseState = PauseState.Paused;
            m_MenuState = MenuState.FrontMenu;
            m_HighlightedColumn = 0;
            m_ShadeOver[0].Visible = false;
        }

        public void ResumeGame()
        {
            HidePauseMenu();
            m_PauseState = PauseState.UnPaused;
            m_MenuState = MenuState.FrontMenu;
        }

        /// <summary>
        /// Remove the menu from the AutoDrawSet piece by piece
        /// </summary>
        private void HidePauseMenu()
        {
            for (int i = m_Button.Length - 1; i >= 0; i--)
            {
                m_ShadeOver[i].RemoveFromAutoDrawSet();
                m_Text[i].RemoveFromAutoDrawSet();
                m_Button[i].RemoveFromAutoDrawSet();
            }
            m_Background.RemoveFromAutoDrawSet();
            m_ShadeOver[m_HighlightedColumn].Visible = true;
            m_HighlightedColumn = 0;
        }

        /// <summary>
        ///Adds the menu to the AutoDrawSet piece by piece
        /// </summary>
        private void ShowPauseMenu()
        {
            m_Background.AddToAutoDrawSet();
            for (int i = 0; i < m_Button.Length; i++)
            {
                m_Button[i].AddToAutoDrawSet();
                m_Text[i].AddToAutoDrawSet();
                m_ShadeOver[i].AddToAutoDrawSet();
            }

        }
    }
}
