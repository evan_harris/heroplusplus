﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class MainMenu : Menu
    {
        #region states of the menu
        public enum Selection { menuStart = 0, menuMain = 1, menuChar = 2, exitGame };
        public Selection mState { get; private set; }
        #endregion

        // background images
        MenuPane mBackgroundImg, mBlackGround, mHeroBanner, mHeroPane;
        int mPlayerNumber, mCounter;

        SubMenuStart mStartMenu;
        SubMenuMain mMainMenu;
        SubMenuCharacterSelection mCharMenu;

        int mUnlock = 2;

        public MainMenu(GameController[] con1)
            : base(con1)
        {
            
        }

        public override void InitializeMenu()
        {

            XNACS1Base.PlayBackgroundAudio("menu_bg", 0.25f);

            mState = Selection.menuStart;
            mPlayerNumber = 2;      // set the amount of controllers
            mCounter = 8;           // set to removal counter

            #region background
            #region background image
            float imageWidth = XNACS1Lib.XNACS1Base.World.WorldDimension.X / XNACS1Lib.XNACS1Base.World.WorldDimension.Y * 65;
            float imageHeight = imageWidth * 800 / 1000;
            mBackgroundImg = new MenuPane(
                imageWidth / 2,
                HeroPP.World.WorldDimension.Y * 0.5f - (HeroPP.World.WorldDimension.Y * 0.5f - imageHeight / 2),
                imageWidth,
                imageHeight,
                "main_menu");
            mBlackGround = new MenuPane(
                imageWidth / 2,
                HeroPP.World.WorldDimension.Y * 0.5f - (HeroPP.World.WorldDimension.Y * 0.5f - imageHeight / 2),
                imageWidth,
                imageHeight,
                "");
            mBlackGround.Color = Color.Black;
            mBlackGround.SetAlpha(0.8f);
            mHeroBanner = new MenuPane(
                imageWidth * 0.25f,
                imageHeight * 0.5f,
                imageWidth * 0.4f,
                imageHeight * 0.13f,
                "heroPP");
            #endregion
            #region hero image over the background
            float imageHeroWidth = imageWidth * 0.65f;
            float imageHeroHeight = imageHeight * 0.45f;
            mHeroPane = new MenuPane(
                (imageWidth - 0.5f * imageHeroWidth),
                imageHeroHeight * 0.5f - 0.5f,
                imageHeroWidth,
                imageHeroHeight,
                "hero_pane");
            #endregion
            #endregion

            // this is the white background

            float menuXPos = HeroPP.World.WorldDimension.X * 0.25f;
            float menuYPos = HeroPP.World.WorldDimension.Y * 0.35f;
            float menuWidth = 125 * 0.2f;
            float menuHeight = 25 * 0.2f;
            mStartMenu = new SubMenuStart(
                menuXPos, menuYPos,
                menuWidth, menuHeight,
                "pressStart", mControls, mPlayerNumber);

            float whitePaneWidth = HeroPP.World.WorldDimension.X * 0.36f;
            float whitePaneHeight = HeroPP.World.WorldDimension.Y * 0.45f;
            float whitePaneXPos = HeroPP.World.WorldDimension.X * 0.235f;
            float whitePaneYPos = HeroPP.World.WorldDimension.Y * 0.35f;
            mMainMenu = new SubMenuMain(
                whitePaneXPos, whitePaneYPos,
                whitePaneWidth, whitePaneHeight,
                "chalkboard", mControls, mPlayerNumber);

            mCharMenu = new SubMenuCharacterSelection(
                whitePaneXPos, whitePaneYPos,
                whitePaneWidth, whitePaneHeight,
                "chalkboard", mControls, mPlayerNumber);
         //   mCharMenu.SetUnlockalbe(mUnlock);
            mCharMenu.SetUnlockalbe(3);
            BringToTop();
            
        }

        public override bool Update()
        {
            base.Update();
            switch (mState)
            {
                case Selection.menuStart:
                    mStartMenu.Update();
                    if (!mStartMenu.IsVisible())
                    {
                        mState = Selection.menuMain;
                        mMainMenu.Update();
                    }
                    break;
                case Selection.menuMain:
                    mMainMenu.Update();
                    #region check the button states
                    if (mMainMenu.IsStart())
                    {
                        return true;
                    }
                    if (mMainMenu.IsCharSelect())
                    {
                        mMainMenu.SetVisible(false);
                        mState = Selection.menuChar;
                        mCharMenu.Update();

                    }
                    if (mMainMenu.IsExit())
                    {
                        mState = Selection.exitGame;
                        UnloadMenu();
                        return true;
                    }
                    break;
                    #endregion
                case Selection.menuChar:
                    mCharMenu.Update();
                    if (!mCharMenu.IsVisible())
                    {
                        mState = Selection.menuMain;
                    }
                    break;
            }
            return false;
        }

        protected override void UnloadMenu()
        {
            mStartMenu.UnloadMenu();
            mMainMenu.UnloadMenu();
            mHeroPane.RemoveFromAutoDrawSet();
            mBlackGround.RemoveFromAutoDrawSet();
            mBackgroundImg.RemoveFromAutoDrawSet();
            mHeroBanner.RemoveFromAutoDrawSet();
            mCharMenu.RemoveFromAutoDrawSet();
            XNACS1Base.PlayBackgroundAudio(null, 0.5f);
        }
        protected void BringToTop()
        {
            mBackgroundImg.BringToTop();
            mBlackGround.BringToTop();
            mHeroBanner.BringToTop();
            mHeroPane.BringToTop();
            mStartMenu.BringToTop();
            mMainMenu.BringToTop();
            mCharMenu.BringToTop();   
        }

        public int GetPlayersSelection()
        {
            return mPlayerNumber;
        }

        public bool LoadGame()
        {
            mBlackGround.IncrementAlpha(0.025f);
            // something needs to be done to stop the increment
            UnloadMenu();
            return false;
        }

        public int Player1Selects()
        {
            return (int)mCharMenu.Player1Selects();
        }
        public int Player2Selects()
        {
            return (int)mCharMenu.Player2Selects();
        }

        public void setWins()
        {
          //  mUnlock++;
        }
    }
}
