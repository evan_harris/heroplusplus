﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class SubMenuCharacterSelection : MenuPane
    {
        #region states of the menu
        public enum Selection { positive = 0, negative = 1, sung = 2, zander = 3, girl = 4, none = 5 };
        public Selection mPlayerState1 { get; private set; }
        public Selection mPlayerState2 { get; private set; }
        public Selection mPlayerSelected1 { get; private set; }
        public Selection mPlayerSelected2 { get; private set; }

        public enum MoveSelection { up, none, down, done };
        public MoveSelection mPlayerMove1 { get; private set; }
        public MoveSelection mPlayerMove2 { get; private set; }
        #endregion

        #region character selection information
        String[] mCharacters = {   "pos_LN_standing",       // positive hero
                                   "neg_LN_standing",       // negative hero
                                   "sung_LN_standing",      // sung hero
                                   "zander_LN_standing",    // zander hero
                                   "girl_LN_standing" };    // girl hero
        int mMaxCharacters = 3;
        int mUnlockedCharacters;
        #endregion

        int mSelectionMoveCounter1 = 0;
        int mSelectionMoveCounter2 = 0;
        int mSelectionDelay = 12;
        int mPlayerNumber = 0;

        #region button variables
        MenuButton  mLeftUpArrow, mRightUpArrow,        // up and down arrows
                    mLeftDownArrow, mRightDownArrow;

        MenuButton  mPlayer1, mPlayer2;                 // player 1 and player 2 textures
        #endregion                                      // current status is to hard code 2 players only

        GameController[] mController;

        public SubMenuCharacterSelection()
        {
            mUnlockedCharacters = 2;
            mPlayerState1 = Selection.positive;
            mPlayerState2 = Selection.negative;

            #region initialize buttons
            mLeftDownArrow = new MenuButton();
            mLeftUpArrow = new MenuButton();
            mRightDownArrow = new MenuButton();
            mRightUpArrow = new MenuButton();

            mPlayer1 = new MenuButton();
            mPlayer2 = new MenuButton();
            #endregion
        }

        public SubMenuCharacterSelection(float X, float Y, float W, float H, String texture, GameController[] con, int players)
        {
            CenterX = X;
            CenterY = Y;
            Width = W;
            Height = H;
            Texture = texture;

            mUnlockedCharacters = 2;
            mPlayerState1 = Selection.positive;
            mPlayerState2 = Selection.negative;
            mPlayerSelected1 = Selection.none;
            mPlayerSelected2 = Selection.none;

            #region initialize buttons
            float charWidth = 8;
            float charHeight = 10;
            float offsetY = charHeight / 2 + 2;
            float offsetX = 5 + charWidth;
            
            mLeftDownArrow = new MenuButton(
                X - offsetX, Y - offsetY, 2, 2, "arrow_down");
            mLeftUpArrow = new MenuButton(
                X - offsetX, Y + offsetY, 2, 2, "arrow_up");
            mPlayer1 = new MenuButton(
                X - offsetX, Y,
                charWidth, charHeight, mCharacters[(int)mPlayerState1]);
            mPlayerMove1 = MoveSelection.none;
            mRightDownArrow = new MenuButton(
                X, Y - offsetY, 2, 2, "arrow_down");
            mRightUpArrow = new MenuButton(
                X, Y + offsetY, 2, 2, "arrow_up");
            mPlayer2 = new MenuButton(
                X, Y,
                charWidth, charHeight, mCharacters[(int)mPlayerState2]);
            mPlayerMove2 = MoveSelection.none;
            #endregion

            mController = con;
            mPlayerNumber = players;
            SetVisible(false);
        }

        /// <summary>
        /// earned a character, increment our counter to include all the characters
        /// </summary>
        public void SetUnlockalbe(int characters)
        {
            mUnlockedCharacters = characters;
            // keep unlocking characters, eventually max out
            if (mUnlockedCharacters >= mMaxCharacters)
            {   // once we unlocked everything, we are done
                mUnlockedCharacters = mMaxCharacters;
            }
        }

        /// <summary>
        /// Unload all values that contained by the menu. Remove it from the drawing set
        /// </summary>
        public void UnloadMenu()
        {
            this.RemoveFromAutoDrawSet();

            mLeftDownArrow.RemoveFromAutoDrawSet();
            mLeftUpArrow.RemoveFromAutoDrawSet();
            mRightDownArrow.RemoveFromAutoDrawSet();
            mRightUpArrow.RemoveFromAutoDrawSet();
            
            mPlayer1.RemoveFromAutoDrawSet();
            mPlayer2.RemoveFromAutoDrawSet();
        }

        public void Update()
        {
            SetVisible(true);
            UpdateThumbstick1();
            UpdateThumbstick2();

            UpdateButtonSelection1();
            UpdateButtonSelection2();

            UpdateTexture1();
            UpdateTexture2();

            if (mPlayerSelected1 != Selection.none && mPlayerSelected2 != Selection.none)
                SetVisible(false);  // exits the character selection
            
        }

        /// <summary>
        /// Update the thumbstick for the specific controller and player state.
        /// </summary>
        /// <param name="con">the current controller passed in</param>
        /// <param name="playerState">the player state that is associated with the controller</param>
        private void UpdateThumbstick1()
        {
            // move depending on buttons pressed
            if (mSelectionMoveCounter1 > mSelectionDelay && mPlayerMove1 != MoveSelection.done)
            {
                mPlayerMove1 = MoveSelection.none;
                if (mController[0].LeftDir.Y < -0.5f)
                {   // increment up by decreasing the player state
                    mSelectionMoveCounter1 = 0;
                    mPlayerState1++;
                    mPlayerMove1 = MoveSelection.down;

                    if ((int)mPlayerState1 >= mUnlockedCharacters)
                        mPlayerState1 = (Selection)0;
                }
                if (mController[0].LeftDir.Y > 0.5f)
                {   // decrement down by increasing the player state
                    mSelectionMoveCounter1 = 0;
                    mPlayerState1--;
                    mPlayerMove1 = MoveSelection.up;
                    if ((int)mPlayerState1 < 0)
                        mPlayerState1 = (Selection)(mUnlockedCharacters - 1);
                }
            }
            else
            {
                mSelectionMoveCounter1++;
                
            }
        }
        private void UpdateThumbstick2()
        {
            // move depending on buttons pressed
            if (mSelectionMoveCounter2 > mSelectionDelay && mPlayerMove2 != MoveSelection.done)
            {
                mPlayerMove2 = MoveSelection.none;
                if (mController[1].LeftDir.Y < -0.5f)
                {   // increment up by decreasing the player state
                    mSelectionMoveCounter2 = 0;
                    mPlayerState2++;
                    mPlayerMove2 = MoveSelection.down;

                    if ((int)mPlayerState2 >= mUnlockedCharacters)
                        mPlayerState2 = (Selection)0;
                }
                if (mController[1].LeftDir.Y > 0.5f)
                {   // decrement down by increasing the player state
                    mSelectionMoveCounter2 = 0;
                    mPlayerState2--;
                    mPlayerMove2 = MoveSelection.up;
                    if ((int)mPlayerState2 < 0)
                        mPlayerState2 = (Selection)(mUnlockedCharacters - 1);
                }

            }
            else
            {
                mSelectionMoveCounter2++;
            }
        }

        private void UpdateButtonSelection1()
        {
            if (mController[0].mAPress && mSelectionMoveCounter1 > mSelectionDelay)
            {
                mPlayerSelected1 = mPlayerState1;
                mPlayerMove1 = MoveSelection.done;
                // i need to stop people from doing up and down thumbsticks
            }

            if (mController[0].mBPress && mSelectionMoveCounter1 > mSelectionDelay)
            {
                if (mPlayerMove1 == MoveSelection.done)
                {
                    mPlayerMove1 = MoveSelection.none;
                    mSelectionMoveCounter1 = 0;
                }
                else if (mPlayerMove1 != MoveSelection.done)
                {
                    SetVisible(false);
                }
            }
        }
        private void UpdateButtonSelection2()
        {
            if (mController[1].mAPress && mSelectionMoveCounter2 > mSelectionDelay)
            {
                mPlayerSelected2 = mPlayerState2;
                // i need to stop people from doing up and down thumbsticks
            }

            if (mController[1].mBPress && mSelectionMoveCounter2 > mSelectionDelay)
            {
                if (mPlayerMove2 == MoveSelection.done)
                {
                    mPlayerMove2 = MoveSelection.none;
                    mSelectionMoveCounter2 = 0;
                }
                else
                {
                    SetVisible(false);
                }
            }
        }

        /// <summary>
        /// Update the texture for the current player and the current player state
        /// </summary>
        /// <param name="playerState"></param>
        /// <param name="player"></param>
        private void UpdateTexture1()
        {
            if (mPlayerMove1 == MoveSelection.down)
            {
                mLeftDownArrow.TextureTintColor = Color.Yellow;
            }
            if (mPlayerMove1 == MoveSelection.up)
            {
                mLeftUpArrow.TextureTintColor = Color.Yellow;
            }
            if ((mPlayerMove1 == MoveSelection.none || mPlayerMove1 == MoveSelection.done )
                && mSelectionMoveCounter1 > mSelectionDelay)
            {
                mLeftDownArrow.TextureTintColor = Color.White;
                mLeftUpArrow.TextureTintColor = Color.White;
            }
            mPlayer1.Texture = mCharacters[(int)mPlayerState1];
        }
        private void UpdateTexture2()
        {
            if (mPlayerMove2 == MoveSelection.down)
            {
                mRightDownArrow.TextureTintColor = Color.Yellow;
            }
            if (mPlayerMove2 == MoveSelection.up)
            {
                mRightUpArrow.TextureTintColor = Color.Yellow;
            }
            if ((mPlayerMove2 == MoveSelection.none || mPlayerMove2 == MoveSelection.done)
                    && mSelectionMoveCounter2 < mSelectionDelay)
            {
                mRightDownArrow.TextureTintColor = Color.White;
                mRightUpArrow.TextureTintColor = Color.White;
            }
            mPlayer2.Texture = mCharacters[(int)mPlayerState2];
        }


        public void SetVisible(bool visible)
        {
            if (!visible)
            {
                this.Visible = false;
                mLeftUpArrow.Visible = false;
                mRightUpArrow.Visible = false;
                mLeftDownArrow.Visible = false;
                mRightDownArrow.Visible = false;
                mPlayer1.Visible = false;
                mPlayer2.Visible = false;

            }
            else
            {
                this.Visible = true;
                mLeftUpArrow.Visible = true;
                mRightUpArrow.Visible = true;
                mLeftDownArrow.Visible = true;
                mRightDownArrow.Visible = true;
                mPlayer1.Visible = true;
                mPlayer2.Visible = true;
            }
        }

        public bool IsVisible()
        {
            return Visible;
        }

        public SubMenuCharacterSelection.Selection Player1Selects()
        {
            if (mPlayerSelected1 == Selection.none)
                return mPlayerState1;
            return mPlayerSelected1;
        }
        public SubMenuCharacterSelection.Selection Player2Selects()
        {
            if (mPlayerSelected2 == Selection.none)
                return mPlayerState2;
            return mPlayerSelected2;
        }
        public override void BringToTop()
        {
            if(!this.IsInAutoDrawSet())
                this.AddToAutoDrawSet();
            this.TopOfAutoDrawSet();
            if (!mLeftDownArrow.IsInAutoDrawSet())
                mLeftDownArrow.AddToAutoDrawSet();
            mLeftDownArrow.TopOfAutoDrawSet();
            if (!mLeftUpArrow.IsInAutoDrawSet())
                mLeftUpArrow.AddToAutoDrawSet();
            mLeftUpArrow.TopOfAutoDrawSet();
            
            if (!mRightUpArrow.IsInAutoDrawSet())
                mRightUpArrow.AddToAutoDrawSet();
            mRightUpArrow.TopOfAutoDrawSet();
            if (!mRightDownArrow.IsInAutoDrawSet())
                mRightDownArrow.AddToAutoDrawSet();
            mRightDownArrow.TopOfAutoDrawSet();


            if (!mPlayer1.IsInAutoDrawSet())
                mPlayer1.AddToAutoDrawSet();
            mPlayer1.TopOfAutoDrawSet();
            if (!mPlayer2.IsInAutoDrawSet())
                mPlayer2.AddToAutoDrawSet();
            mPlayer2.TopOfAutoDrawSet();

        }

    }
}
