﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class MenuButton : XNACS1Rectangle
    {
        private bool mPressed;
        public MenuButton()
        {
        }
        public MenuButton(float X, float Y, float W, float H, String texture)
        {
            CenterX = X;
            CenterY = Y;
            Width = W;
            Height = H;
            Texture = texture;
            mPressed = false;
            Color = Color.AliceBlue;
        }

        public bool IsPressed()
        {
            return mPressed;
        }

        public void SetPressed(bool aButtonPressed)
        {
            if (aButtonPressed)
                mPressed = true;
            else mPressed = false;
        }

        public void SetAlpha(float alpha)
        {
            TextureTintColor = new Color(Color.R * alpha, Color.G * alpha, Color.B * alpha, alpha);
        }
    }
}
