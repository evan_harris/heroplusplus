﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class MenuPane : XNACS1Rectangle
    {
        float mAlpha;
        public MenuPane()
        {
        }
        public MenuPane(float X, float Y, float W, float H, String texture)
        {
            CenterX = X;
            CenterY = Y;
            Width = W;
            Height = H;
            Texture = texture;
        }

        public void SetAlpha(float alpha)
        {
            mAlpha = alpha;
            TextureTintColor = new Color(Color.R * mAlpha, Color.G * mAlpha, Color.B * mAlpha, mAlpha);
        }
        public void IncrementAlpha(float alpha)
        {
            mAlpha -= alpha;
            TextureTintColor = new Color(Color.R * mAlpha, Color.G * mAlpha, Color.B * mAlpha, mAlpha);
        }
        public virtual void BringToTop()
        {
            if (this.IsInAutoDrawSet())
                this.AddToAutoDrawSet();
            this.TopOfAutoDrawSet();
        }
    }
}
