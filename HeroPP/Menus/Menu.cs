﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class Menu
    {
        protected GameController[] mControls;

        protected int mHighlightedColumn;
        protected int mHighlightedRow;
        protected int mSelectionMoveCounter;
        protected int mSelectionDelay;

        protected Vector2 mMenuCenterPos;
        protected float mMenuWidth;
        protected float mMenuHeight;

        // parts to draw a menu
        protected MenuPane mBackground;
        protected MenuButton[] mButtons;
        protected MenuButton[] mText;

        //------------------------ALL PUBLIC FUNCTIONS AND INTERFACES---------------------------
        //construct the main menu
        public Menu(GameController[] con1)
        {
            mHighlightedColumn = 0;
            mHighlightedRow = 0;
            mSelectionMoveCounter = 0;
            mSelectionDelay = 7;

            // reference the controller set
            mControls = con1;
            mButtons = mText = null;
        }

        public virtual void InitializeMenu()
        {
            // load background
            mBackground = new MenuPane();
            mBackground.Width = XNACS1Base.World.WorldDimension.X;
            mBackground.Height = XNACS1Base.World.WorldDimension.Y;
            mBackground.CenterX = (XNACS1Base.World.WorldMax.X - XNACS1Base.World.WorldMin.X) / 2;
            mBackground.CenterY = (XNACS1Base.World.WorldMax.Y - XNACS1Base.World.WorldMin.Y) / 2;
            mBackground.Visible = false;
        }

        //return true when done
        public virtual bool Update()
        {
            // watch out for overflow
            if (mSelectionMoveCounter == int.MaxValue)
                mSelectionMoveCounter = mSelectionDelay + 1;

            // increment the delay counter;
            mSelectionMoveCounter++;
            return false;
        }

        protected virtual void MoveVertical()
        {
        }

        protected virtual void MoveHorizontal()
        {
        }

        //-----------------------ALL PRIVATE WORKER FUCTIONS---------------------------------
        protected virtual void UnloadMenu()
        {
            //foreach (XNACS1Rectangle b in mButtons)
            // b.RemoveFromAutoDrawSet();

            //foreach (XNACS1Rectangle t in mText)
            //t.RemoveFromAutoDrawSet();

            mBackground.RemoveFromAutoDrawSet();
        }

        protected virtual void Hide()
        {
            if (mButtons != null)
                foreach (XNACS1Rectangle b in mButtons)
                    b.RemoveFromAutoDrawSet();

            if (mText != null)
                foreach (XNACS1Rectangle t in mText)
                    t.RemoveFromAutoDrawSet();

            if (mBackground != null)
                mBackground.RemoveFromAutoDrawSet();
        }
    }
}
