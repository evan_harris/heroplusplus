﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class HealthBar : CharacterBar
    {
        public HealthBar()
        {
        }

        public HealthBar(float X, float Y, float W, float H)
        {
            mCenter = new Vector2(X,Y);
            mWidth = W;
            mHeight = H;

            mHealthGauge = new XNACS1Rectangle(mCenter,mWidth,mHeight);
            mHealthGauge.Color = Color.LawnGreen;

            mBorderThickness = 0.18f;

            // border implementation
            mTopBorder = new XNACS1Rectangle();
            mTopBorder.Color = Color.Black;
            mTopBorder.Width = mWidth;
            mTopBorder.Height = mBorderThickness;
            mTopBorder.CenterX = mCenter.X;
            mTopBorder.CenterY = mCenter.Y + mHeight / 2;

            mBottomBorder = new XNACS1Rectangle();
            mBottomBorder.Color = Color.Black;
            mBottomBorder.Width = mWidth;
            mBottomBorder.Height = mBorderThickness;
            mBottomBorder.CenterX = mCenter.X;
            mBottomBorder.CenterY = mCenter.Y - mHeight / 2;

            mLeftBorder = new XNACS1Rectangle();
            mLeftBorder.Color = Color.Black;
            mLeftBorder.Width = mBorderThickness;
            mLeftBorder.Height = mHeight;
            mLeftBorder.CenterX = mCenter.X - mWidth / 2;
            mLeftBorder.CenterY = mCenter.Y;

            mRightBorder = new XNACS1Rectangle();
            mRightBorder.Color = Color.Black;
            mRightBorder.Width = mBorderThickness;
            mRightBorder.Height = mHeight;
            mRightBorder.CenterX = mCenter.X + mWidth / 2;
            mRightBorder.CenterY = mCenter.Y;

        }
    }
}
