﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    
    class CharacterBar
    {
        protected float mWidth, mHeight, mBorderThickness;
        protected Vector2 mCenter;
        protected XNACS1Rectangle mTopBorder, mLeftBorder, mRightBorder, mBottomBorder;
        protected XNACS1Rectangle mHealthGauge;


        public CharacterBar()
        {
        }
        public void Clear()
        {
            mTopBorder.RemoveFromAutoDrawSet();
            mLeftBorder.RemoveFromAutoDrawSet();
            mRightBorder.RemoveFromAutoDrawSet();
            mBottomBorder.RemoveFromAutoDrawSet();
            mHealthGauge.RemoveFromAutoDrawSet();

        }
        /// <summary>
        /// Move the bar according to the camera
        /// </summary>
        public void UpdateCamera(float moveX)
        {
            mTopBorder.CenterX += moveX;
            mLeftBorder.CenterX += moveX;
            mRightBorder.CenterX += moveX;
            mBottomBorder.CenterX += moveX;
            mHealthGauge.CenterX += moveX;
            mCenter.X += moveX;
            float diffX = mWidth - mHealthGauge.Width;
            mHealthGauge.CenterX = mCenter.X - diffX / 2;
        }
        public void UpdateBarStat(float percentStat)
        {
            mHealthGauge.Width = percentStat * mWidth;

        }
        public void BringToFront()
        {

            mTopBorder.TopOfAutoDrawSet();
            mLeftBorder.TopOfAutoDrawSet();
            mRightBorder.TopOfAutoDrawSet();
            mBottomBorder.TopOfAutoDrawSet();
            mHealthGauge.TopOfAutoDrawSet();

        }

    }
}
