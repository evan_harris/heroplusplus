﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class CharacterHUD : XNACS1Rectangle
    {
        EnergyBar mEnergy;
        HealthBar mHealth;
        
        private float xoffset;
        private float yoffset;
        public CharacterHUD(float X, float Y, float W, float H, String label)
        {
            
            Texture = label;
            Width = W;
            Height = H;
             xoffset = CenterX = X;
            yoffset = CenterY = Y;
            float tempWidth = W - 7.5f;
            float tempHeight = H / 4;
            mHealth = new HealthBar(X + 2.7f, Y + tempHeight/3 + 1.1f, tempWidth, tempHeight);
            mEnergy = new EnergyBar(X + 2.7f, (float)(Y - tempHeight/3 + 0.3f), tempWidth, tempHeight);



        }
        public void Clear()
        {
            mHealth.Clear();
            mEnergy.Clear();
            this.Visible = false;
            this.RemoveFromAutoDrawSet();
        }
        public void UpdateCamera()
        {
            float x = CenterX - (XNACS1Base.World.WorldMin.X + xoffset);
            mHealth.UpdateCamera(-x);
            mEnergy.UpdateCamera(-x);
            this.CenterX -= x;

        }
        public void BringToFront()
        {
            this.TopOfAutoDrawSet();
            mHealth.BringToFront();
            mEnergy.BringToFront();
            
        }
        public void UpdateHealthBarStat(float h)
        {
            mHealth.UpdateBarStat(h);
        }
        public void UpdateEnergyBarStat(float h)
        {
            mEnergy.UpdateBarStat(h);
        }
    }
}
