﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class SubMenuStart : MenuPane
    {
        MenuButton mButtonStart;
        float mButtonWidth, mButtonHeight, mPercent;
        bool mSize, mIsVisible;
        GameController[] mController;
        int mPlayerNumber = 0;

        public SubMenuStart()
        {
            mButtonStart = new MenuButton();
        }

        public SubMenuStart(float X, float Y, float W, float H, String texture, GameController[] con, int players)
        {
            mButtonWidth = W;
            mButtonHeight = H;
            mButtonStart = new MenuButton(X, Y, mButtonWidth, mButtonHeight, "pressStart");

            CenterX = X;
            CenterY = Y;
            Width = W;
            Height = H;
            Visible = false;
            mPercent = 1;
            mSize = true;

            mController = con;
            mPlayerNumber = players;

            mIsVisible = true;
        }

        public void UnloadMenu()
        {
            this.RemoveFromAutoDrawSet();
            mButtonStart.RemoveFromAutoDrawSet();
        }

        

        public void Update()
        {
            UpdateTexture();
            UpdateStartMenu();
            

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startPress"></param>
        /// <param name="state"></param>
        private void UpdateStartMenu()
        {
            for (int i = 0; i < mPlayerNumber; ++i)
            {
                if (mController[i].mStartPress)
                {
                    SetVisible(false);
                }
            }
        }

        private void UpdateTexture()
        {
            #region grow and shrink the size of the start menu
            // for each controller
            if (mSize)
            {
                mPercent -= 0.005f;
                if (mPercent < 0.9f)
                    mSize = false;
            }
            else
            {
                mPercent += 0.005f;
                if (mPercent > 1f)
                    mSize = true;
            }
            mButtonStart.Width = mPercent * mButtonWidth;
            mButtonStart.Height = mPercent * mButtonHeight;
            #endregion
        }

        public void SetVisible(bool visible)
        {
            if (!visible)
            {
                mButtonStart.Visible = false;
                mIsVisible = false;
            }
            else
            {
                mButtonStart.Visible = true;
                mIsVisible = true;
            }
        }

        public bool IsVisible()
        {
            return mIsVisible;
        }
        public override void BringToTop()
        {
            if( !this.IsInAutoDrawSet())
                this.AddToAutoDrawSet();
            this.TopOfAutoDrawSet();
            if(!this.mButtonStart.IsInAutoDrawSet())
                mButtonStart.AddToAutoDrawSet();
            mButtonStart.TopOfAutoDrawSet();
        }
    }
}
