﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class SubMenuMain : MenuPane
    {
        #region states of the menu
        public enum Selection { startGame = 0, selectCharacter = 1, exitGame = 2 };
        public Selection mState { get; private set; }
        #endregion

        #region buttons and button variables
        MenuButton mButtonStart, mButtonCharacterSelect, mButtonExit;
        int mMaxButtons = 3;
        #endregion

        #region sub menu information
        int mPlayerNumber = 0;
        int mSelectionMoveCounter = 0;  // the current count for moving the thumbstick
        int mSelectionDelay = 7;        // the maximum value that the selection 
                                        // counter needs to exceed before moving the controls
        #endregion

        GameController[] mController;
        
        /// <summary>
        /// Sub menu constructor
        /// </summary>
        public SubMenuMain()
        {
            mButtonStart = new MenuButton();
            mButtonCharacterSelect = new MenuButton();
            mButtonExit = new MenuButton();

            mState = Selection.startGame;
        }

        /// <summary>
        /// Sub menu constructor with variables
        /// </summary>
        /// <param name="X">position X</param>
        /// <param name="Y">position Y</param>
        /// <param name="W">width of the menu</param>
        /// <param name="H">height of the menu</param>
        /// <param name="texture">textures</param>
        /// <param name="con">game controller array</param>
        public SubMenuMain(float X, float Y, float W, float H, String texture, GameController[] con, int players)
        {
            #region set current pane variables
            CenterX = X;
            CenterY = Y;
            Width = W;
            Height = H;
            Texture = texture;
            #endregion

            mState = Selection.startGame;   // set the current button that will be highlighted

            mController = con;              // get the array of controllers
            mPlayerNumber = players;
            #region set new buttons
            float buttonWidth = Width;
            float buttonHeight = Height * 0.22f;
            float borderSpace = (Height - 3 * buttonHeight) / 5;

            // make new buttons here
            mButtonStart = new MenuButton(
                X, Y + buttonHeight + borderSpace,
                buttonWidth, buttonHeight, "txtStartSelected");

            mButtonCharacterSelect = new MenuButton(
                X, Y,
                buttonWidth, buttonHeight, "txtCharSelect");

            mButtonExit = new MenuButton(
                X, Y - buttonHeight - borderSpace,
                buttonWidth, buttonHeight, "txtExit");
            #endregion
            SetVisible(false);
        }

        /// <summary>
        /// Set the player amount 
        /// </summary>
        /// <param name="players"></param>
        public void SetNumberOfPlayers(int players)
        {
            mPlayerNumber = players;
        }

        public void Update()
        {
            SetVisible(true);
            UpdateThumbstick();
            UpdateButtonSelection();
            UpdateButtonTextures();
        }

        /// <summary>
        /// Unload and remove everything from the menu
        /// </summary>
        public void UnloadMenu()
        {
            this.RemoveFromAutoDrawSet();
            mButtonStart.RemoveFromAutoDrawSet();
            mButtonCharacterSelect.RemoveFromAutoDrawSet();
            mButtonExit.RemoveFromAutoDrawSet();
        }

        /// <summary>
        /// Update the main menu with functions:
        /// update the thumbsticks, buttons, textures, and then try to do something
        /// </summary>
        public void UpdateMainMenu()
        {
            UpdateThumbstick();
            UpdateButtonSelection();
            UpdateButtonTextures();
        }

        /// <summary>
        /// Update the thumbstick motion and button location
        /// </summary>
        private void UpdateThumbstick()
        {
            
            
            // move depending on buttons pressed
            if (mSelectionMoveCounter > mSelectionDelay)
            {
                mSelectionMoveCounter = 0;
                // check to see if the thumbstick was moved
                for (int i = 0; i < mPlayerNumber; i++)
                {
                
                    if (mController[i].LeftDir.Y < -0.5f)
                    {
                        mState++;
                        // if the state is greater than max buttons (off the menu), set it to the first menu item
                        if ((int)mState >= mMaxButtons)
                        {
                            mState = Selection.startGame;
                        }
                    }
                    else if (mController[i].LeftDir.Y > 0.5f)
                    {
                        mState--;
                        // if the state is less than zero (off the menu, set it to the last menu item
                        if (mState < 0)
                        {
                            mState = Selection.exitGame;
                        }
                    }
                }
            }
            else
            {
                mSelectionMoveCounter++;
            }
        }

        /// <summary>
        /// Update if buttons were pressed on the current button pressed.
        /// </summary>
        private void UpdateButtonSelection()
        {
            // set the buttons to false first
            mButtonStart.SetPressed(false);
            mButtonCharacterSelect.SetPressed(false);
            mButtonExit.SetPressed(false);

            // check to see which buttons were pressed on the selected items 
            for (int i = 0; i < mPlayerNumber; i++)
            {
                // pressing the back button will set the exit button to true
                if (mController[i].mBackPress)
                {
                    mButtonExit.SetPressed(true);
                }

                // switch on the state only if the button A is pressed
                switch (mState)
                {
                    case Selection.startGame:
                        if (mController[i].mAPress)
                        {
                            mButtonStart.SetPressed(true);
                        }
                        break;
                    case Selection.selectCharacter:
                        if (mController[i].mAPress)
                        {
                            mButtonCharacterSelect.SetPressed(true);
                        }
                        break;
                    case Selection.exitGame:
                        if (mController[i].mAPress)
                        {
                            mButtonExit.SetPressed(true);
                        }
                        break;
                }
            }

        }

        /// <summary>
        /// Update the button textures. If it isn't the item that is selected, make sure
        /// we change the alpha to a lower alpha value
        /// </summary>
        private void UpdateButtonTextures()
        {
            // set the alpha to 0.85 for a default value
            mButtonStart.Texture = "txtStart";
            mButtonCharacterSelect.Texture = "txtCharSelect";
            mButtonExit.Texture = "txtExit";

            // for a certain state, set the state of the other buttons
            // to an alpha to a low value 
            switch (mState)
            {
                case Selection.startGame:
                    mButtonStart.Texture = "txtStartSelected";
                    break;
                case Selection.selectCharacter:
                    mButtonCharacterSelect.Texture = "txtCharSelectSelected";
                    break;
                case Selection.exitGame:
                    mButtonExit.Texture = "txtExitSelected";
                    break;
            }
        }

        public void SetVisible(bool visible)
        {
            if (!visible)
            {
                this.Visible = false;
                mButtonStart.Visible = false;
                mButtonCharacterSelect.Visible = false;
                mButtonExit.Visible = false;

            }
            else
            {
                this.Visible = true;
                mButtonStart.Visible = true;
                mButtonCharacterSelect.Visible = true;
                mButtonExit.Visible = true;

            }
        }

        public bool IsStart()
        {
            return mButtonStart.IsPressed();
        }
        public bool IsCharSelect()
        {
            return mButtonCharacterSelect.IsPressed();
        }
        public bool IsExit()
        {
            return mButtonExit.IsPressed();
        }
        public override void BringToTop()
        {
            if(!this.IsInAutoDrawSet())
                this.AddToAutoDrawSet();
            this.TopOfAutoDrawSet();
            if (!mButtonStart.IsInAutoDrawSet())
                mButtonStart.AddToAutoDrawSet();
            mButtonStart.TopOfAutoDrawSet();
            if (!mButtonCharacterSelect.IsInAutoDrawSet())
                mButtonCharacterSelect.AddToAutoDrawSet();
            mButtonCharacterSelect.TopOfAutoDrawSet();
            if (!mButtonExit.IsInAutoDrawSet())
                mButtonExit.TopOfAutoDrawSet();
            mButtonExit.TopOfAutoDrawSet();
            //, mButtonCharacterSelect, mButtonExit;
        }

    }
}
