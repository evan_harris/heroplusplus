﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XNACS1Lib;
/*
 * This is the parent class to all the levels which defines
 * the interfaces to be used for all of them
 */
namespace Hero_Namespace
{
    class Level
    {

        protected List<Enemy> enemylist = new List<Enemy>();
        protected List<Spectator> spectatorlist = new List<Spectator>();
        public int mLevelWidth = 100;

        protected AttackManager attacks;
        protected GameObjManager objmgr;
        public float zmin;
        public float zmax;

        protected int numberofbhimages;
        protected XNACS1Rectangle[] background;

        public Level()
        { }

        public virtual float getHeroPos()
        {
            return 0;
        }

        public virtual float getCamPos()
        {
            return 0f;
        }

        public virtual void InitializeLevel(GameObjManager mgr, AttackManager attack)
        {
            objmgr = mgr;
            attacks = attack;
        }
        public virtual bool UpdateLevel()
        {
            return true;
        }
        public virtual void UnloadLevel()
        {
            objmgr.ClearAll();

            XNACS1Base.PlayBackgroundAudio(null, 0.5f);

            foreach (Enemy e in enemylist)
            {
                e.MakeNotActive();
            }
            enemylist.Clear();

            foreach (Spectator s in spectatorlist)
                s.MakeNotActive();
            spectatorlist.Clear();

            for (int i = 0; i < background.Length; i++)
            {
                background[i].RemoveFromAutoDrawSet();
                //background[i] = null;
            }


        }
        public int LevelWidth()
        {
            return mLevelWidth;
        }

        public virtual void resetLevel()
        {
          
        }
       
    }
}