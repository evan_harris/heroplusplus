﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hero_Namespace
{
    class LvlThreeMess : LevelMessage
    {
        private int goAwayTimer;
        private int growtimer;
        private float growrate = 0.1f;
        private bool growbigger = false;
        public LvlThreeMess()
        {
            Width = 50f * 1.5f;
            Height = 5.75f * 1.5f;
            Texture = "Lvl3Mess";
            goAwayTimer = 200;


        }

        public override void Update()
        {

            CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
            CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;

            goAwayTimer--;
            if (goAwayTimer <= 0)
            {
                RemoveFromAutoDrawSet();

            }
            else
            {



                TopOfAutoDrawSet();
                if (growtimer >= 30)
                {
                    growtimer = 0;
                    if (growbigger)
                        growbigger = false;
                    else
                        growbigger = true;
                }
                else
                {
                    growtimer++;
                    if (growbigger)
                    {
                        Width += growrate;
                        Height += growrate * (Height / Width);
                    }
                    else
                    {
                        Width -= growrate;
                        Height -= growrate * (Height / Width);
                    }
                }

                //if (goAwayTimer <= 60)
                //{
                //    CenterX += 1.5f;
                //}
                //else
                //{
                //    CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
                //    CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
                //}
                //}

            }

        }

        public override void Activate(float inxoffset, float inyoffset)
        {
            xoffset = inxoffset;
            yoffset = inyoffset;

            CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
            CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
            AddToAutoDrawSet();
            TopOfAutoDrawSet();
            Visible = true;

        }

        public override bool isFinished()
        {
            return goAwayTimer <= 0;
        }

    }
}
