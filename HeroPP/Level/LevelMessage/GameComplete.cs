﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hero_Namespace
{
    class GameComplete : LevelMessage
    {
        private int goAwayTimer;
    

        public bool isActive;
        public GameComplete()
        {
            Width = 1000f * 0.06f;
            Height = 800f * 0.06f;
            Texture = "endgame";
            goAwayTimer = 500;

            isActive = false;
        }

        public override void Update()
        {
            goAwayTimer--;
            if (goAwayTimer <= 0)
            {
                RemoveFromAutoDrawSet();

            }
            else
            {


                CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
                CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
                TopOfAutoDrawSet();
               
                }

                //if (goAwayTimer <= 60)
                //{
                //    CenterX += 1.5f;
                //}
                //else
                //{
                //    CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
                //    CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
                //}
                //}

            }

        

        public override void Activate(float inxoffset, float inyoffset)
        {
            xoffset = inxoffset;
            yoffset = inyoffset;

            CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
            CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
            AddToAutoDrawSet();
            TopOfAutoDrawSet();

            isActive = true;
        }

        public override bool isFinished()
        {
            return goAwayTimer <= 0;
        }

    }
}
