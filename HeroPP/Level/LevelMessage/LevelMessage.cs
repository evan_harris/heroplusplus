﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNACS1Lib;
namespace Hero_Namespace
{
    abstract class LevelMessage: XNACS1Rectangle
    {
        protected float xoffset = 0;
        protected float yoffset = 0;
        public LevelMessage()
        {
            RemoveFromAutoDrawSet();
        }
        public abstract void Update();
        public abstract void Activate(float inxoffset, float inyoffset);
        public abstract bool isFinished();

    }
}
