﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hero_Namespace
{
    class respawn_message : LevelMessage
    {
        private int goAwayTimer = 0;
        private int growtimer;
        private float growrate = 0.1f;
        private bool growbigger = false;
        private bool beenran = false;

        public respawn_message()
        {
            Width = 350f * 0.1f;
            Height = 100f * 0.1f;
            Texture = "respawn_ind";
            goAwayTimer = 250;


        }

        public override void Update()
        {
            goAwayTimer--;

            if (goAwayTimer <= 0)
            {
                RemoveFromAutoDrawSet();

            }
            else
            {

                CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
                CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
                TopOfAutoDrawSet();

                if (growtimer >= 30)
                {
                    growtimer = 0;
                    if (growbigger)
                        growbigger = false;
                    else
                        growbigger = true;
                }
                else
                {
                    growtimer++;
                    if (growbigger)
                    {
                        Width += growrate;
                        Height += growrate;
                    }
                    else
                    {
                        Width -= growrate;
                        Height -= growrate;
                    }
                }

            }
        }

        public override void Activate(float inxoffset, float inyoffset)
        {
            goAwayTimer = 100;
            xoffset = inxoffset;
            yoffset = inyoffset;

            CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
            CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
            AddToAutoDrawSet();
            TopOfAutoDrawSet();
            growtimer = 0;
            beenran = true;
        }

        public override bool isFinished()
        {
            return goAwayTimer <= 0;
        }
        public bool isRun()
        {
            return beenran;
        }
    }
}
