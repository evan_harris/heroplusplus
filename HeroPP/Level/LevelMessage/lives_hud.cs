﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNACS1Lib;
namespace Hero_Namespace
{
    class lives_hud : LevelMessage
    {
        XNACS1Rectangle[] lifeheart;
        private int currentlife;
        private float heart_spacing = 4.5f;
        
        public lives_hud()
        { 
            //initialize all the little hearts
            lifeheart = new XNACS1Rectangle[10];
            for (int i = 0; i < 10; i++ )
            {
                lifeheart[i] = new XNACS1Rectangle();
                lifeheart[i].Visible = false;
                lifeheart[i].Texture = "heart_life";
                lifeheart[i].Width = 4;
                lifeheart[i].Height = 4;
                RemoveFromAutoDrawSet();
            }
            Width = 300 * 0.07f;
            Height = 200 * 0.07f;
            Visible = false;
        }
        public void AddLife(int number)
        { 
        currentlife++;
        }

         public override void Update()
        {
           
             //center the bounding box
                CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
                CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;

                Visible = true;
               
             //adjust all of the hearts too, according to the bounding box
                for (int i = 0; i < 10; i++)
                {
                    lifeheart[i].TopOfAutoDrawSet();
                    lifeheart[i].CenterX = CenterX + (i * heart_spacing);
                    lifeheart[i].CenterY = CenterY;
                    //if the heart should be shown
                   
                    if (i < currentlife)
                    {
                        if (!lifeheart[i].Visible)
                            lifeheart[i].Visible = true;
                    }
                    else
                    {
                        if (lifeheart[i].Visible)
                            lifeheart[i].Visible = false;
                    }
                }
               
               
        }

        public override void Activate(float inxoffset, float inyoffset)
        {
            xoffset = inxoffset;
            yoffset = inyoffset;
            CenterX = xoffset + XNACS1Lib.XNACS1Base.World.WorldMin.X;
            CenterY = yoffset + XNACS1Lib.XNACS1Base.World.WorldMin.Y;
          // AddToAutoDrawSet();
         //   TopOfAutoDrawSet();
            foreach (XNACS1Rectangle rec in lifeheart)
                rec.AddToAutoDrawSet();
            Visible = true;
     
        }
        public override bool isFinished()
        {
            return false;
        }
        public bool RemoveLife(int number)
        {
            if(currentlife <= 0)
                return false;
            else
                currentlife--;
            return true;
        }

        public void SetLives(int number)
        {
        currentlife = number;
        }

        public bool LivesRemain()
        {
            return currentlife > 0;
        }

      
        
        }

    }
