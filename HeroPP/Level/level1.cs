﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;
namespace Hero_Namespace
{
   
    class level1 : Level
    {

        bool bosssummon = false;
        GoLvlMessage gomess;
        introStoryMess intromess;
        LevelComplete lvlcomplete;
        public level1()
        {
            zmin = 0.0f;
            zmax = 32f;

            numberofbhimages = 6;
            mLevelWidth = 868;  // pixel width of the level
        }

        public override float getHeroPos()
        {
            return 10f;
        }

        public override float getCamPos()
        {
            return 0f;
        }

        public override void InitializeLevel(GameObjManager mgr, AttackManager attackmgr)
        {

            XNACS1Base.PlayBackgroundAudio("lvl1_bg", 0.5f);

            background = new XNACS1Rectangle[numberofbhimages];
            for (int i = 0; i < numberofbhimages; i++)
            {
                background[i] = new XNACS1Rectangle();
                background[i].CenterX = 72.5f + (float)(i * 145);
                background[i].CenterY = 40;
                background[i].Width = 145f;
                background[i].Height = 87f;
            }

            background[0].Texture = "level1_1";
            background[1].Texture = "level1_2";
            background[2].Texture = "level1_3";
            background[3].Texture = "level1_4";
            background[4].Texture = "level1_5";
            background[5].Texture = "level1_6";

            //set the object manager reference
            objmgr = mgr;
            attacks = attackmgr;

            //NOW, do some object setups
            objmgr.setZValues(zmin, zmax);
            //-----------------------------------------------------
           
                Enemy enem;

                enem = new Walker();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(120, 15);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);


                enem = new Walker();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(175, 20);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);

                enem = new WeeFi();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(175, 20);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);


                enem = new Walker();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(380, 16);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);


                enem = new WeeFi();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(500, 8);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);

                enem = new WeeFi();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(500, 16);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);

                enem = new WeeFi();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(500, 26);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);

                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(100, 28);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);


                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(215, 6);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);

                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(300, 6);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);

                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(300, 20);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);



                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(400, 6);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);



                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(400, 16);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);


                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(400, 24);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);



                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(700, 6);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);



                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(700, 12);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);



                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(700, 18);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);



                enem = new TankFlipFlop();
                enem.SetAttackMgr(attacks);
                enem.SetInitialPosition(700, 24);
                enemylist.Add(enem);
                objmgr.AddEnemy(enem);
            //--------------------------------------------
            pickup pic;

            pic = new edrink();
            pic.SetSimplePos(240, 0, 15);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(255, 0, 15);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(250, 0, 8);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(720, 0, 8);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(720, 0, 4);
            objmgr.AddPickUp(pic);

            pic = new sandwich();
            pic.SetSimplePos(215, 0, 20);
            objmgr.AddPickUp(pic);

            pic = new sandwich();
            pic.SetSimplePos(434, 0, 18);
            objmgr.AddPickUp(pic);


            pic = new sandwich();
            pic.SetSimplePos(570, 0, 6);
            objmgr.AddPickUp(pic);


            pic = new sandwich();
            pic.SetSimplePos(600, 0, 15);
            objmgr.AddPickUp(pic);

            Spectator spec = new SpectatorZander();
            spec.SetInitialPosition(25, 30);
            objmgr.AddSpectator(spec);

            spec = new SpectatorSung();
            spec.SetInitialPosition(723, 29);
            objmgr.AddSpectator(spec);

            objmgr.setZValues(zmin, zmax);


            gomess = new GoLvlMessage();
            intromess = new introStoryMess();
            lvlcomplete = new LevelComplete();
            intromess.Activate(40, 50);
            //gomess.Activate(85, 50);
            

        }

        public override bool UpdateLevel()
        {



            if (!intromess.isFinished())
                intromess.Update();
            else
            {
                if (gomess.isRun())
                {
                    if (!gomess.isFinished())
                        gomess.Update();
                }
                else
                    gomess.Activate(85, 50);
            
            
            }

            //check for victory conditions

            if (objmgr.EnemiesDead() && bosssummon == false)
            {
                Level1Boss boss;
                boss = new Level1Boss();
                boss.SetAttackMgr(attacks);
                boss.SetInitialPosition(850, 24);
                enemylist.Add(boss);
                objmgr.AddEnemy(boss);
                bosssummon = true;
            }

            if(objmgr.EnemiesDead())
            {
            if(lvlcomplete.isActive == true)
            {
                if(lvlcomplete.isFinished())
                {
                return true; //finish the level after displaying the message
                }
            lvlcomplete.Update();
            }
            else
                lvlcomplete.Activate(55,40);
            
            }

            return false;
            //return true when done            
        }

    }
}
