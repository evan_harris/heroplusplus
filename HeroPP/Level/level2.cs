﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNACS1Lib;
namespace Hero_Namespace
{
    class level2 : Level
    {
        GoLvlMessageL gomess;
        LevelComplete lvlcomplete;
        LvlTwoMess intromess;
        public level2()
        {
            zmin = 0.0f;
            zmax = 32f;

            numberofbhimages = 6;
            mLevelWidth = 868;  // pixel width of the level
        }

        public override float getHeroPos()
        {
            return 855f;
        }

        public override float getCamPos()
        {
            return 750f;
        }

        public override void InitializeLevel(GameObjManager mgr, AttackManager attackmgr)
        {
            XNACS1Base.PlayBackgroundAudio("lvl2_bg", 0.5f);

            background = new XNACS1Rectangle[numberofbhimages];
            for (int i = 0; i < numberofbhimages; i++)
            {
                background[i] = new XNACS1Rectangle();
                background[i].CenterX = 72.5f + (float)(i * 145);
                background[i].CenterY = 43.5f;
                background[i].Width = 145f;
                background[i].Height = 87;
            }

            background[0].Texture = "level2_1";
            background[1].Texture = "level2_2";
            background[2].Texture = "level2_3";
            background[3].Texture = "level2_4";
            background[4].Texture = "level2_5";
            background[5].Texture = "level2_6";

            //set the object manager reference
            objmgr = mgr;
            attacks = attackmgr;

            //NOW, do some object setups
            objmgr.setZValues(zmin, zmax);
            //-----------------------------------------------------

            Enemy enem;

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(100, 15);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);


            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(100, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);


            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(100, 25);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(100, 30);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);


            enem = new WeeFi();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(700, 8);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);


            enem = new WeeFi();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(700, 16);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new WeeFi();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(700, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new WeeFi();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(700, 31);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(600, 28);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);


            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(600, 22);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(600, 16);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(600, 34);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);



            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(580, 34);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);



            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(560, 34);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);



            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(580, 16);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);



            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(560, 6);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);



            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(500, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);



            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(475, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);



            enem = new TankFlipFlop();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(450, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new WeeFi();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(450, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new WeeFi();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(450, 14);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new WeeFi();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(430, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(350, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(325, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(315, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(300, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(290, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(275, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(250, 26);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            /////

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(350, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(325, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(315, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(300, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(290, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(275, 20);
            objmgr.AddEnemy(enem);
            enemylist.Add(enem);

            enem = new Walker();
            enem.SetAttackMgr(attacks);
            enem.SetInitialPosition(250, 20);
            enemylist.Add(enem);
            objmgr.AddEnemy(enem);


            //--------------------------------------------
            pickup pic;

            pic = new edrink();
            pic.SetSimplePos(240, 0, 15);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(255, 0, 15);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(250, 0, 8);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(720, 0, 8);
            objmgr.AddPickUp(pic);

            pic = new edrink();
            pic.SetSimplePos(720, 0, 4);
            objmgr.AddPickUp(pic);

            pic = new sandwich();
            pic.SetSimplePos(215, 0, 20);
            objmgr.AddPickUp(pic);

            pic = new sandwich();
            pic.SetSimplePos(434, 0, 18);
            objmgr.AddPickUp(pic);


            pic = new sandwich();
            pic.SetSimplePos(570, 0, 6);
            objmgr.AddPickUp(pic);


            pic = new sandwich();
            pic.SetSimplePos(600, 0, 15);
            objmgr.AddPickUp(pic);

            objmgr.setZValues(zmin, zmax);
            gomess = new GoLvlMessageL();
            lvlcomplete = new LevelComplete();
            intromess = new LvlTwoMess();
            intromess.Activate(50, 50);
            
        }

        public override bool UpdateLevel()
        {

            if (!intromess.isFinished())
                intromess.Update();
            else
            {
                if (gomess.isRun())
                {
                    if (!gomess.isFinished())
                        gomess.Update();
                }
                else
                    gomess.Activate(20, 50);
            }


       

            //check for victory conditions
          
            if (objmgr.EnemiesDead())//the victory condition
            {//message stuff
                if (lvlcomplete.isActive == true)
                {
                    if (lvlcomplete.isFinished())
                    {
                        return true; //finish the level after displaying the message
                    }
                    lvlcomplete.Update();
                }
                else
                    lvlcomplete.Activate(55, 40);

            }

            return false;
            //return true when done     

        }

    }
}
