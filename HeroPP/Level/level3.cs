﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNACS1Lib;
namespace Hero_Namespace
{
    class level3 : Level
    {
        public enum waves { wave1, wave2, wave3, wave4, wave5, wave6, wave7, wave8, wave9, wave10 };
        public waves wavestate;
        public bool done = false;
        public bool spawn = true;

        DefendLvlMessage defendmess;
        GameComplete lvlcomplete;
        LvlThreeMess intromess;

        public level3()
        {
            zmin = 0.0f;
            zmax = 32f;

            numberofbhimages = 6;
            mLevelWidth = 144;  // pixel width of the level
            wavestate = waves.wave1;

          

        }

        public override float getHeroPos()
        {
            return 0f;
        }

        public override float getCamPos()
        {
            return 0f;
        }

        public override void InitializeLevel(GameObjManager mgr, AttackManager attackmgr)
        {
            done = false;
            wavestate = waves.wave1;
            XNACS1Base.PlayBackgroundAudio("lvl3_bg", 0.5f);
            background = new XNACS1Rectangle[numberofbhimages];
            for (int i = 0; i < numberofbhimages; i++)
            {
                background[i] = new XNACS1Rectangle();
                background[i].CenterX = 72.5f + (float)(i * 145);
                background[i].CenterY = 43.5f;
                background[i].Width = 145f;
                background[i].Height = 87;
            }

            background[0].Texture = "level2_1";

            //set the object manager reference
            objmgr = mgr;
            attacks = attackmgr;

            //NOW, do some object setups
            objmgr.setZValues(zmin, zmax);

            defendmess = new DefendLvlMessage();
            lvlcomplete = new GameComplete();
            intromess = new LvlThreeMess();
            intromess.Activate(50, 50);
           
        }

        public override bool UpdateLevel()
        {



            if (!intromess.isFinished())
                intromess.Update();
            else
            {
                if (defendmess.isRun())
                {
                    if (!defendmess.isFinished())
                        defendmess.Update();
                }
                else
                    defendmess.Activate(50, 50);
            }

            Enemy enem;
            pickup pic;
            //check for victory conditions
            if (!done)
            {
                switch (wavestate)
                {
                        
                    case waves.wave1:

                        if (spawn)
                        {
                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave2;
                            spawn = true;
                        }

                        break;

                    case waves.wave2:

                        if (spawn)
                        {
                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 5;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            pic = new edrink();
                            pic.SetSimplePos(10, 0, 15);
                            objmgr.AddPickUp(pic);


                            pic = new sandwich();
                            pic.SetSimplePos(10, 0, 20);
                            objmgr.AddPickUp(pic);

                            pic = new edrink();
                            pic.SetSimplePos(20, 0, 25);
                            objmgr.AddPickUp(pic);



                            objmgr.setZValues(zmin, zmax);

                            spawn = false;

                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave3;
                            spawn = true;
                        }

                        break;

                    case waves.wave3:

                        if (spawn)
                        {
                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            pic = new edrink();
                            pic.SetSimplePos(10, 0, 15);
                            objmgr.AddPickUp(pic);


                            pic = new sandwich();
                            pic.SetSimplePos(10, 0, 20);
                            objmgr.AddPickUp(pic);

                            pic = new edrink();
                            pic.SetSimplePos(20, 0, 25);
                            objmgr.AddPickUp(pic);


                            pic = new sandwich();
                            pic.SetSimplePos(20, 0, 30);
                            objmgr.AddPickUp(pic);

                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave4;
                            spawn = true;
                        }
                        break;

                    case waves.wave4:

                        if (spawn)
                        {
                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;



                            pic = new sandwich();
                            pic.SetSimplePos(10, 0, 20);
                            objmgr.AddPickUp(pic);

                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave5;
                            spawn = true;
                        }
                        break;

                    case waves.wave5:

                        if (spawn)
                        {
                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 5;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;
                                                     

                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave6;
                            spawn = true;
                        }
                        break;

                    case waves.wave6:

                        if (spawn)
                        {
                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 5;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 5;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            pic = new edrink();
                            pic.SetSimplePos(10, 0, 15);
                            objmgr.AddPickUp(pic);


                            pic = new sandwich();
                            pic.SetSimplePos(10, 0, 20);
                            objmgr.AddPickUp(pic);

                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave7;
                            spawn = true;
                        }
                        break;

                    case waves.wave7:

                        if (spawn)
                        {
                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;



                            pic = new sandwich();
                            pic.SetSimplePos(10, 0, 20);
                            objmgr.AddPickUp(pic);

                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave8;
                            spawn = true;
                        }
                        break;

                    case waves.wave8:

                        if (spawn)
                        {
                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            pic = new edrink();
                            pic.SetSimplePos(10, 0, 15);
                            objmgr.AddPickUp(pic);


                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave9;
                            spawn = true;
                        }
                        break;

                    case waves.wave9:

                        if (spawn)
                        {
                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }

                        if (objmgr.EnemiesDead())
                        {
                            wavestate = waves.wave10;
                            spawn = true;
                        }
                        break;

                    case waves.wave10:

                        if (spawn)
                        {
                            
                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new Walker();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 15);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop(); ;
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new TankFlipFlop();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 5;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 10);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 20);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;


                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 25);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 4;

                            enem = new WeeFi();
                            enem.SetAttackMgr(attacks);
                            enem.SetInitialPosition(120, 30);
                            enemylist.Add(enem);
                            objmgr.AddEnemy(enem);
                            enem.chase = true;
                            enem.EnLevel = 5;
                            
                            Level3Boss boss;
                            boss = new Level3Boss();
                            boss.SetAttackMgr(attacks);
                            boss.SetInitialPosition(140, 24);
                            enemylist.Add(boss);
                            objmgr.AddEnemy(boss);
                            boss.chase = true;

                            pic = new edrink();
                            pic.SetSimplePos(10, 0, 15);
                            objmgr.AddPickUp(pic);


                            pic = new sandwich();
                            pic.SetSimplePos(10, 0, 20);
                            objmgr.AddPickUp(pic);

                            pic = new edrink();
                            pic.SetSimplePos(20, 0, 25);
                            objmgr.AddPickUp(pic);


                            pic = new sandwich();
                            pic.SetSimplePos(20, 0, 30);
                            objmgr.AddPickUp(pic);

                            objmgr.setZValues(zmin, zmax);

                            spawn = false;
                        }


                        if (objmgr.EnemiesDead())
                        {
                            done = true;
                        }
                        break;
                }

            }

            if (done)//the victory condition
            {//message stuff
                if (lvlcomplete.isActive == true)
                {
                    if (lvlcomplete.isFinished())
                    {
                        return true; //finish the level after displaying the message
                    }
                    lvlcomplete.Update();
                }
                else
                    lvlcomplete.Activate(55, 30);

            }

            return false;
            //return true when done     

        }

    }
}
