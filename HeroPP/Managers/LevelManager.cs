﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

/*
 * The level manager does most of the heavy lifting in the game
 * it creates the heroes, according to the number of players it was given
 * 
 * IT MUST CONTROL
 *GameObjManager
 *All Levels
 *CameraController
 *PauseMenu
 * 
 * 
 */
namespace Hero_Namespace
{
    class LevelManager
    {
        //gameobjmanager is the game engine which must be updated
        //it also must own all of the objects in the game.
        GameObjManager GameObjMan;

        //control sets
        GameController control1;
        int play1controlling;

        GameController control2;
        int play2controlling;

        private int[] mPlayerSelection;
        int numberofplayers;
        int numberofwins = 0;

        //POTENTIAL HEROS
        Hero[] heroArray;


     //create an attack manager class
        AttackManager AttackMgr = new AttackManager();

        //camera control
        CameraControl camera = new CameraControl();

        //pause menu
        PauseMenu pause = new PauseMenu();

        //LEVEL INFORMATION
        int numberoflevels = 3;
        int currentlevel = -1;
        Level[] mArrayOfLevels;
        
        //STATE INFORMATION
        enum levelstatus { waiting, paused, playing, transition }
        levelstatus state;
        private int transitiontimer = 100;//used in the update when the heros die
        //HEALTHBAR
        CharacterHUD[] heroBar;
        static float heroBarWidth = 25;
        static float heroBarHeight = 6;
        static float heroBarMargin = 3;

        //lifebar
        lives_hud lifehud;
        respawn_message resp_message = new respawn_message();

        //--------------------ALL PUBLIC FUNCTIONS------------------------
        public LevelManager(int players)
        {
            //shouldn't do much here
            //
            //just set up the initial stuff like the array
            //and load the levels into them
            mArrayOfLevels = new Level[numberoflevels];
            numberofplayers = players;

            GameObjMan = new GameObjManager();
            GameObjMan.SetAttacks(AttackMgr);

            //load individual levels into the array
            mArrayOfLevels[0] = new level1();
            mArrayOfLevels[1] = new level2();
            mArrayOfLevels[2] = new level3();
            state = levelstatus.waiting;

            mPlayerSelection = new int[players];

                heroBar = new CharacterHUD[numberofplayers];

        }

        //sets a reference to a specific controller
        public void SetController(int number, GameController con1)
        {
            switch (number)
            {
                case 1:
                    control1 = con1;
                    break;
                case 2:
                    control2 = con1;
                    break;
                case 3:
                
                    break;
                case 4:
                    break;
            }

          pause.setController(control1);
        }

        public void ResetCurrentLevel()
        {
            //level done, enemies removed from draw set
            mArrayOfLevels[currentlevel].UnloadLevel();
            //remove from physics
            GameObjMan.ClearAll();
            //remove from camera
            camera.ClearCameraList();

            foreach (CharacterHUD h in heroBar)
            {
                h.Clear();
            
            }
            //start the game at the level

            StartGame(currentlevel + 1);
        
        
        }

        public void SetPlayer(int player, int selection)
        {
            mPlayerSelection[player - 1] = selection;
        }

        //update the game
        public bool Update()
        {

            //detecting to pause
            if (control1.mStartPress && state != levelstatus.paused)
            {
                state = levelstatus.paused;                
                pause.InitializeMenu(new Vector2 (camera.GetCameraPos().X + HeroPP.World.WorldDimension.X / 2, camera.GetCameraPos().Y + HeroPP.World.WorldDimension.Y / 2));
                pause.PauseGame();
            }


            //----------------
            switch (state)
            { 
                case levelstatus.paused:
                    //do paused stuff
                    if (pause.Update())
                        state = levelstatus.playing;
                    //if quit is selected, return true
                    if (pause.m_PauseState == PauseMenu.PauseState.Exiting)
                    {
                        camera.SetDefaultPosY(0.0f);
                        camera.ClearCameraList();
                        camera.ResetCamera();
                        return true;                        
                    }
                    if (pause.m_PauseState == PauseMenu.PauseState.Resetting)
                    {
                        pause.m_PauseState = PauseMenu.PauseState.UnPaused;
                        ResetCurrentLevel();
                    }
                    break;
                case levelstatus.playing:
                    
                    //do level update
                    if (UpdateCurrentLevel())
                    {

                        if (currentlevel + 2 > numberoflevels)
                        {
                            //you ran through all of the levels!! you must rock!!!
                            if (transitiontimer <= 0)
                            {
                                transitiontimer = 100;
                                camera.ResetCamera();

                                numberofwins++;

                                return true;
                            }
                            else
                                transitiontimer--;
                        }
                        else
                        {
                            EndCurrentLevel();
                            StartGame(currentlevel + 2);
                        }
                     }
                    if (GameObjMan.HeroesDead())
                    {
                        if (transitiontimer <= 0)
                        {
                            ResetCurrentLevel();
                            transitiontimer = 100;
                        }
                        else
                            transitiontimer--;
                    }   
                        UpdateHeroes();         ////////////update heroes handles controls
                    GameObjMan.Update();
                    AttackMgr.Update();     //update the attacks
                                             //if you beat the game, return true
                    
                    
                    break;
                case levelstatus.waiting:
                    break;
            }
            return false;

        }
        //startgame
        public void StartGame(int levelnumber)
        {
            //initialize the objmanager class
  //then move to the level once everything is loaded up
              //first time loading a level

            if (levelnumber > numberoflevels)
                return;


            if (currentlevel == -1)
            {
               

                camera.ResetCamera();
                //initilize the level by making it the current state
                currentlevel = levelnumber - 1;
                //initialize the level by sending it the gameobjman
                mArrayOfLevels[currentlevel].InitializeLevel(GameObjMan, AttackMgr);
                //then call the level initialize function to load the level objects into
                //the game manager class
 //set the lives when moving to the first level and the first level only
                lifehud = new lives_hud();
                lifehud.SetLives(2);
                lifehud.Activate(70, 62);
                //assume that all objects are in the game obj manager class
                //SET ZINDEXES IN THE GAME OBJ MANAGER CLASS
                GameObjMan.setZValues(mArrayOfLevels[currentlevel].zmin, mArrayOfLevels[currentlevel].zmax);
            }
            else
            {
                //unload the current level
                mArrayOfLevels[currentlevel].UnloadLevel();

                //then load the new one
                camera.ResetCamera();
                //initilize the level by making it the current state
                currentlevel = levelnumber - 1;
                //initialize the level by sending it the gameobjman
                mArrayOfLevels[currentlevel].InitializeLevel(GameObjMan, AttackMgr);
                //then call the level initialize function to load the level objects into
                //the game manager class
                lifehud.SetLives(2);
                //assume that all objects are in the game obj manager class
                //SET ZINDEXES IN THE GAME OBJ MANAGER CLASS
                GameObjMan.setZValues(mArrayOfLevels[currentlevel].zmin, mArrayOfLevels[currentlevel].zmax);
                foreach (CharacterHUD h in heroBar)
                {
                    if (h != null)
                        h.BringToFront();
                
                }
                //dont forget to give it the gameobjmanager reference!
                
            }
            //create heroes
            createHeroes();
            //send hero references to obj manager

            state = levelstatus.playing;

            


        }
        //--------------------private functions-------------------------


        private void EndCurrentLevel()
        {
            mArrayOfLevels[currentlevel].UnloadLevel();
            //remove from physics
            GameObjMan.ClearAll();
            //remove from camera
            camera.ClearCameraList();

            foreach (CharacterHUD h in heroBar)
            {
                h.Clear();

            }

        }

        
        //update the current level and also check if the victory condition
        //is met. If it is met, call move to level
        private bool UpdateCurrentLevel()
        {
            #region update the character health
            for (int i = 0; i < heroArray.Length; ++i)
            {
                heroBar[i].UpdateHealthBarStat(heroArray[i].GetPercentHealth());
                heroBar[i].UpdateEnergyBarStat(heroArray[i].GetPercentEnergy());
            }
            #endregion
            

            bool returnbool = mArrayOfLevels[currentlevel].UpdateLevel();
            //also update the camera
            camera.UpdateCamera(mArrayOfLevels[currentlevel].LevelWidth());

            //this is where the life system works
            lifehud.Update();

             bool deadhero = false;
            //search to see if there are any dead heros
            foreach (Hero h in heroArray)
                {
                    if (!h.isActive())
                        deadhero = true;
                 }

            if (deadhero && resp_message.isFinished() && lifehud.LivesRemain())
            {
                resp_message.Activate(50, 50);
            }
            else
                resp_message.Update();


            if (deadhero)
            {
                foreach (Hero h in heroArray)
                {
                    if (!h.isActive() && h.RequestLife && lifehud.RemoveLife(1))
                        h.ReviveHero();
                }
            
            }

            return returnbool;


        }

        //create two heroes per person
        private void createHeroes()
        {
            camera.SetDefaultPosY(mArrayOfLevels[currentlevel].getCamPos());
            camera.ResetCamera();

            heroArray = new Hero[numberofplayers];
            float leftHero = heroBarMargin + heroBarWidth / 2;
            float rightHero = 2 * heroBarMargin + 1.5f * heroBarWidth;
            float[] position = new float[numberofplayers];
            position[0] = leftHero;
            position[1] = rightHero;
            for (int i = 0; i < numberofplayers; i++)
            {
                if (mPlayerSelection[i] == (int)SubMenuCharacterSelection.Selection.negative)//neg hero creation
                { 
                    heroArray[i] = new HeroNegative();
                    heroBar[i] = new CharacterHUD(
                        position[i],
                        XNACS1Base.World.WorldMax.Y - heroBarHeight/2,
                        heroBarWidth, heroBarHeight, "hero_HUD_neg");
                    GameObjMan.AddHero(heroArray[i]);
                    camera.AddToCamera(heroArray[i]);
                    camera.AddHUDToCamera(heroBar[i]);
                    heroArray[i].SetAttackMgr(AttackMgr);
                    GameObjMan.setZValues(mArrayOfLevels[currentlevel].zmin, mArrayOfLevels[currentlevel].zmax);
                }
                else if (mPlayerSelection[i] == (int) SubMenuCharacterSelection.Selection.positive)
                {
                    heroArray[i] = new HeroWhite();
                    heroArray[i].CenterX = heroArray[i].mPosition.X = mArrayOfLevels[currentlevel].getHeroPos();
                    heroBar[i] = new CharacterHUD(
                        position[i],
                        XNACS1Base.World.WorldMax.Y - heroBarHeight / 2,
                        heroBarWidth, heroBarHeight, "hero_HUD_pos");
                    GameObjMan.AddHero(heroArray[i]);
                    camera.AddToCamera(heroArray[i]);
                    camera.AddHUDToCamera(heroBar[i]);
                    heroArray[i].SetAttackMgr(AttackMgr);
                    GameObjMan.setZValues(mArrayOfLevels[currentlevel].zmin, mArrayOfLevels[currentlevel].zmax);
                
                }
                else if (mPlayerSelection[i] == (int)SubMenuCharacterSelection.Selection.sung)
                {
                    heroArray[i] = new HeroSung();
                    heroArray[i].CenterX = heroArray[i].mPosition.X = mArrayOfLevels[currentlevel].getHeroPos();
                    heroBar[i] = new CharacterHUD(
                            position[i],
                            XNACS1Base.World.WorldMax.Y - heroBarHeight / 2,
                            heroBarWidth, heroBarHeight, "hero_HUD_sung");
                    GameObjMan.AddHero(heroArray[i]);
                    camera.AddToCamera(heroArray[i]);
                    camera.AddHUDToCamera(heroBar[i]);
                    heroArray[i].SetAttackMgr(AttackMgr);
                    GameObjMan.setZValues(mArrayOfLevels[currentlevel].zmin, mArrayOfLevels[currentlevel].zmax);
                }
                    /*
                else if (mPlayerSelection[i] == Color.Green)
                {
                    heroArray[i] = new HeroPositive();
                    heroArray[i].mPosition.X = mArrayOfLevels[currentlevel].getHeroPos();
                    heroBar[i] = new CharacterHUD(
                            2 * heroBarMargin + 1.5f * heroBarWidth,
                            XNACS1Base.World.WorldMax.Y - heroBarHeight / 2,
                            heroBarWidth, heroBarHeight, "hero_HUD_pos");
                    GameObjMan.AddHero(heroArray[i]);
                    camera.AddToCamera(heroArray[i]);
                    camera.AddHUDToCamera(heroBar[i]);
                    heroArray[i].SetAttackMgr(AttackMgr);
                    GameObjMan.setZValues(mArrayOfLevels[currentlevel].zmin, mArrayOfLevels[currentlevel].zmax);
                }*/
             

            }
            //player one controls hero 0 initially
            play1controlling = 0;
            play2controlling = 1;
            
        }

        //---------------------send the controls to the heroes only!!-----
        private void UpdateHeroes()
        {
            heroArray[play1controlling].ControlUpdate(control1);
            heroArray[play2controlling].ControlUpdate(control2);
        }

        public int getWins()
        {
            return numberofwins;
        }
    }
}
