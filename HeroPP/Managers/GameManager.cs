﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XNACS1Lib;
/*
 * Game Manager directs user inputs, runs the level manager, and runs the menu
 * 
 * its main job is to switch between the menu and the level manager
 * 
 * HAS-------
 * MainMenu
 * LevelManager
 * 
 * ----------------------
 * */
namespace Hero_Namespace
{
    class GameManager
    {
        //it will always be updated
        //reference to game controller 1
        private GameController controller1;
        private GameController controller2;
        private GameController controller3;
        private GameController controller4;

        int numberofwins = 0;

        private int[] mPlayerSelection;

        //set up the states for the overall state machine
        public enum GameState { playing, menu, quit };
        private GameState state;

        //SET up what the game manager controlls
        private MainMenu menu;                      //////////////////////////////////////////////////////////////////////////////////////////////////
        private LevelManager lvlmgr;
        //---------------------------PUBLIC FUNCTIONS---------------------

        //initialize the game manager
        public GameManager(GameController con1, GameController con2, GameController con3, GameController con4)
        {
            //set controller reference
            controller1 = con1;
            controller2 = con2;
            controller3 = con3;
            controller4 = con4;
            //set the menu controller and create a new menu
            menu = new MainMenu(new GameController[] {con1, con2, con3, con4});

            InitializeMenu();//start off with the menu          //////////////////////////////////////////////////////////////////////////////////////////////////
            //InitializeLevelManager(1,1);
        }
      

        //run the logic of the game, are you in menu or in game????
        public void Update()
        {
            switch (state)
            {
                case GameState.menu:
                    
                    UpdateMenu();
                    break;

                case GameState.playing:
                    UpdateLevelMangager();
                    break;
            }
        }

        // determine whether to exit or not
        public bool ShouldExit()
        {
            return(state == GameState.quit);
        }

        //--------------PRIVATE FUNCTIONS--------------------------------



        //give the main menu what it needs to run, and tell it to run
        private void InitializeMenu()
        {
           state = GameState.menu;
           //
           menu.InitializeMenu();               ////////////////////////////////////////////////////////////////////////////////////////////////// 
           
        }
        //return TRUE WHEN DONE

        //when in the menu state, update it
        private void UpdateMenu()
        {

            //update the menu and detect if the user has selected to play.
            if (menu.Update())                                                      
            {
                //if the user wants to start the game, get the player numbers and such              
                if (menu.mState == MainMenu.Selection.exitGame)                                    
                {                    
                    state = GameState.quit;
                    return;
                }
                //start level 1, one player MAY CHANGE
                InitializeLevelManager(1, menu.GetPlayersSelection());
                
                //InitializeLevelManager(1, 2);
            }
        }
        //initiliaze the level manager and start playing!
        private void InitializeLevelManager(int level, int players)
        {
            lvlmgr = new LevelManager(players);

            mPlayerSelection = new int[players];

            //link the lvlmgr with controller 1
            switch (players)
            {
                case 1:
                    lvlmgr.SetController(1, controller1);
                    mPlayerSelection[0] = menu.Player1Selects();
                    lvlmgr.SetPlayer(1, menu.Player1Selects());
                    break;

                case 2:
                    lvlmgr.SetController(1, controller1);
                    lvlmgr.SetController(2, controller2);
                    mPlayerSelection[0] = menu.Player1Selects();
                    mPlayerSelection[1] = menu.Player2Selects();
                    lvlmgr.SetPlayer(1, menu.Player1Selects());
                    lvlmgr.SetPlayer(2, menu.Player2Selects());
                    break;
                }

            state = GameState.playing;
            lvlmgr.StartGame(level);        
        }

        //RETURN TRUE WHEN DONE
        //this means when the levels are complete, or user quits,
        //this will return true
        //update the level manager with whatever it needs
        private bool UpdateLevelMangager()
        {
            if (lvlmgr.Update())
            {
                checkWins();
                menu.InitializeMenu();
                state = GameState.menu;
                
                return true;
            }
            else
                return false;
        }
        
        public void checkWins()
        {
            if (numberofwins != lvlmgr.getWins())
            {
                numberofwins = lvlmgr.getWins();
                menu.setWins();
            }
        }
    }
}