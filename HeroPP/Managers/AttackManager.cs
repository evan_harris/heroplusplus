﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hero_Namespace
{
    class AttackManager
    {

        private static int totalattacks = 50;

        //HERO Pri (neg) ATTACK

        public PriShot[] mHeroPriAttacks = new PriShot[totalattacks];
        public SecShot[] mHeroSecAttacks = new SecShot[totalattacks];
        public TriShot[] mHeroTriAttacks = new TriShot[totalattacks];
        public AOEShot[] mHeroAOEAttacks = new AOEShot[totalattacks];
        public CritShot[] mHeroCritAttacks = new CritShot[totalattacks];
        private int mCurrentHeroPriAttack = 0;
        private int mCurrentHeroSecAttack = 0;
        private int mCurrentHeroTriAttack = 0;
        private int mCurrentHeroCritAttack = 0;
        private int mCurrentHeroAOEAttack = 0;

        //

        public AttackManager()
        {
            for (int i = 0; i < totalattacks; i++)
            {
                mHeroPriAttacks[i] = new PriShot();
                mHeroSecAttacks[i] = new SecShot();
                mHeroTriAttacks[i] = new TriShot();
                mHeroCritAttacks[i] = new CritShot();
                mHeroAOEAttacks[i] = new AOEShot();
            }
        
        
        }

        public PriShot GetNextNegPri()
        {
            mCurrentHeroPriAttack++;
            if (mCurrentHeroPriAttack >= totalattacks)
                mCurrentHeroPriAttack = 0;
            return mHeroPriAttacks[mCurrentHeroPriAttack];
        }
        public CritShot GetNextNegCrit()
        {
            mCurrentHeroCritAttack++;
            if (mCurrentHeroCritAttack >= totalattacks)
                mCurrentHeroCritAttack = 0;
            return mHeroCritAttacks[mCurrentHeroCritAttack];
        }
        public SecShot GetNextNegSec()
        {
            mCurrentHeroSecAttack++;
            if (mCurrentHeroSecAttack >= totalattacks)
                mCurrentHeroSecAttack = 0;
            return mHeroSecAttacks[mCurrentHeroSecAttack];
        }
        public TriShot GetNextNegTri()
        {
            mCurrentHeroTriAttack++;
            if (mCurrentHeroTriAttack >= totalattacks)
                mCurrentHeroTriAttack = 0;
            return mHeroTriAttacks[mCurrentHeroTriAttack];
        }

        public AOEShot GetNextNegAOE()
        {
            mCurrentHeroAOEAttack++;
            if (mCurrentHeroAOEAttack >= totalattacks)
                mCurrentHeroAOEAttack = 0;
            return mHeroAOEAttacks[mCurrentHeroAOEAttack];
        }

        public void Update()
        {
            foreach (PriShot attack in mHeroPriAttacks)
            {
                attack.Update();
            
            }
            foreach (SecShot attack in mHeroSecAttacks)
            {
                attack.Update();

            }
            foreach (TriShot attack in mHeroTriAttacks)
            {
                attack.Update();

            }
            foreach (CritShot attack in mHeroCritAttacks)
            {
                attack.Update();

            }
            foreach (AOEShot attack in mHeroAOEAttacks)
            {
                attack.Update();
            }
        
        }


    }
}
