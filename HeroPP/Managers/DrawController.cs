﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNACS1Lib;

/*
draw controller will hold references to all objects which are in the world
 *it controlls the order in things are drawn
 *
 * how to use this class:
 * 
 * first set the zmin and the zmax USING THE PUBLIC FUNCTIONS
 * then use the add to draw set function- thats it
 * oh yeah, you have to update it when you want as well
*/
namespace Hero_Namespace
{
    class DrawController
    {
        
        private List<GameObject>[] objectlist;  //the array of lists which hold
        private int zlayers = 50;       //the number of indecrements in the z direction

        private float zmin = 0.0f;
        private float zmax = 50.0f;
        private float zincrement = 1;

        //------------------public stuff to use----------------------------------
        public DrawController() { 
            //initialize array
        objectlist = new List<GameObject>[zlayers];
        for(int i=0; i<zlayers ; i++)
        {
            objectlist[i] = new List<GameObject>();
        }
            //figure out the increments of the z index
        

        }


        public void setZmin(float min)
        {
            zmin = min;
            calczincrement();
        
        }
        public void setZmax(float max)
        {
            zmax = max;
            calczincrement();
        }

        public void AddToDraw(GameObject obj)
        {
        //find the correct z index and add it to the correct list

            int zlayer = FindZlayer(obj.getZindex());
            if(zlayer < zlayers && zlayer >= 0)
            objectlist[zlayer].Add(obj);
        }
        public void ClearDrawSet()
        {
        for(int i=0; i < zlayers; i++)
            {
                objectlist[i].Clear();
                //objectlist[i] = null;
            }
        
        }
        public void UpdateDraw()
        {
            SortList();//sort the list of objects



            for (int i = zlayers-1; i >= 0; i--)
            {
                foreach (GameObject obj in objectlist[i])
                {
                    if (obj.isActive())
                    {

                   
                        obj.BringToTop();
                    }
                }
            
            }
        
        }
        //-----------------------BEgin private members------------------------------
        private void calczincrement()
        {
            zincrement = (zmax - zmin) / zlayers;
        }
        //will return the layer in which the player
        //belongs to, given its z value
        private int FindZlayer(float zind)
        {
            return (int)Math.Abs((zmin - zind) / zincrement);
        }
        //this is the nested loop used to sort all of the objects
        private void SortList()
        {
            //loop through every layer
            for (int i = 0; i < zlayers; i++)
            {
                //check every object on the layer and determine if it is in the wrong spot
                for (int j = 0; j < objectlist[i].Count; j++ )
                {
                    if (FindZlayer(objectlist[i][j].getZindex()) != i && FindZlayer(objectlist[i][j].getZindex()) < zlayers && FindZlayer(objectlist[i][j].getZindex()) >= 0 && objectlist[i][j].isActive())
                    {

                             objectlist[FindZlayer(objectlist[i][j].getZindex())].Add(objectlist[i][j]);//add to another
                            objectlist[i].Remove(objectlist[i][j]);//remove from one layer
                    }
                }            
            }        
        }
    }
}
