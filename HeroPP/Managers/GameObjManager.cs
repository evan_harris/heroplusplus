﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class GameObjManager
    {
        //DAMN, ok here we go!!!

        //IMPLEMENT DRAW SET
        DrawController drawcon = new DrawController();

        //start with heros
        List<Hero> herolist = new List<Hero>();

        //enemy storage
        List<Enemy> enemylist = new List<Enemy>();

        //specstator array
        List<Spectator> spectatorarr = new List<Spectator>();

        //pickup array
        List<pickup> pickuparr = new List<pickup>();

        private AttackManager attacks;

        //HERO minus attack

        private PriShot[] primaryattacks;
        private SecShot[] secondaryattacks;
        private CritShot[] critattacks;
        private TriShot[] tertiaryattacks;
        private AOEShot[] AOEattacks;
            
            //--------------------------INTERFACES------PUBLIC-----------------------
        public GameObjManager()
        { }

        //set the zvalues of all of the objects
        public void setZValues(float zmin, float zmax)
        {
            foreach (Hero currenthero in herolist)
            {
                currenthero.mMinZ = zmin;
                currenthero.mMaxZ = zmax;
            
            }
            foreach (Enemy currentenemy in enemylist)
            {
                currentenemy.mMinZ = zmin;
                currentenemy.mMaxZ = zmax;
            
            }
            foreach (Spectator currentspec in spectatorarr)
            {
                currentspec.mMaxZ = zmax;
                currentspec.mMinZ = zmin;
            
            }

            drawcon.setZmax(zmax);
            drawcon.setZmin(zmin);

        }
        public void SetAttacks(AttackManager attmgr)
        {
            attacks = attmgr;
            primaryattacks = attacks.mHeroPriAttacks;
            secondaryattacks = attacks.mHeroSecAttacks;
            critattacks = attacks.mHeroCritAttacks;
            tertiaryattacks = attacks.mHeroTriAttacks;
            AOEattacks = attacks.mHeroAOEAttacks;
            foreach (GameObject att in primaryattacks)
                drawcon.AddToDraw(att);
            foreach (GameObject att in critattacks)
                drawcon.AddToDraw(att);
            foreach (GameObject att in secondaryattacks)
                drawcon.AddToDraw(att);
            foreach (GameObject att in tertiaryattacks)
                drawcon.AddToDraw(att);
            foreach (GameObject att in AOEattacks)
                drawcon.AddToDraw(att);

        }
        public void AddHero(Hero inhero)
        {
            herolist.Add(inhero);
            drawcon.AddToDraw(inhero);
        }

        public bool HeroesDead()
        {
            foreach (Hero h in herolist)
                if (h.isActive())
                    return false;
            return true;
        
        }
        public bool EnemiesDead()
        {
            foreach (Enemy e in enemylist)
                if (e.isActive())
                    return false;
            return true;
        
        }

        public void AddEnemy(Enemy inenemy)
        {
            enemylist.Add(inenemy);
            drawcon.AddToDraw(inenemy);
        }

        public void AddSpectator(Spectator inspec)
        {
            spectatorarr.Add(inspec);
            drawcon.AddToDraw(inspec);
        
        }
        public void AddPickUp(pickup inpickup)
        {
            pickuparr.Add(inpickup);
            drawcon.AddToDraw(inpickup);
        
        }
        public void ClearEnemies()
        {
            drawcon.ClearDrawSet();
            enemylist.Clear();

            //add back in everything to the draw controller except the enemies
            foreach (Hero h in herolist)
            {
                drawcon.AddToDraw(h);
                h.SetInitialPosition(10, 2);
            }
            foreach (Spectator s in spectatorarr)
                drawcon.AddToDraw(s);
            foreach (pickup p in pickuparr)
                drawcon.AddToDraw(p);


        }
        public void ClearAll()
        {

            drawcon.ClearDrawSet();
            foreach (Enemy e in enemylist)
                e.MakeNotActive();
            foreach (Hero h in herolist)
                h.MakeNotActive();

            enemylist.Clear();
            herolist.Clear();
            spectatorarr.Clear();
         
        
        }
        
        public void Update()
        {
            drawcon.UpdateDraw();

            //SPECTATOR LOOP------------------------------------
            #region spectator
            foreach (Spectator currentspec in spectatorarr)
            {
                if (currentspec.isActive())
                {
                    //calc the closest hero using peters thing(modified)
                    Hero closesthero = null;
                    Vector2 closest = currentspec.Center;

                    #region hero to spectator
                    foreach (Hero currenthero in herolist)
                    {
                        if (currenthero.isActive())
                        {
                            //push if they collide
                            if (currenthero.ObjCollided(currentspec))
                                CollisionMath.pushcharfromchar(currentspec, currenthero);


                            //update the hero
                            if (closest == currentspec.Center)
                            {
                                closesthero = currenthero;
                                closest = currenthero.Center;
                            }
                            else
                            {
                                if (Math.Abs(currentspec.CenterX) - Math.Abs(currenthero.CenterX) < Math.Abs(currentspec.CenterX) - Math.Abs(closest.X))
                                {
                                    closesthero = currenthero;
                                    closest = currenthero.Center;
                                }
                            }
                        }
                    }
                    #endregion

                    //ACTIVATE THE SPECTATOR
                    //if (closesthero != null)
                        currentspec.Update(closesthero);
                }
            }
            #endregion

            //LOOP THROUGH ALL HEROES---------------------------
            #region hero
            foreach (Hero currenthero in herolist)
            {
                if (currenthero.isActive())
                {
                    #region hero on hero
                    //HERO ON HERO
                    foreach (Hero currenthero2 in herolist)
                    {
                        if (currenthero2.isActive() && currenthero.ObjCollided(currenthero2))
                        {
                            //BOUNCE THE TWO
                            CollisionMath.pushcharfromchar(currenthero, currenthero2);
                        }

                    }
                    #endregion

                    #region hero on pickup
                    //HERO ON PICKUP
                    foreach (pickup currpickup in pickuparr)
                    {
                        if (currpickup.isActive() && currpickup.ObjCollided(currenthero))
                        {
                            currpickup.GetPickup(currenthero);
                            XNACS1Base.PlayACue("pickup");
                        }
                    }
                    #endregion

                    #region hero on enemy
                    //HERO ON ENEMY
                    foreach (Enemy currenemy in enemylist)
                    {
                        if (currenemy.isActive())
                        {

                            if (currenemy.ObjCollided(currenthero))//if enemy collides with the hero
                            {
                                //enemy and hero collision
                                //BOUNCE AND DO DAMAGE TO HERO
                                currenthero.CollideDamage();
                                CollisionMath.pushcharfromchar(currenemy, currenthero);
                                currenthero.PushBack();
                            }

                            if (currenthero.IsImmune())
                            {
                                currenthero.DecrementImmuneCounter();

                                if (currenthero.ImmuneCounterLeft() == 0)
                                {
                                    currenthero.ResetImmuneCounter();
                                    currenthero.UndoImmune();
                                }
                            }
                        }
                    }
                    #endregion

                    #region hero on attacks
                    //HERO ON ATTACKS
                    foreach (PriShot attack in primaryattacks)
                    {//-check the attack against the hero
                        if (attack.isActive() && !attack.getOrigin().isHero() && currenthero.ObjCollided(attack))
                        {
                            //attack.transferdamage();
                            currenthero.UpdateHealth((int)attack.transferdamage());
                        }
                    }
                    #endregion

                    //HERO ON PICKUPS
                    //-----------------------------------------------------------

                    //update the hero
                    currenthero.Update();
                }
            }
            #endregion

            
            //--------------------------------$ND OF FIRST LOOP FOR HERO-------------------------------------
            //LOOP THROUGH ENEMIES
            #region enemy
            foreach (Enemy currentenemy in enemylist)
            {
                if (currentenemy.isActive())
                {

                    #region enemy on attacks
                    //-------------ENEMY ON ATTACKS-------------
                    #region prishots
                    foreach (PriShot attack in primaryattacks)
                    {
                        if (attack.isActive())
                            if (currentenemy.ObjCollided(attack))
                            {
                                //if the attack hits and enemy

                                //the hero shot an enemy
                                if (attack.getOrigin().isHero())
                                {

                                    currentenemy.UpdateHealth((int)attack.transferdamage());//(int)attack.transferdamage());

                                }
                                else//the enemy shot another enemy
                                {

                                }
                            }
                    }
                    #endregion

                    #region secshot
                    foreach (SecShot attack in secondaryattacks)
                    {
                        if (attack.isActive())
                        {
                            if (currentenemy.ObjCollided(attack))
                            {
                                //if the attack hits and enemy

                                //the hero shot an enemy
                                if (attack.getOrigin().isHero())
                                {

                                    currentenemy.UpdateHealth((int)attack.transferdamage());//(int)attack.transferdamage());

                                }
                                else//the enemy shot another enemy
                                {

                                }
                            }
                        }
                    }
                    #endregion

                    #region trishot
                    foreach (TriShot attack in tertiaryattacks)
                    {
                        if (attack.isActive())
                        {
                            if (currentenemy.ObjCollided(attack))
                            {
                                //if the attack hits and enemy

                                //the hero shot an enemy
                                if (attack.getOrigin().isHero())
                                {

                                    currentenemy.UpdateHealth((int)attack.transferdamage());//(int)attack.transferdamage());

                                }
                                else//the enemy shot another enemy
                                {

                                }
                            }
                        }
                    }
                    #endregion

                    #region critshot
                    foreach (CritShot attack in critattacks)
                    {
                        if (attack.isActive())
                        {
                            if (currentenemy.ObjCollided(attack))
                            {
                                //if the attack hits and enemy

                                //the hero shot an enemy
                                if (attack.getOrigin().isHero())
                                {

                                    currentenemy.UpdateHealth((int)attack.transferdamage());//(int)attack.transferdamage());

                                }
                                else//the enemy shot another enemy
                                {

                                }
                            }
                        }
                    }
                    #endregion

                    #region aoeshot
                    foreach (AOEShot attack in AOEattacks)
                    {
                        if (attack.isActive())
                        {
                            if (currentenemy.ObjCollided(attack))
                            {
                                //if the attack hits and enemy

                                //the hero shot an enemy
                                if (attack.getOrigin().isHero())
                                {

                                    currentenemy.UpdateHealth((int)attack.transferdamage());//(int)attack.transferdamage());

                                }
                                else//the enemy shot another enemy
                                {

                                }
                            }
                        }
                    }
                    #endregion

                    #endregion

                    #region enemy on enemy
                    //--ENEMY ON ENEMY----------
                    foreach (Enemy currentenemy2 in enemylist)
                    {
                        if (currentenemy.isActive() && currentenemy2.isActive() && currentenemy.ObjCollided(currentenemy2))
                            CollisionMath.pushcharfromchar(currentenemy, currentenemy2);  // ?????????????????
                    }
                    #endregion

                    #region enemy AI
                    //---------------------DUPLACITE -- MOVE TO UPPER LOOP ----------------------
                    Vector2 closest = currentenemy.Center;

                    foreach (Hero currenthero in herolist)
                    {
                        if (currenthero.isActive())
                        {
                            //update the hero
                            if (closest == currentenemy.Center)
                                closest = currenthero.Center;

                            else
                            {
                                if (Math.Abs(currentenemy.CenterX) - Math.Abs(currenthero.CenterX) < Math.Abs(currentenemy.CenterX) - Math.Abs(closest.X))
                                    closest = currenthero.Center;
                            }
                        }
                    }

                    bool updateSpecial = true;
                    foreach (Hero currenthero in herolist)
                    {
                        if(currenthero.isActive())
                        updateSpecial = (updateSpecial && !currenthero.IsAttackSpecial());
                    }
                    
                    if (updateSpecial)
                    {
                        currentenemy.Update(closest);
                    }
                    #endregion

                    currentenemy.UpdateTint();
                }
            }
            #endregion
            //---------------------------END OF SECOND LOOP FOR ENEMY-------------------------------------

            //------------NOW DETECT ATTACKS AND PICKUPS----------------------------
         
        }

     
        //---------------------------PRIVATE-------------HELPERS---------------------------
        




    }
}
