﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class CameraControl
    {
        List<Character> characterarr = new List<Character>();
        List<CharacterHUD> characterbararr = new List<CharacterHUD>();

        //global parameters
        float ViewWidth = 100.0f;
        Vector2 DefaultPos = new Vector2(00.0f, 0.0f);
        public Vector2 CurrentPos = new Vector2(0.0f, 0.0f);
        float CameraSpeed = 0.2f;

        //bool leftbound1 = false;
        //bool leftbound2 = false;
        //bool rightbound1 = false;
        //bool rightbound2 = false;
        //bool leftwindowbound = false;
        //bool rightwindowbound = false;
        //-----------------public interfaces-------------------------
        public CameraControl()
        {
            ResetCamera();
        }

        public void SetDefaultPosY(float Y)
        {
            DefaultPos = new Vector2(Y, 0.0f);
            CurrentPos = DefaultPos;
        }

        public Vector2 GetCameraPos()
        {
            return CurrentPos;
        }

        public void AddToCamera(Character inchar)
        {
            characterarr.Add(inchar);
        }
        public void AddHUDToCamera(CharacterHUD inbar)
        {
            characterbararr.Add(inbar);
        }
        public void ClearCameraList()
        {
            characterarr.Clear();
            characterbararr.Clear();
        }


        //change the camera based on the rectangle and lock the rectangles
        //to the world
        public void UpdateCamera(int tempwidth)
        {
            float currX = CurrentPos.X;

            //first calc the center of the screen
            float xcamcenter = CurrentPos.X + (ViewWidth / 2);
            float xchange = 0;
            bool lBound = false;
            bool  rBound = false;
            float tempchange;
            foreach (Character obj in characterarr)
            {
                if (obj.isActive())
                {
                    //detect the difference in each character
                    tempchange = obj.mPosition.X - xcamcenter;

                    xchange += tempchange;
                    
                    if (XNACS1Base.World.CollideWorldBound(obj) == BoundCollideStatus.CollideLeft)
                    {
                        lBound = true;
                        obj.ClampCharacterWorldBound();
                    }

                    if (XNACS1Base.World.CollideWorldBound(obj) == BoundCollideStatus.CollideRight)
                    {
                        rBound = true;
                        obj.ClampCharacterWorldBound();
                    }
                }
             }


            if (!(rBound || lBound))
            {
                CurrentPos.X += xchange * CameraSpeed;
                if (CurrentPos.X <= 0)
                    CurrentPos.X = 0;
                if (CurrentPos.X + ViewWidth >= tempwidth)
                    CurrentPos.X = currX;

                XNACS1Base.World.SetWorldCoordinate(CurrentPos, ViewWidth);
            }

            #region for each healthbar
            foreach (CharacterHUD rec in characterbararr)
            {
                rec.UpdateCamera();
            }
            #endregion

        }


        public void ResetCamera()
        {
            //always full screen
            XNACS1Base.SetAppWindowPixelDimension(true, 800, 600);     ////////////////////////////////////////////////////////////////////////
            //set to the left of the screen
            XNACS1Lib.XNACS1Base.World.SetWorldCoordinate(DefaultPos, ViewWidth);

            //calculate the correct width depending on the aspect ratio
            ViewWidth = XNACS1Lib.XNACS1Base.World.WorldDimension.X / XNACS1Lib.XNACS1Base.World.WorldDimension.Y * 65;
            
            CurrentPos = DefaultPos;
        }
        //-------------------------private functions-----------------
    }
}
