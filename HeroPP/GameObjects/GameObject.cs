﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class GameObject : XNACS1Rectangle
    {
        //global variables
        protected enum objectstate { active, notactive };
        protected objectstate mObjstate = objectstate.active;
        public Vector3 mPosition; // current position of the game object
        protected float mZDepth;

        public GameObject()
        {
            mPosition = new Vector3();
            mPosition.X = 0;
            mPosition.Y = 0;
            mPosition.Z = 0;
            MakeActive();
            CenterX = 0;
            CenterY = 0;
            mZDepth = 0;
            VelocityX = 0;
            VelocityY= 0;
        }
        //Make the object active to the world
        public void MakeActive()
        {
            mObjstate = objectstate.active;
            ShouldTravel = true;
            AddToAutoDrawSet();
        }

        public virtual void BringToTop()
        {
            this.TopOfAutoDrawSet();
        }

        virtual public bool isCharacter()
        {
            return false;
        }
        public void MakeNotActive()
        {
            RemoveFromAutoDrawSet();
            mObjstate = objectstate.notactive;
            mPosition.X = mPosition.Y = mPosition.Z = 0;
            ShouldTravel = false;
            
        }

        public bool isActive()
        {
            return mObjstate == objectstate.active;
        }
        //returns the z index of the object
        public float getZindex()
        {
            return mPosition.Z;
        }

        public float getXposition()
        {
            return mPosition.X;
        }

        /// <summary>
        /// Check to see if the objects are collided in respect to 
        /// the position X, Y, and Z and the zDepth
        /// </summary>
        /// <param name="rhs"></param>
        /// <returns></returns>
       virtual public bool ObjCollided(GameObject rhs)
        {
            if (this == rhs)
            {
                return false;
            }

            if (this.Collided(rhs))//automatically takes into account x and y coordinates
            {
                //now handle z cooddinate
                if (this.mPosition.Z > rhs.mPosition.Z)
                {
                    if (this.mPosition.Z - (this.mZDepth / 2) <= rhs.mPosition.Z + (rhs.mZDepth / 2))
                        return true;
                }
                else
                {
                    if (rhs.mPosition.Z - (rhs.mZDepth / 2) <= this.mPosition.Z + (this.mZDepth / 2))
                        return true;

                }

            
            
            }

            return false;//return false default

           //-----CRAIGS STUFF--------------
            
            //float variableWidth = 0.3f;
            //#region check to see if we are in the right Z range
            //// check to see if this object is within the depth of the object colliding 
            //if ((mPosition.Z + mZDepth * 0.5f) >= (rhs.mPosition.Z- mZDepth * 0.5f)
            //    && (mPosition.Z - mZDepth * 0.5f) <= (rhs.mPosition.Z + rhs.mZDepth * 0.5f))
            //{
            //    #region check to see if the Y position is within the right range
            //    // check to see if we are within the y range 
            //    if ((mPosition.Y +  0.25*Height) <= (rhs.mPosition.Y + rhs.Height))
            //        //&& (mPosition.Y ) <= (rhs.mPosition.Y + 0.7f * rhs.Height))
            //    {
                    
            //        #region this game object is to the left of the object
            //        // check to see if this object is to the left of the rhs object
            //        if (mPosition.X <= rhs.mPosition.X)
            //        {
            //            // check to see if we are within x range
            //            if ((mPosition.X + (Width * variableWidth)) >= (rhs.mPosition.X - (rhs.Width * variableWidth)))
            //            {
            //                return true;
            //            }
            //        }
            //        #endregion
            //        #region this game object is to the right of the object
            //        else // position.X > rhs.position.X
            //        {
            //            // check to see if we are within x range
            //            if ((mPosition.X - (Width * variableWidth)) <= (rhs.mPosition.X + (rhs.Width * variableWidth)))
            //            {
            //                return true;
            //            }
            //        }
            //        #endregion
            //    }
            //    #endregion
            //}


            //return false;   // the objects have not collided
            //#endregion
            
        }

        public bool ObjBarrier(GameObject rhs)
        {
            if (this == rhs)
            {
                return false;
            }
            float variableWidth = 0.8f;
            #region check to see if we are in the right Z range
            // check to see if this object is within the depth of the object colliding 
            if ((mPosition.Z + mZDepth) >= (rhs.mPosition.Z - mZDepth)
                && (mPosition.Z - mZDepth) <= (rhs.mPosition.Z + rhs.mZDepth))
            {
                #region check to see if the Y position is within the right range
                // check to see if we are within the y range 
                //if ((position.Y + Height) >= (rhs.position.Y)
                //    || (position.Y) <= (rhs.position.Y + Height))
                //{

                #region this game object is to the left of the object
                // check to see if this object is to the left of the rhs object
                if (mPosition.X <= rhs.mPosition.X)
                {
                    // check to see if we are within x range
                    if ((mPosition.X + (Width * variableWidth)) >= (rhs.mPosition.X - (rhs.Width * variableWidth)))
                    {
                        return true;
                    }
                }
                #endregion
                #region this game object is to the right of the object
                else // position.X > rhs.position.X
                {
                    // check to see if we are within x range
                    if ((mPosition.X - (Width * variableWidth)) <= (rhs.mPosition.X + (rhs.Width * variableWidth)))
                    {
                        return true;
                    }
                }
                #endregion
                //}
                #endregion
            }


            return false;   // the objects have not collided
            #endregion
        }

        public void SetSimplePos(float x, float y, float z)
        {
            CenterX = mPosition.X = x;
            mPosition.Z = z;
            CenterY = mPosition.Z + Height / 2 + y;
        
        
        }
    }
}
