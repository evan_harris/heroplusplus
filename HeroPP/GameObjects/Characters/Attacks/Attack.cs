﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;
namespace Hero_Namespace
{
    class Attack : GameObject
    {
        #region attack information
        protected float mDamage;
        protected float mRange;
        #endregion

        protected Vector3 mInitialPos;
        protected enum direction {left,right};
        protected bool mInUse;
        protected direction mTravelDirection;
        protected enum origintype { enemy, hero };
        //protected origintype originstate;
        protected Character mOrigin;
        
        public Attack()
        {
            mInitialPos = new Vector3();
            mInUse = false;
            mTravelDirection = direction.left;
            mDamage = 0;
            mRange = 0;
            mInitialPos.X = 0;
            mInitialPos.Y = 0;
            MakeNotActive();
        }

        //returns the amount of damage that the attack has
        //then destorys the attack
        public virtual float transferdamage()
        {
            MakeNotActive();
            return mDamage;
        }

        public void setRange(float inrange)
        {
            mRange = inrange;
        }

        public Character getOrigin()
        {
            return mOrigin;
        }

    }
}
