﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;

namespace Hero_Namespace
{
    class ParticleShot
    {
        //XNACS1ParticleEmitter.TailEmitter mEmitter;
        protected XNACS1ParticleEmitter.ExplodeEmitter mEmitter;
        protected float mSize = 0.5f;
        protected int particleLife = 20;
        protected float particleSize = 0.25f;

        public ParticleShot(String texture)
        {
            Vector2 center = new Vector2(-20, -20);

            //mEmitter = new XNACS1ParticleEmitter.TailEmitter(center, particleLife, size, texture, Color.AntiqueWhite);
            //mEmitter = new XNACS1ParticleEmitter.ExplodeEmitter(center, particleLife, mSize, texture, Color.Gray, mSize*1.5f);

        }
        public void Update(Vector2 move)
        {
            if (mEmitter.Visible)
            {
                mEmitter.Center = move;
                mEmitter.EmitFrequency = 20;
                mEmitter.ExplodeSpeed = .75f;
                
                mEmitter.Explode(1);
               
                
            }
            
        }
        public void RemoveParticle()
        {
            mEmitter.EmitFrequency = 0;
            mEmitter.CenterY = -20;
            mEmitter.CenterX = -20;
            mEmitter.Visible = false;
            //mEmitter.RemoveEmitterFromUpdateSet();
            
            mEmitter.RemoveFromAutoDrawSet();

            
            mEmitter = null;
            
        }
        public void MakeActive(Vector2 move, String texture)
        {
            mEmitter = new XNACS1ParticleEmitter.ExplodeEmitter(move, particleLife, mSize, texture, Color.Gray, mSize);
            mEmitter.Visible = true;
            //mEmitter.Center = move;
            //mEmitter.Texture = texture;
            //BringToFront();
        }
        private void BringToFront()
        {
            //mEmitter.TopOfAutoDrawSet();
        }

        public bool IsActive()
        {
            return (mEmitter != null);
        }
    }
}
