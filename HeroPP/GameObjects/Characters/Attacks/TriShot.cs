﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class TriShot : Attack
    {
        protected XNACS1Circle mShadow;
        protected ParticleShot mParticle;
        private float mShootSpeed = 1.5f;
        protected String mShadowTexture = "shadow_fire";
        protected int mHitCounter;
        protected int mCountTime;
        protected float Zspeed;
        protected float mMaxZ;
        public TriShot()
        {
            Width = 4f;
            Height = 4f;
            Texture = "pos_shot";
            MakeNotActive();
            CenterX = 0;
            CenterY = -20;
            mParticle = new ParticleShot(Texture);
            mZDepth = 10f;

            #region shadow stuff
            mShadow = new XNACS1Circle();
            mShadow.Visible = false;
            mShadow.CenterX = mPosition.X = CenterX;
            mShadow.CenterY = mPosition.Z = CenterY;// position.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadow.Radius = 2f;

            #endregion
        }

        //detect if the shot should be dead or not
        public void Update()
        {
            //centerx will be auto updated, so assign it to positionx
            mShadow.CenterY += Zspeed;
            CenterY += Zspeed;
            mPosition.Z += Zspeed;
            mPosition.X = CenterX;
            mShadow.CenterX = CenterX;
            //only update if active
            if (mObjstate == objectstate.active)
            {


                
                if ((Math.Abs(mPosition.X - mInitialPos.X) >= mRange ||
                    Math.Abs(mPosition.Z - mInitialPos.Y) >= mRange) ||
                    mPosition.Z >= mMaxZ)
                {
                    MakeNotActive();
                    mParticle.RemoveParticle();
                    mShadow.RemoveFromAutoDrawSet();
                    mShadow.Visible = false;
                }
                else if (mParticle.IsActive())
                {
                    mParticle.Update(Center);
                }
                

            }


        }

        //check to see if the shot is in use
        public bool IsFired()
        {
            return mInUse;
        }

        public override float transferdamage()
        {
                MakeNotActive();
                mParticle.RemoveParticle();
                mShadow.RemoveFromAutoDrawSet();
                mShadow.Visible = false;
                return mDamage;
            
        }

        //initialize the shot
        public void Fire(float XStart, float YStart, float Z, float Xtravel, float Ztravel, Character inorigin, float inrange, float indamage, float maxZ)
        {
            mDamage = indamage;
            mOrigin = inorigin;
            mRange = inrange;
            MakeActive();//from gameobj, set travel
            mInitialPos.X = mPosition.X = CenterX = XStart;
            mParticle.MakeActive(Center, Texture);
            mInitialPos.Z = mPosition.Z = Z;
            
            mShadow.AddToAutoDrawSet();
            mShadow.Visible = true;
            mShadow.TopOfAutoDrawSet();
            mShadow.CenterY = mPosition.Z;
            mPosition.Y = CenterY = YStart;
            mMaxZ = maxZ;
            
                VelocityX = Xtravel;
                Zspeed = Ztravel;
                Zspeed *= 0.5f;
            
        }
    }
}
