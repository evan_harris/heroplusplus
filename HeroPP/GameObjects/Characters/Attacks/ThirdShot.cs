﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class ThirdShot : Attack
    {
        protected XNACS1Circle mShadow;
        protected ParticleShot mParticle;
        private float mShootSpeed = 1.5f;
        protected String mShadowTexture = "shadow_fire";

        public ThirdShot()
        {
            Width = 3f;
            Height = 3f;
            ///Texture = "neg_shot";
            MakeNotActive();
            CenterX = 0;
            CenterY = -20;
            mParticle = new ParticleShot(Texture);
            mZDepth = 8f;

            #region shadow stuff
            mShadow = new XNACS1Circle();
            mShadow.Visible = false;
            mShadow.CenterX = mPosition.X = CenterX;
            mShadow.CenterY = mPosition.Z = CenterY;// position.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadow.Radius = 2f;

            #endregion
        }

        //detect if the shot should be dead or not
        public void Update()
        {
            //centerx will be auto updated, so assign it to positionx
            mPosition.X = CenterX;
            mShadow.CenterX = CenterX;
            //only update if active
            if (mObjstate == objectstate.active)
            {


                /*if (traveldirection == direction.left)
                {
                    if ((initialX - position.X) >= range)
                    {
                        MakeNotActive();
                        mParticle.RemoveParticle();
                        shadow.RemoveFromAutoDrawSet();
                        shadow.Visible = false;
                        
                    }
                }
                else if (traveldirection == direction.right)
                {
                    if (position.X - initialX >= range)
                    {
                        MakeNotActive();
                        mParticle.RemoveParticle();
                        shadow.RemoveFromAutoDrawSet();
                        shadow.Visible = false;
                    }
                }*/
                if (Math.Abs(mPosition.X - mInitialX) >= mRange)
                {
                    MakeNotActive();
                    mParticle.RemoveParticle();
                    mShadow.RemoveFromAutoDrawSet();
                    mShadow.Visible = false;
                }
                else if (mParticle.IsActive())
                    mParticle.Update(Center);

            }


        }

        //check to see if the shot is in use
        public bool IsFired()
        {
            return mInUse;
        }

        public override float transferdamage()
        {
            MakeNotActive( );
            mParticle.RemoveParticle();
            mShadow.RemoveFromAutoDrawSet();
            mShadow.Visible = false;
            return mDamage;
        }

        //initialize the shot
        public void Fire(float XStart, float YStart, float Z, bool directionRight, Character inorigin, float inrange, float indamage)
        {
            mDamage = indamage;
            mOrigin = inorigin;
            mRange = inrange;
            MakeActive();//from gameobj, set travel
            mInitialX = mPosition.X = CenterX = XStart;
            mParticle.MakeActive(Center, Texture);
            mPosition.Z = Z;

            mShadow.AddToAutoDrawSet();
            mShadow.Visible = true;
            mShadow.TopOfAutoDrawSet();
            mShadow.CenterY = mPosition.Z;
            mPosition.Y = CenterY = YStart;


            if (directionRight)
            {
                VelocityX = mShootSpeed;
                mTravelDirection = direction.right;
            }
            else
            {
                VelocityX = -mShootSpeed;
                mTravelDirection = direction.left;
            }
        }
    }
}
