﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{

    class Enemy : Character
    {
        #region textures of the enemy
        protected String AttackLeft;
        protected String AttackRight;
        protected String AttackWalkingLeft_01;
        protected String AttackWalkingLeft_02;
        protected String AttackWalkingRight_01;
        protected String AttackWalkingRight_02;
        protected String AttackJumpLeft;
        protected String AttackJumpRight;
        protected String mAttackPri;
        #endregion

        #region enemy variables
        protected int Level;
        protected int Range;
        protected int mHealth;
        protected int mTotalHealth;
        //protected string BinHealth;
        protected int Radius;
        public bool chase;
        protected bool mDead = false;
        protected int patrol = 0;
        public int EnLevel = XNACS1Base.RandomInt(1,5);
        protected bool facing = false;
        //keeps track of whether a texture tint is currently being applied
        private bool tintChanged = false;
        private int tintTimer = 0;
        //holds a color for tinting on different hero collisions
        Color mTintingColor;
        #endregion

        #region attack variables

        //every hero has an array of attacks
        public PriShot[] priAtt; // COMMENT THIS OUT???

        protected float prishotdamage = 0;


        //the time between the shots
        protected int attackDelay = 20;
        private int currentattackDelay = 0;

        public enum attack { notAttack, attacking };    // is the hero attacking
        public attack attackState { get; set; }
        #endregion

        EnemyHUD mEnemyHUD;

        public Enemy()
        {
            Range = EnLevel * 20;
           // mHealth = XNACS1Base.RandomInt(1, 7);
            mHealth = 4;
            
            mTotalHealth = 4;
            Radius = EnLevel * 15;
            chase = false;
            
            mSpeed = 0.5f;
            attackDelay = 10;

            #region Enemy HUD
            mEnemyHUD = new EnemyHUD(Center, mHealth);
            #endregion

        }

        virtual public void Update(Vector2 closest)
        {
            ControlUpdate(closest);
            
            Label = "";

            if (!mDead)
            {
                if (mHealth <= 0)
                {
                    mEnemyHUD.MakeNotActive();
                    XNACS1Base.PlayACue("death");
                    MakeNotActive();
                    mDead = true;
                }
                else
                {
                    mEnemyHUD.UpdateHealth(mHealth);
                }
            }


            if (!mDead)
            {
                //Label = BinHealth;
                KeepInWorld();
                UpdateTextures();
            }
            else
                Death();
        }

        virtual public void Death()
        {
            Radius = 0;
            chase = false;
            mShadow.RemoveFromAutoDrawSet();
            mEnemyHUD.Remove();
            //mEnemyHUD = null;
        }

        public override bool isHero()
        {
            return false;
        }

        public bool isDead()
        {
            return mDead;
        }


        public override void BringToTop()
        {
            base.BringToTop();
            mEnemyHUD.BringToTop();
        }

        virtual public void ControlUpdate(Vector2 con)
        {
            mEnemyHUD.UpdatePosition(Center);
            Vector2 targetDir = new Vector2();
            bool Normalize = true;


            if (con == Center)
            { 
            
            if (patrol % 20 == 0)
                {
                    targetDir.X = XNACS1Base.RandomFloat(-3f, 3f);
                    targetDir.Y = XNACS1Base.RandomFloat(-3f, 3f);
                    UpdateMovement(targetDir.X, targetDir.Y);
                    mEnemyHUD.UpdatePosition(Center + targetDir);
                }
            
            patrol++;
            return;
            }




            if (Math.Abs(Math.Abs(CenterX) - Math.Abs(con.X)) <= Radius && chase == false)
            {
                chase = true;
                targetDir.X = con.X - Center.X;
                targetDir.Y = con.Y - Center.Y;

                if (Math.Abs(targetDir.X) <= Range)
                {
                    TryAttackPri();
                    Normalize = true;
                }
                else
                    Normalize = true;

                if (Math.Abs(targetDir.Y) <= 1)
                {
                    targetDir.Y = 0;
                    TryAttackPri();
                }
                else
                    Normalize = true;


                if (Normalize)
                    targetDir.Normalize();

                UpdateMovement(targetDir.X * .5f, targetDir.Y * .5f);
            }

            else if (chase == true)
            {
                chase = true;
                targetDir.X = con.X - Center.X;
                targetDir.Y = con.Y - Center.Y;

                if (Math.Abs(targetDir.X) <= Range)
                {
                    if (facing == true)
                    {
                        targetDir.X = 0;
                        Normalize = false;
                        //facing = false;
                    }

                    else if (con.X > CenterX)
                    {
                        targetDir.X = .1f;
                        facing = true;
                        Normalize = true;
                    }
                    else if (con.X < CenterX)
                    {
                        targetDir.X = -.1f;
                        facing = true;
                        Normalize = true;
                    }

                    

                    TryAttackPri();
                }

                else
                    Normalize = true;

                if (Math.Abs(targetDir.Y) <= 5)
                {
                    targetDir.Y = 0;
                    TryAttackPri();
                }
                else
                    Normalize = true;


                if (Normalize)
                    targetDir.Normalize();

                UpdateMovement(targetDir.X * mSpeed, targetDir.Y * mSpeed);
            }

            else 
            {
                if (patrol % 20 == 0)
                {
                    targetDir.X = XNACS1Base.RandomFloat(-3f, 3f);
                    targetDir.Y = XNACS1Base.RandomFloat(-3f, 3f);
                    UpdateMovement(targetDir.X, targetDir.Y);
                }
            }
            patrol++;
        }

        virtual public void UpdateTextures()
        {
            switch (mLookState)
            {
                case looking.lookLeft:

                    switch (mJumpState)
                    {
                        #region jumping, looking left
                        case jump.jumping:
                            if (IsAttacking())
                                Texture = AttackJumpLeft;
                            else
                                Texture = mJumpingLeft;
                            break;
                        #endregion

                        #region not jumping, looking left
                        case jump.notJump:
                            if (IsMovingLeft())
                            {
                                if (mWalkingCounter <= 5)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingLeft_01;
                                    else
                                        Texture = mWalkingLeft_01;
                                }
                                else if (mWalkingCounter <= 10)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingLeft_02;
                                    else
                                        Texture = mWalkingLeft_02;

                                }
                                mWalkingCounter++;
                                if (mWalkingCounter == 10)
                                    mWalkingCounter = 0;
                            }
                            if (IsNotMoving())
                            {
                                if (IsAttacking())
                                    Texture = AttackLeft;
                                else
                                    Texture = mStandingLeft;
                            }
                            break; // not jumping
                        #endregion
                    }
                    break; // look left

                case looking.lookRight:

                    switch (mJumpState)
                    {
                        #region jumping, looking right
                        case jump.jumping:
                            if (IsAttacking())
                                Texture = AttackJumpRight;
                            else
                                Texture = mJumpingRight;
                            break;
                        #endregion

                        #region not jumping, looking right
                        case jump.notJump:
                            if (IsMovingRight())
                            {
                                if (mWalkingCounter <= 5)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingRight_01;
                                    else
                                        Texture = mWalkingRight_01;
                                }
                                else if (mWalkingCounter <= 10)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingRight_02;
                                    else
                                        Texture = mWalkingRight_02;

                                }
                                mWalkingCounter++;
                                if (mWalkingCounter == 10)
                                    mWalkingCounter = 0;
                            }
                            if (IsNotMoving())
                                if (IsAttacking())
                                    Texture = AttackRight;
                                else
                                    Texture = mStandingRight;
                            break;
                        #endregion
                    }
                    break; // look right
            }
            UpdateShadow(); // move the shadow in the right place
        }

        virtual public void UpdateHealth(int Sign)
        {
            // set tint-changing values
            tintChanged = true;
            mTintingColor = Color.DarkRed;
            tintTimer = 5;

            mHealth += Sign;
            Radius = 100;

          
        }

        /// <summary>
        /// UpdateTint: Controls tint changes related to object collisions.
        /// If the tint has already been changed and the timer runs down it will remove
        /// the tint from the texture. Also increments the timer if there was a tint change.
        /// Note: The trigger for the tintchange happens in UpdateHealth
        /// </summary>
        public void UpdateTint()
        {
            if (tintChanged)
            {
                // continue counting how long the texturetint has been effected
                tintTimer--;

                // if the tint color hasn't been changed, change it
                if (TextureTintColor == Color.White)
                    TextureTintColor = mTintingColor;

                // if tinting is over reset to normal state
                if (tintTimer == 0)
                {
                    tintChanged = false;
                    TextureTintColor = Color.White;
                }
            }
        }

        virtual public void TryAttackPri()
        {
            //i want to attack and am able to
            if (currentattackDelay <= 0)
            {
                currentattackDelay = attackDelay * (10 - EnLevel);
                attackState = attack.attacking;
                AttackPri();
            }//i am currently attacking
            else if (currentattackDelay >= 0)
            {
                currentattackDelay--;
                attackState = attack.notAttack;
            }
            //else              
        }

        private void AttackPri()
        {
            PriShot shot = mAttacks.GetNextNegPri();
            shot.Texture = mAttackPri;

            if (mLookState == looking.lookLeft)
            {
                shot.Fire(mPosition.X - Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");
            }
            else
            {
                shot.Fire(mPosition.X + Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");
            }
        }

       private bool IsAttacking()
        {
            return (attackState == attack.attacking);
        }

      
    }


}
