﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class TankFlipFlop : Enemy
    {
        public TankFlipFlop()
        {

            #region textures for the Level 1 Enemy
            mStandingRight = "Enemy_FlippyFloppy_R";
            mStandingLeft = "Enemy_FlippyFloppy_L";
            mJumpingRight = "Enemy_FlippyFloppy_R";
            mJumpingLeft = "Enemy_FlippyFloppy_L";
            mWalkingLeft_01 = "Enemy_FlippyFloppy_L";
            mWalkingLeft_02 = "Enemy_FlippyFloppy_L";
            mWalkingRight_01 = "Enemy_FlippyFloppy_R";
            mWalkingRight_02 = "Enemy_FlippyFloppy_R";
            AttackLeft = "Enemy_FlippyFloppy_L";
            AttackRight = "Enemy_FlippyFloppy_R";
            AttackWalkingLeft_01 = "Enemy_FlippyFloppy_L";
            AttackWalkingLeft_02 = "Enemy_FlippyFloppy_L";
            AttackWalkingRight_01 = "Enemy_FlippyFloppy_R";
            AttackWalkingRight_02 = "Enemy_FlippyFloppy_R";
            AttackJumpLeft = "Enemy_FlippyFloppy_L";
            AttackJumpRight = "Enemy_FlippyFloppy_R";
            mAttackPri = "enemy_shot";
            #endregion

            #region enemy stuff
            Texture = mStandingRight;
            Width = 15;
            Height = 12;
            mCenterToBot = Height / 2;

            CenterX = 0;
            CenterY = 0;
            mZDepth = 12;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            mSpeed = .5f;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 9f;
            mShadow.Radius = mShadowRadius;
            #endregion

            prishotdamage = -1;
        }
    }
    
}
