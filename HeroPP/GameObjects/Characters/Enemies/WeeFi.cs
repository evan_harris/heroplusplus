﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class WeeFi : Enemy
    {
        public WeeFi()
        {

            #region textures for the Level 1 Enemy
            mStandingRight = "Enemy_Weefi_R";
            mStandingLeft = "Enemy_Weefi_L";
            mJumpingRight = "Enemy_Weefi_R";
            mJumpingLeft = "Enemy_Weefi_L";
            mWalkingLeft_01 = "Enemy_Weefi_L";
            mWalkingLeft_02 = "Enemy_Weefi_L";
            mWalkingRight_01 = "Enemy_Weefi_R";
            mWalkingRight_02 = "Enemy_Weefi_R";
            AttackLeft = "Enemy_Weefi_L";
            AttackRight = "Enemy_Weefi_R";
            AttackWalkingLeft_01 = "Enemy_Weefi_L";
            AttackWalkingLeft_02 = "Enemy_Weefi_L";
            AttackWalkingRight_01 = "Enemy_Weefi_R";
            AttackWalkingRight_02 = "Enemy_Weefi_R";
            AttackJumpLeft = "Enemy_Weefi_L";
            AttackJumpRight = "Enemy_Weefi_R";
            mAttackPri = "enemy_shot";
            #endregion

            #region enemy stuff
            Texture = mStandingRight;
            Width = 8 * 1.2f;
            Height = 10 * 1.2f;
            mCenterToBot = Height / 2;

            CenterX = 0;
            CenterY = 0;
            mZDepth = 7;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 4.5f;
            mShadow.Radius = mShadowRadius;
            #endregion

            prishotdamage = -1;
        }
    }

}
