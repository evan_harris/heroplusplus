﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;


namespace Hero_Namespace
{
    class EnemyHealthBar : XNACS1Rectangle
    {
        protected XNACS1Rectangle mBlueHealth;

        public EnemyHealthBar(Vector2 center)
        {
            Width = 1f;
            Height = 3;
            Color = Color.Red;
            Center = center;
            mBlueHealth = new XNACS1Rectangle();
            mBlueHealth.Width = Width;
            mBlueHealth.Height = Height;
            mBlueHealth.Center = Center;

        }
        public void Update(Vector2 move)
        {
            Center = move;
            mBlueHealth.Center = Center;
        }
        public void UpdateEnemyHealth(float percentBlue)
        {
            mBlueHealth.Height *= percentBlue;
            float change = Height - mBlueHealth.Height;
            mBlueHealth.CenterY -= change / 2;
            if (mBlueHealth.Height >= Height || mBlueHealth.Height <= 0)
            {
                RemoveHealthBar();
            }
        }
        private void RemoveHealthBar()
        {
            mBlueHealth.Visible = false;
            mBlueHealth.RemoveFromAutoDrawSet();
            Visible = false;
            RemoveFromAutoDrawSet();
        }
        public void BringToFront()
        {
            TopOfAutoDrawSet();
            mBlueHealth.TopOfAutoDrawSet();
        }
    }
}
