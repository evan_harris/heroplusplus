﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class Level3Boss : Boss
    {
        public Level3Boss()
        {

            #region textures for the Level 1 Enemy
            mStandingRight = "boss3_R";
            mStandingLeft = "boss3";
            mJumpingRight = "boss3_R";
            mJumpingLeft = "boss3";
            mWalkingLeft_01 = "boss3";
            mWalkingLeft_02 = "boss3";
            mWalkingRight_01 = "boss3_R";
            mWalkingRight_02 = "boss3_R";
            AttackLeft = "boss3";
            AttackRight = "boss3_R";
            AttackWalkingLeft_01 = "boss3";
            AttackWalkingLeft_02 = "boss3";
            AttackWalkingRight_01 = "boss3_R";
            AttackWalkingRight_02 = "boss3_R";
            AttackJumpLeft = "boss3";
            AttackJumpRight = "boss3_R";
            mAttackPri = "enemy_shot";
            #endregion

            #region enemy stuff
            Texture = mStandingRight;
            Width = 40;
            Height = 32;
            mCenterToBot = Height / 2;

            CenterX = 0;
            CenterY = 0;
            mZDepth = 12;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 18f;
            mShadow.Radius = mShadowRadius;
            #endregion

            prishotdamage = -2;
        }
    }

}
