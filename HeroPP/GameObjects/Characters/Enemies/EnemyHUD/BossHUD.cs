﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class BossHUD
    {
        EnemyBitObj mBitCount1;
        EnemyBitObj mBitCount2;
        EnemyBitObj mBitCount3;
        EnemyBitObj mBitCount4;
        EnemyBitObj mBitCount5;
        EnemyHealthBar mHealthBar;
        String mHealthString;

        public BossHUD(Vector2 center, int health)
        {
            UpdateHealthString(health);

            mBitCount1 = new EnemyBitObj(center, mHealthString[0]);
            mBitCount2 = new EnemyBitObj(center, mHealthString[1]);
            mBitCount3 = new EnemyBitObj(center, mHealthString[2]);
            mBitCount4 = new EnemyBitObj(center, mHealthString[3]);
            mBitCount5 = new EnemyBitObj(center, mHealthString[4]);
            mHealthBar = new EnemyHealthBar(center);

        }
        public void UpdateHealth(int health)
        {
            UpdateHealthString(health);
            mBitCount1.UpdateHealth(mHealthString[0]);
            mBitCount2.UpdateHealth(mHealthString[1]);
            mBitCount3.UpdateHealth(mHealthString[2]);
            mBitCount4.UpdateHealth(mHealthString[3]);
            mBitCount5.UpdateHealth(mHealthString[4]);
            float percentBlue = (32f - (float)health) / 32f;
            mHealthBar.UpdateEnemyHealth(percentBlue);
        }
        public void UpdatePosition(Vector2 move)
        {
            move.Y += 6.5f;
            move.X -= 4f;
            mBitCount1.Update(move);

            move.X += 0.9f;
            mBitCount2.Update(move);

            move.X += 0.9f;
            mBitCount3.Update(move);

            move.X += 0.9f;
            mBitCount4.Update(move);

            move.X += 0.9f;
            mBitCount5.Update(move);

            move.X -= 2.7f;
            move.Y += 1.5f;
            mHealthBar.Update(move);

        }
        private void UpdateHealthString(int health)
        {
            switch (health)
            {
                case 1:
                    mHealthString = "00001";
                    break;
                case 2:
                    mHealthString = "00010";
                    break;
                case 3:
                    mHealthString = "00011";
                    break;
                case 4:
                    mHealthString = "00100";
                    break;
                case 5:
                    mHealthString = "00101";
                    break;
                case 6:
                    mHealthString = "00110";
                    break;
                case 7:
                    mHealthString = "00111";
                    break;
                case 8:
                    mHealthString = "01000";
                    break;
                case 9:
                    mHealthString = "01001";
                    break;
                case 10:
                    mHealthString = "01010";
                    break;
                case 11:
                    mHealthString = "01011";
                    break;
                case 12:
                    mHealthString = "01100";
                    break;
                case 13:
                    mHealthString = "01101";
                    break;
                case 14:
                    mHealthString = "01110";
                    break;
                case 15:
                    mHealthString = "01111";
                    break;
                case 16:
                    mHealthString = "10000";
                    break;
                case 17:
                    mHealthString = "10001";
                    break;
                case 18:
                    mHealthString = "10010";
                    break;
                case 19:
                    mHealthString = "10011";
                    break;
                case 20:
                    mHealthString = "10100";
                    break;
                case 21:
                    mHealthString = "10101";
                    break;
                case 22:
                    mHealthString = "10110";
                    break;
                case 23:
                    mHealthString = "10111";
                    break;
                case 24:
                    mHealthString = "11000";
                    break;
                case 25:
                    mHealthString = "11001";
                    break;
                case 26:
                    mHealthString = "11010";
                    break;
                case 27:
                    mHealthString = "11011";
                    break;
                case 28:
                    mHealthString = "11100";
                    break;
                case 29:
                    mHealthString = "11101";
                    break;
                case 30:
                    mHealthString = "11110";
                    break;
                case 31:
                    mHealthString = "11111";
                    break;
                default:
                    mHealthString = "00000";
                    break;
            }
        }
        public void MakeNotActive()
        {
            mBitCount1.RemoveFromAutoDrawSet();
            mBitCount2.RemoveFromAutoDrawSet();
            mBitCount3.RemoveFromAutoDrawSet();
            mBitCount4.RemoveFromAutoDrawSet();
            mBitCount5.RemoveFromAutoDrawSet();
            mHealthBar.MakeNotActive();
        }
        public void BringToTop()
        {
            mBitCount1.BringToTop();
            mBitCount2.BringToTop();
            mBitCount3.BringToTop();
            mBitCount4.BringToTop();
            mBitCount5.BringToTop();
            mHealthBar.BringToTop();
        }
        public void Remove()
        {
            mBitCount1.RemoveFromAutoDrawSet();
            mBitCount2.RemoveFromAutoDrawSet();
            mBitCount3.RemoveFromAutoDrawSet();
            mBitCount4.RemoveFromAutoDrawSet();
            mBitCount5.RemoveFromAutoDrawSet();
            mHealthBar.RemoveFromAutoDrawSet();
        }
    }
}
