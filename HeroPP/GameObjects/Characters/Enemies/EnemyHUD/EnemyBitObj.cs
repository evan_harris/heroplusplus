﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;


namespace Hero_Namespace
{
    class EnemyBitObj : XNACS1Rectangle
    {
        public EnemyBitObj(Vector2 center, char label)
        {
            Center = center;
            Width = 0.8f;
            Height = 1.5f;
            if (label == '0')
                Texture = "EnemyBinaryCount_0";
            else
                Texture = "EnemyBinaryCount_1";
            BringToTop();
        }
        public void BringToTop()
        {
            TopOfAutoDrawSet();
        }
        public void Update(Vector2 center)
        {
            Center = center;
        }
        public void UpdateHealth(char label)
        {
            if (label == '0')
                Texture = "EnemyBinaryCount_0";
            else
                Texture = "EnemyBinaryCount_1";
        }
    }
}
