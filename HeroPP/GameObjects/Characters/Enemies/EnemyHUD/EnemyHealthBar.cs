﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;


namespace Hero_Namespace
{
    class EnemyHealthBar : XNACS1Rectangle
    {
        protected XNACS1Rectangle mBlueHealth;
        float mWidth;
        public EnemyHealthBar(Vector2 center)
        {
            Visible = true;
            Width = 6f;
            mWidth = Width;
            Height = 1.5f;
            Texture = "enemy_healthbar_blue";
            Center = center;
            mBlueHealth = new XNACS1Rectangle();
            mBlueHealth.Width = Width;
            mBlueHealth.Height = Height;
            mBlueHealth.Center = Center;
            mBlueHealth.Visible = true;
            mBlueHealth.Texture = "enemy_healthbar_red";
            
            BringToTop();

        }
        public void Update(Vector2 center)
        {
            Center = center;
            mBlueHealth.Center = Center;
        }
        public void UpdateEnemyHealth(float percentBlue)
        {
            mBlueHealth.Width = mWidth * percentBlue;
            Width = mWidth * (1 - percentBlue);

            mBlueHealth.CenterX += Width / 2;            
            CenterX -= mBlueHealth.Width / 2;
            
        }
        public void BringToTop()
        {
            TopOfAutoDrawSet();
            mBlueHealth.TopOfAutoDrawSet();
        }
        public void MakeNotActive()
        {
            RemoveFromAutoDrawSet();
            mBlueHealth.RemoveFromAutoDrawSet();
        }
    }
}
