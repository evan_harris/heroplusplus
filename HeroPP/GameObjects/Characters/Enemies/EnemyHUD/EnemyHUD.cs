﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class EnemyHUD
    {
        EnemyBitObj mBitCount1;
        EnemyBitObj mBitCount2;
        EnemyBitObj mBitCount3;
        EnemyHealthBar mHealthBar;
        String mHealthString;

        public EnemyHUD(Vector2 center, int health)
        {
            UpdateHealthString(health);

            mBitCount1 = new EnemyBitObj(center, mHealthString[0]);
            mBitCount2 = new EnemyBitObj(center, mHealthString[1]);
            mBitCount3 = new EnemyBitObj(center, mHealthString[2]);          
            mHealthBar = new EnemyHealthBar(center);

        }
        public void UpdateHealth(int health)
        {
            UpdateHealthString(health);
            mBitCount1.UpdateHealth(mHealthString[0]);
            mBitCount2.UpdateHealth(mHealthString[1]);
            mBitCount3.UpdateHealth(mHealthString[2]);
            float percentBlue = (4f - (float)health ) / 4f;
            mHealthBar.UpdateEnemyHealth(percentBlue);
        }
        public void UpdatePosition(Vector2 move)
        {
            move.Y += 6.5f;
            move.X -= 4f;
            mBitCount1.Update(move);

            move.X += 0.9f;
            mBitCount2.Update(move);

            move.X += 0.9f;
            mBitCount3.Update(move);

            move.X += 3.7f;
            mHealthBar.Update(move);

        }
        private void UpdateHealthString(int health)
        {
            switch (health)
            {
                case 1:
                    mHealthString = "001";
                    break;
                case 2:
                    mHealthString = "010";
                    break;
                case 3:
                    mHealthString = "011";
                    break;
                case 4:
                    mHealthString = "100";
                    break;
                case 5:
                    mHealthString = "101";
                    break;
                case 6:
                    mHealthString = "110";
                    break;
                case 7:
                    mHealthString = "111";
                    break;
                default:
                    mHealthString = "000";
                    break;
            }
        }
        public void MakeNotActive()
        {
            mBitCount1.RemoveFromAutoDrawSet();
            mBitCount2.RemoveFromAutoDrawSet();
            mBitCount3.RemoveFromAutoDrawSet();
            mHealthBar.MakeNotActive();
        }
        public void BringToTop()
        {
            mBitCount1.BringToTop();
            mBitCount2.BringToTop();
            mBitCount3.BringToTop();
            mHealthBar.BringToTop();
        }
        public void Remove()
        {
            mBitCount1.RemoveFromAutoDrawSet();
            mBitCount2.RemoveFromAutoDrawSet();
            mBitCount3.RemoveFromAutoDrawSet();
            mHealthBar.RemoveFromAutoDrawSet();

        }
    }
}
