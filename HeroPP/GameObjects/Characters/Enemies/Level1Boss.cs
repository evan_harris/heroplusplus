﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class Level1Boss : Boss
    {
        public Level1Boss()
        {

            #region textures for the Level 1 Enemy
            mStandingRight = "boss";
            mStandingLeft = "boss";
            mJumpingRight = "boss";
            mJumpingLeft = "boss";
            mWalkingLeft_01 = "boss";
            mWalkingLeft_02 = "boss";
            mWalkingRight_01 = "boss";
            mWalkingRight_02 = "boss";
            AttackLeft = "boss";
            AttackRight = "boss";
            AttackWalkingLeft_01 = "boss";
            AttackWalkingLeft_02 = "boss";
            AttackWalkingRight_01 = "boss";
            AttackWalkingRight_02 = "boss";
            AttackJumpLeft = "boss";
            AttackJumpRight = "boss";
            mAttackPri = "enemy_shot";
            #endregion

            #region enemy stuff
            Texture = mStandingRight;
            Width = 35;
            Height = 40;
            mCenterToBot = Height / 2;

            CenterX = 0;
            CenterY = 0;
            mZDepth = 12;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 18f;
            mShadow.Radius = mShadowRadius;
            #endregion

            prishotdamage = -2;
        }
    }

}
