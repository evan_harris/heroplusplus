﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{

    class Boss : Enemy
    {
        bool activeflag;
        protected string BinHealth;

        #region attack variables

        private int currentattackDelay = 0;
        private int mSpecialCount = 0;

        #endregion

        BossHUD mEnemyHUD;

        public Boss()
        {
            Range = EnLevel * 20;
            mHealth = XNACS1Base.RandomInt(15, 17);
            mTotalHealth = 31;
            Radius = 60;
            chase = false;
            mLookState = looking.lookLeft;
            attackDelay = 3;
            mSpeed = 0.7f;
            attackDelay = 10;
            prishotdamage = 1;
            mSpecialCount = 80;
            activeflag = true;

            #region Boss HUD
            mEnemyHUD = new BossHUD(Center, mHealth);
            #endregion

        }

        public override void Update(Vector2 closest)
        {
           ControlUpdate(closest);

            Label = "";


            if (!mDead)
            {
                if (mHealth <= 0)
                {
                    mEnemyHUD.MakeNotActive();
                    XNACS1Base.PlayACue("death");
                    MakeNotActive();
                    mDead = true;
                }
                else
                {
                    mEnemyHUD.UpdateHealth(mHealth);
                }
            }


            if (!mDead)
            {
                Label = BinHealth;
                KeepInWorld();
                UpdateTextures();
            }
            else
                Death();
        }

        public void Death()
        {
            Radius = 0;
            chase = false;
            mEnemyHUD.Remove();
            //mEnemyHUD = null;
            mShadow.RemoveFromAutoDrawSet();
            //mShadow = null;
        }

        public override bool isHero()
        {
            return false;
        }

        public override void BringToTop()
        {
            base.BringToTop();
            mEnemyHUD.BringToTop();
        }

        public override void ControlUpdate(Vector2 con)
        {
            mEnemyHUD.UpdatePosition(Center);
            Vector2 targetDir = new Vector2();
            bool Normalize = true;


            if (con == Center)
            {

                if (patrol % 20 == 0)
                {
                    targetDir.X = XNACS1Base.RandomFloat(-3f, 3f);
                    targetDir.Y = XNACS1Base.RandomFloat(-3f, 3f);
                    UpdateMovement(targetDir.X, targetDir.Y);
                    mEnemyHUD.UpdatePosition(Center + targetDir);
                }

                patrol++;
                return;
            }




            if (Math.Abs(Math.Abs(CenterX) - Math.Abs(con.X)) <= Radius && chase == false)
            {
                chase = true;
                targetDir.X = con.X - Center.X;
                targetDir.Y = con.Y - Center.Y;

                if (Math.Abs(targetDir.X) <= Range)
                {
                    TryAttackPri();
                    Normalize = true;
                }
                else
                    Normalize = true;

                if (Math.Abs(targetDir.Y) <= 1)
                {
                    targetDir.Y = 0;
                    TryAttackPri();
                }
                else
                    Normalize = true;


                if (Normalize)
                    targetDir.Normalize();

                UpdateMovement(targetDir.X * .5f, targetDir.Y * .5f);
            }

            else if (chase == true)
            {
                chase = true;
                targetDir.X = con.X - Center.X;
                targetDir.Y = con.Y - Center.Y;

                if (Math.Abs(targetDir.X) <= Range)
                {
                    if (facing == true)
                    {
                        targetDir.X = 0;
                        Normalize = false;
                        //facing = false;
                    }

                    else if (con.X > CenterX)
                    {
                        targetDir.X = .1f;
                        facing = true;
                        Normalize = true;
                    }
                    else if (con.X < CenterX)
                    {
                        targetDir.X = -.1f;
                        facing = true;
                        Normalize = true;
                    }



                    TryAttackPri();
                }

                else
                    Normalize = true;

                if (Math.Abs(targetDir.Y) <= 5)
                {
                    targetDir.Y = 0;
                    TryAttackPri();
                }
                else
                    Normalize = true;


                if (Normalize)
                    targetDir.Normalize();

                UpdateMovement(targetDir.X * .5f, targetDir.Y * .5f);
            }

            else
            {
                if (patrol % 20 == 0)
                {
                    targetDir.X = XNACS1Base.RandomFloat(-3f, 3f);
                    targetDir.Y = XNACS1Base.RandomFloat(-3f, 3f);
                    UpdateMovement(targetDir.X, targetDir.Y);
                }
            }

        }

        public override void UpdateTextures()
        {
            switch (mLookState)
            {
                case looking.lookLeft:

                    switch (mJumpState)
                    {
                        #region jumping, looking left
                        case jump.jumping:
                            if (IsAttacking())
                                Texture = AttackJumpLeft;
                            else
                                Texture = mJumpingLeft;
                            break;
                        #endregion

                        #region not jumping, looking left
                        case jump.notJump:
                            if (IsMovingLeft())
                            {
                                if (mWalkingCounter <= 5)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingLeft_01;
                                    else
                                        Texture = mWalkingLeft_01;
                                }
                                else if (mWalkingCounter <= 10)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingLeft_02;
                                    else
                                        Texture = mWalkingLeft_02;

                                }
                                mWalkingCounter++;
                                if (mWalkingCounter == 10)
                                    mWalkingCounter = 0;
                            }
                            if (IsNotMoving())
                            {
                                if (IsAttacking())
                                    Texture = AttackLeft;
                                else
                                    Texture = mStandingLeft;
                            }
                            break; // not jumping
                        #endregion
                    }
                    break; // look left

                case looking.lookRight:

                    switch (mJumpState)
                    {
                        #region jumping, looking right
                        case jump.jumping:
                            if (IsAttacking())
                                Texture = AttackJumpRight;
                            else
                                Texture = mJumpingRight;
                            break;
                        #endregion

                        #region not jumping, looking right
                        case jump.notJump:
                            if (IsMovingRight())
                            {
                                if (mWalkingCounter <= 5)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingRight_01;
                                    else
                                        Texture = mWalkingRight_01;
                                }
                                else if (mWalkingCounter <= 10)
                                {
                                    if (IsAttacking())
                                        Texture = AttackWalkingRight_02;
                                    else
                                        Texture = mWalkingRight_02;

                                }
                                mWalkingCounter++;
                                if (mWalkingCounter == 10)
                                    mWalkingCounter = 0;
                            }
                            if (IsNotMoving())
                                if (IsAttacking())
                                    Texture = AttackRight;
                                else
                                    Texture = mStandingRight;
                            break;
                        #endregion
                    }
                    break; // look right
            }
            UpdateShadow(); // move the shadow in the right place
        }

        public override void UpdateHealth(int Sign)
        {

            mHealth += Sign;
            Radius = 100;

            if (mHealth >= 32 && activeflag) 
            {
                mHealth = 0;
                activeflag = false;
                //Death();
            }
        }

        public override void TryAttackPri()
        {
            //i want to attack and am able to
            if (currentattackDelay <= 0)
            {
                currentattackDelay = 40;
                attackState = attack.attacking;
                AttackPri();
            }//i am currently attacking
            else if (currentattackDelay >= 0)
            {
                currentattackDelay--;
                attackState = attack.notAttack;
            }

            if (mSpecialCount <= 0)
            {
                mSpecialCount = 120;
                attackState = attack.attacking;
                AttackPriSpecial();
            }//i am currently attacking
            else if (mSpecialCount >= 0)
            {
                mSpecialCount--;
                attackState = attack.notAttack;
            }
            
        }

        private void AttackPri()
        {
            PriShot shot = mAttacks.GetNextNegPri();
            shot.Texture = mAttackPri;

            if (mLookState == looking.lookLeft)
            {
                shot.Fire(mPosition.X - Width / 2 + 12f, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");

            }
            else
            {

                shot.Fire(mPosition.X + Width / 2 - 12f, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");

            }
        }

        private void AttackPriSpecial()
        {
            PriShot shot = mAttacks.GetNextNegPri();
            shot.Texture = mAttackPri;

            if (mLookState == looking.lookLeft)
            {

                shot.Fire(mPosition.X - Width / 2, (mPosition.Y + mPosition.Z + mCenterToBot - 2.8f - 20f), mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");

                shot = mAttacks.GetNextNegPri();
                shot.Texture = mAttackPri;

                shot.Fire(mPosition.X - Width / 2 + 12f, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");

                shot = mAttacks.GetNextNegPri();
                shot.Texture = mAttackPri;

                shot.Fire(mPosition.X - Width / 2, (mPosition.Y + mPosition.Z + mCenterToBot - 2.8f + 20f), mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");
            }
            else
            {
                shot.Fire(mPosition.X + Width / 2, (mPosition.Y + mPosition.Z + mCenterToBot - 2.8f + 10f), mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");

                shot = mAttacks.GetNextNegPri();
                shot.Texture = mAttackPri;

                shot.Fire(mPosition.X + Width / 2 - 12f, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");

                shot = mAttacks.GetNextNegPri();
                shot.Texture = mAttackPri;

                shot.Fire(mPosition.X + Width / 2, (mPosition.Y + mPosition.Z + mCenterToBot - 2.8f - 10f), mPosition.Z, (mLookState == looking.lookRight), this, Range, prishotdamage);
                XNACS1Base.PlayACue("robotfire");

            }
        }

        private bool IsAttacking()
        {
            return (attackState == attack.attacking);
        }


    }


}
