﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class Walker : Enemy
    {
        public Walker()
        {

            #region textures for the Level 1 Enemy
            mStandingRight = "Enemy_Woot_R";
            mStandingLeft = "Enemy_Woot_L";
            mJumpingRight = "Enemy_Woot_R";
            mJumpingLeft = "Enemy_Woot_L";
            mWalkingLeft_01 = "Enemy_Woot_L";
            mWalkingLeft_02 = "Enemy_Woot_L";
            mWalkingRight_01 = "Enemy_Woot_R";
            mWalkingRight_02 = "Enemy_Woot_R";
            AttackLeft = "Enemy_Woot_L";
            AttackRight = "Enemy_Woot_R";
            AttackWalkingLeft_01 = "Enemy_Woot_L";
            AttackWalkingLeft_02 = "Enemy_Woot_L";
            AttackWalkingRight_01 = "Enemy_Woot_R";
            AttackWalkingRight_02 = "Enemy_Woot_R";
            AttackJumpLeft = "Enemy_Woot_L";
            AttackJumpRight = "Enemy_Woot_R";
            mAttackPri = "enemy_shot";
            #endregion

            #region enemy stuff
            Texture = mStandingRight;
            Width = 10;
            Height = 12;
            mCenterToBot = Height / 2;

            CenterX = 0;
            CenterY = 0;
            mZDepth = 7;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            mSpeed = .5f;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 4.5f;
            mShadow.Radius = mShadowRadius;
            
            #endregion

            prishotdamage = -1;
        }
    }

}
