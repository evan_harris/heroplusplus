﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class SpectatorZander : Spectator
    {
        public enum sayings { hoare, homework, valgrind };
        public sayings sayingState { get; set; }
        protected int counterSaying;
        public SpectatorZander()
        {
            mStandingLeft = mJumpingLeft = mWalkingLeft_01 = mWalkingLeft_02 = "zander_LN";
            mStandingRight = mJumpingRight = mWalkingRight_01 = mWalkingRight_02 = "zander_RN";

            #region spectator stuff
            Texture = mStandingRight;
            Width = 12*.9f;
            Height = 15*.9f;
            mCenterToBot = Height / 2;

            CenterX = 0;
            CenterY = 0;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 5f;
            mShadow.Radius = mShadowRadius;
            
            
            #endregion

            #region activation field
            activationField = new XNACS1Rectangle();
            activationField.Width = 15;
            activationField.Height = 6;
            activationField.CenterX = mPosition.X;
            activationField.CenterY = mPosition.Z;
            activationField.Visible = false;
            sayingState = sayings.hoare;
            counterSaying = 0;
            #endregion

            #region saying bubble
            sayingField = new WordBubble();
            sayingField.CenterX = CenterX + Height / 2 + 0.5f;
            sayingField.CenterY = CenterY + Width / 2 + 4;
            sayingField.Width = 10;
            sayingField.Height = 7f;
            #endregion

        }
        public override void Update(Hero hero)
        {

            #region update the position
            mPosition.X = CenterX;
            mPosition.Z = CenterY - mCenterToBot;
            mPosition.Y = 0;
            #endregion

            #region update the Center
            PositionCenter();
            #endregion

            UpdateShadow();
            mShadow.Radius = mShadowRadius;

            activationField.CenterX = mPosition.X;
            activationField.CenterY = mPosition.Z;

            sayingField.CenterX = CenterX + Height / 2 + 0.5f;
            sayingField.CenterY = CenterY + Width / 2 + 3.75f;
            if (hero.Collided(activationField))
            {
                Action();
            }
            else
                sayingField.TurnOffBubble();
        }

        public override bool isHero()
        {
            return false;

        }
        public void Action()
        {
            if (sayingState == sayings.hoare && counterSaying < 160)
            {
                sayingField.ChangeBubble("zander_talk_bubble_01");

                counterSaying++;
                if (counterSaying >= 160)
                {
                    sayingState = sayings.homework;
                    counterSaying = 0;
                }
            }
            if (sayingState == sayings.homework && counterSaying < 160)
            {
                sayingField.ChangeBubble("zander_talk_bubble_02");

                counterSaying++;
                if (counterSaying >= 160)
                {
                    sayingState = sayings.valgrind;
                    counterSaying = 0;
                }
            }
            if (sayingState == sayings.valgrind && counterSaying < 160)
            {
                sayingField.ChangeBubble("zander_talk_bubble_03");

                counterSaying++;
                if (counterSaying >= 160)
                {
                    sayingState = sayings.hoare;
                    counterSaying = 0;
                }
            }
        }

        public override void BringToTop()
        {
            mShadow.TopOfAutoDrawSet();
            base.BringToTop();
        }
        public override void SetInitialPosition(float posx, float posz)
        {
            #region update the position
            mPosition.X = posx;
            mPosition.Z = posz;
            mPosition.Y = 0;
            #endregion

            #region update the Center
            PositionCenter();
            #endregion

            UpdateShadow();

            activationField.CenterX = mPosition.X;
            activationField.CenterY = mPosition.Z;

            sayingField.CenterX = CenterX + Height / 2 + 0.5f;
            sayingField.CenterY = CenterY + Width / 2 + 3.75f;
        }
    }
}