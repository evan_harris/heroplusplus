﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class SpectatorSung : Spectator
    {
        public enum moving { move, notMove };
        public moving moveState { get; set; }
        int countMovement;
        int timeCount, blinkCount;
        XNACS1Rectangle SungLeft, SungRight;
        public SpectatorSung()
        {
            mStandingLeft = "sung";


            #region spectator stuff
            Texture = mStandingLeft;
            Width = 13*.8f;
            Height = 16*.8f;
            mCenterToBot = Height / 2;

            CenterX = -20;
            CenterY = -20;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadow.Radius = 4.5f;
            #endregion

            activationField = new XNACS1Rectangle();
            activationField.Width = 15*.8f;
            activationField.Height = 20*.8f;
            activationField.CenterX = CenterX;
            activationField.CenterY = CenterY - activationField.Height / 2;
            activationField.Visible = false;
            moveState = moving.notMove;
            countMovement = 0;
            timeCount = blinkCount = 20;

            sayingField = new WordBubble();
            sayingField.CenterX = CenterX + Height / 2 + 0.5f;
            sayingField.CenterY = CenterY + Width / 2 + 3.75f;
            sayingField.Width = 10;
            sayingField.Height = 7f;
            sayingField.Texture = "sung_talk_bubble_01";

            SungLeft = new XNACS1Rectangle(Center, Width, Height);
            SungLeft.Texture = "sung_move_01";
            SungRight = new XNACS1Rectangle(Center, Width, Height);
            SungRight.Texture = "sung_move_02";
        }
        public override void Update(Hero hero)
        {
        
            if (hero.Collided(activationField))
            {
                if (moveState != moving.move)
                {
                    Action();
                    sayingField.Visible = true;
                }
            }
            else if (SungLeft.Center != Center)
            {
                SungLeft.Center = Center;
                SungRight.Center = Center;
                sayingField.CenterX = CenterX + Height / 2 + 0.5f;
                sayingField.CenterY = CenterY + Width / 2 + 3.75f;
                activationField.CenterX = CenterX;
                activationField.CenterY = CenterY - activationField.Height / 2;
            
            }



            if (moveState == moving.move)
            {
                Action();
                countMovement++;
                BlinkOut();
                if (countMovement >= 150)
                {
                    this.Visible = false;
                    SungLeft.Visible = false;
                    SungRight.Visible = false;
                    BlinkMoving();
                }
            }

        }
        public void Action()
        {
            SungLeft.CenterX -= 0.25f;
            SungRight.CenterX += 0.25f;


            moveState = moving.move;
        }

        public override bool isHero()
        {
            return false;
        
        }
        public void BlinkOut()
        {
            if (countMovement % timeCount == 1)
            {
                this.Visible = false;
                mShadow.Visible = false;

            }
            else if (countMovement % timeCount == (int)(0.5 * timeCount))
            {
                this.Visible = true;
                mShadow.Visible = true;

                timeCount -= 1;
                if (timeCount <= 0)
                {
                    this.Visible = false;
                    mShadow.Visible = false;
                }
            }
        }
        public void BlinkMoving()
        {
            if (blinkCount > 0 && countMovement % blinkCount == 1)
            {
                SungLeft.Visible = false;
                SungRight.Visible = false;
                blinkCount--;
                if (blinkCount <= 0)
                {
                    SungLeft.Visible = false;
                    SungRight.Visible = false;

                }
            }
            else if (blinkCount > 0 && countMovement % blinkCount == (int)(0.5 * blinkCount))
            {
                SungLeft.Visible = true;
                SungRight.Visible = true;
                sayingField.Visible = true;
                blinkCount--;
                if (blinkCount <= 0)
                {
                    SungLeft.Visible = false;
                    SungRight.Visible = false;
                    sayingField.Visible = false;
                }
            }

        }
        public override void SetInitialPosition(float posx, float posz)
        {
            #region update the position
            mPosition.X = posx;
            mPosition.Z = posz;
            mPosition.Y = 0;
            #endregion

            #region update the Center
            PositionCenter();
            #endregion

            UpdateShadow();

            activationField.CenterX = mPosition.X;
            activationField.CenterY = mPosition.Z;

            sayingField.CenterX = CenterX + Height / 2 + 0.5f;
            sayingField.CenterY = CenterY + Width / 2 + 3.75f;
        }
    }
}
