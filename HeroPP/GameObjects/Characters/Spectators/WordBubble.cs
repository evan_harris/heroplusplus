﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class WordBubble : XNACS1Rectangle
    {
        public WordBubble()
        {
            Visible = false;
            Width = 15;
            Height = 10;
        }
        public void ChangeBubble(String texture)
        {
            Texture = texture;
            Visible = true;
        }
        public void TurnOffBubble()
        {
            Visible = false;
        }
    }
}
