﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    abstract class Spectator : Character
    {

        protected XNACS1Rectangle activationField;
        protected WordBubble sayingField;
        public Spectator()
        {
        }

        public abstract void Update(Hero hero);
        public override void SetInitialPosition(float posx, float posz)
        {
            base.SetInitialPosition(posx, posz);
        }
        public override void BringToTop()
        {
            mShadow.TopOfAutoDrawSet();
            base.BringToTop();
        }

    }
}
