﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNACS1Lib;
namespace Hero_Namespace
{
    class HeroSung : Hero
    {

        private XNACS1Rectangle mCounter;
        private int mSlowDownCounter = -1;
        private bool mTriShotMade = false;

        public HeroSung()
        {
            mPriShootDamage = -1;
            mCritShootDamage = -3;
            mSecShootDamage = -1;
            mTriShootDamage = -3;
            mAOEShootDamage = -1;
            mTriEnergyCost = 98;

            #region textures for the negative hero
            mStandingRight = "sung_RN_standing";
            mStandingLeft = "sung_LN_standing";
            mJumpingRight = "sung_RN_jumping";
            mJumpingLeft = "sung_LN_jumping";
            mWalkingLeft_01 = "sung_LN_walking_01";
            mWalkingLeft_02 = "sung_LN_walking_02";
            mWalkingRight_01 = "sung_RN_walking_01";
            mWalkingRight_02 = "sung_RN_walking_02";
            mAttackLeft = "sung_LS_standing";
            mAttackRight = "sung_RS_standing";
            mAttackWalkingLeft_01 = "sung_LS_walking_01";
            mAttackWalkingLeft_02 = "sung_LS_walking_02";
            mAttackWalkingRight_01 = "sung_RS_walking_01";
            mAttackWalkingRight_02 = "sung_RS_walking_02";
            mAttackJumpLeft = "sung_LS_jumping";
            mAttackJumpRight = "sung_RS_jumping";
            mAttackPri = "shot_xbox";
            mAttackSec = "shot_xbox";
            mAttackTri = "shot_xbox";
            mAttackCrit = "shot_xbox";
            mResetHealth = 150;
            mHealth = mResetHealth;
            #endregion

   
            #region hero stuff
            Texture = mStandingRight;
            Width = 12 * .85f;
            Height = 15 * .85f;
            mCenterToBot = Height / 2;

            CenterX = 10;
            CenterY = 10;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 4.5f;
            mShadow.Radius = mShadowRadius;
            #endregion

            mCounter = new XNACS1Rectangle();
            mCounter.CenterX = CenterX;
            mCounter.CenterY = CenterY + 8;
            mCounter.Width = 0;
            mCounter.Height = 0;
            mCounter.LabelColor = Microsoft.Xna.Framework.Color.White;
        }
        public override void TryAttackTri(bool buttonYPressed)
        {

            //i want to attack and am able to
            if (buttonYPressed && mCurrentTriAttackDelay <= 0 && UseEnergy(mTriEnergyCost))
            {
                UpdateHealth(-10);//health decrease per shot
                mCurrentTriAttackDelay = mTriAttackDelay;
                mTriAttackState = attack.attacking;
                AttackTri();
            }//i am currently attacking
            else// if (currentattackDelay >= 0)
            {
                mCurrentTriAttackDelay--;
                if (mCurrentTriAttackDelay <= (mTriAttackDelay / 2))
                    mTriAttackState = attack.notAttack;
            }
            //else
        }
     
        protected override void AttackTri()
        {
            //retrive the next shot from the attack manager
            XNACS1Base.PlayACue("shotSpecial");
            TriShot attack;
            float shootspeed = 1;
            float degree = 0;

           
            //shooting right
            while (degree <= 360)
            {
                attack = mAttacks.GetNextNegTri();
                attack.Texture = mAttackTri;
                attack.setRange(6);
                degree = degree + 10;
                attack.Fire(mPosition.X - Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (float)Math.Cos(degree) * shootspeed, (float)Math.Sin(degree) * shootspeed, this, mTriShootRange, mTriShootDamage, mMaxZ);


            }
        }
        public override void Update()
        {
            base.Update();
            mCounter.CenterX = CenterX;
            mCounter.CenterY = CenterY + 8;
            if (mSlowDownCounter > 0)
            {
                int count = mSlowDownCounter / 40;
                mCounter.Label = "Slowz Timer: " + count;
            }
            else
            {
                mCounter.Label = ""; 
            }
        }
        public override void BringToTop()
        {
            base.BringToTop();
            mCounter.TopOfAutoDrawSet();
        }
        public override void Dead()
        {
            mCounter.Label = "";
            mSlowDownCounter = 0;
            mCurrentEnergy = 0;
            mHealth = 0;
            RemoveFromAutoDrawSet();
            mShadow.RemoveFromAutoDrawSet();
            MakeNotActive();
            mIsAttackSpecial = false;
        }
    }
}
