﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class Hero : Character
    {

        #region Strings declared for the attack type textures  for the hero
        protected String mAttackLeft;
        protected String mAttackRight;
        protected String mAttackWalkingLeft_01;
        protected String mAttackWalkingLeft_02;
        protected String mAttackWalkingRight_01;
        protected String mAttackWalkingRight_02;
        protected String mAttackJumpLeft;
        protected String mAttackJumpRight;
        protected String mAttackPri;
        protected String mAttackSec;
        protected String mAttackTri;
        protected String mAttackCrit;
        protected string mAttackAOE;
        #endregion

        #region the attack variables pertaining to the hero
        protected int mPriAttackDelay;
        protected int mSecAttackDelay;
        protected int mTriAttackDelay;
        protected int mCritAttackDelay;
        protected int mAOEAttackDelay;
        protected int mCurrentPriAttackDelay = 0;
        protected int mCurrentSecAttackDelay = 0;
        protected int mCurrentTriAttackDelay = 0;
        protected int mCurrentCritAttackDelay = 0;
        protected int mCurrentAOEAttackDelay = 0;
        protected bool mIsAttackSpecial = false;

        //shooting range
        protected float mPriShootRange = 60;
        protected float mSecShootRange = 40;
        protected float mTriShootRange = 40;
        protected float mCritShootRange = 40;
        protected float mAOEShootRange = 40;

        //shooting damage
        protected float mPriShootDamage = 0;
        protected float mSecShootDamage = 0;
        protected float mTriShootDamage = 0;
        protected float mCritShootDamage = 0;
        protected float mAOEShootDamage = 0;

        //shooting energy cost
        protected float mPriEnergyCost = 10;
        protected float mSecEnergyCost = 10;
        protected float mTriEnergyCost = 97;
        protected float mCritEnergyCost = 10;
        protected float mAOEEnergyCost = 75;
        //ensures that two crits can't happen in a row
        private bool critLast = false;

        //keeps track of whether a texture tint is currently being applied
        private bool tintChanged = false;
        private int tintTimer = 0;

        //holds a color for tinting on different hero collisions
        Color mTintingColor;

        public enum attack { notAttack, attacking };    // is the hero attacking
        public attack mPriAttackState { get; set; }
        public attack mSecAttackState { get; set; }
        public attack mTriAttackState { get; set; }
        public attack mCritAttackState { get; set; }
        public attack mAOEAttackState { get; set; }
        #endregion

        #region the hero state if he is alive or not
        public enum mAlive { alive, notAlive };    // is the hero attacking
        public mAlive mAliveState { get; set; }
        #endregion

        #region health variables
        protected int mHealth;
        protected int mResetHealth = 30;
        public bool RequestLife = false;
        #endregion

        protected int BlinkRate = 5;
        protected int BlinkTimer = 5;
        protected int NumberOfBlinks = 6;

        #region pickup emmitter info
        private XNACS1ParticleEmitter.FireEmitter mEnergyEmit;
        private XNACS1ParticleEmitter.FireEmitter mHealthEmit;
        private int mEnergyEmitTimer = 10;
        private int mHealthEmitTimer = 10;
        private bool healthEmitting = false;
        private bool energyEmitting = false;
        #endregion

        #region Energyincormaiton
        protected float mEnRechargeRate = .25f;
        protected float mCurrentEnergy;
        protected float mMaxEnergy = 100;
        #endregion
        /// <summary>
        /// Hero constructor that is set up for the children of Hero
        /// </summary>
        public Hero()
        {
            #region character motion
            mZDepth = 3;         // all heroes have a depth of 3
            mSpeed = 0.95f;       // how fast the hero moves
            #endregion

            #region attack information
            mPriAttackDelay = 8;    // time delay for attacking
                                //the attack manager is used to shoot
            mSecAttackDelay = 16;

            mTriAttackDelay = 8;

            mCritAttackDelay = 8;

            mAOEAttackDelay = 24;
            #endregion

            #region health and life states
            mHealth = mResetHealth;
            mAliveState = mAlive.alive;
            #endregion

            mCurrentEnergy = mMaxEnergy;
        }

        /// <summary>
        /// Updates the hero by keeping it in the world and making sure we update 
        /// the textures properly
        /// </summary>
        public virtual void Update()
        {
            CheckHealth();
            UpdateEnergy();
            KeepInWorld();
            UpdateTextures();
            UpdateTint();
            UpdateEmit();
            AttackSpecialUpdate();
        }

        private void UpdateEnergy()
        {
            if (mCurrentEnergy + mEnRechargeRate > mMaxEnergy)
            {
                mCurrentEnergy = mMaxEnergy;
            }
            else
            {
                mCurrentEnergy += mEnRechargeRate;
            }
            //Label = mCurrentEnergy.ToString();

        }

        public void Addhealth(float amount)
        {
            if (amount > 0)
            {
                // set tint-changing values
                tintChanged = true;
                healthEmitting = true;
                mTintingColor = Color.LawnGreen;
                tintTimer = 5;
                mHealthEmitTimer = 20;
                // just in case another pickup was made before the timer ran down
                if (mHealthEmit != null)
                {
                    mHealthEmit.RemoveEmitterFromUpdateSet();
                    mHealthEmit.RemoveFromAutoDrawSet();
                }
                // uses the player shadow at point of impact to position effect
                mHealthEmit = new XNACS1ParticleEmitter.FireEmitter(mShadow.Center, 30, 0.5f, "test", Color.LawnGreen, 20f, 4f, new Vector2(0, 5));
            }
            else
            {
                // set tint-changing values
                tintChanged = true;
                energyEmitting = true;
                mTintingColor = Color.CornflowerBlue;
                tintTimer = 5;
                mEnergyEmitTimer = 20;
                // just in case another pickup was made before the timer ran down
                if (mEnergyEmit != null)
                {
                    mEnergyEmit.RemoveEmitterFromUpdateSet();
                    mEnergyEmit.RemoveFromAutoDrawSet();
                }
                // uses the player shadow at point of impact to position effect
                mEnergyEmit = new XNACS1ParticleEmitter.FireEmitter(mShadow.Center, 30, 0.5f, "test", Color.CornflowerBlue, 20f, 4f, new Vector2(0, 5));
            }

            if (amount + mHealth > mResetHealth)
            {
                mHealth = mResetHealth;
                
            }
            else
                mHealth += (int)amount;
        
        }

        public void AddEnergy(float amount)
        {
            if (mCurrentEnergy + amount > mMaxEnergy)
            {
                mCurrentEnergy = mMaxEnergy;
            }
            else
            {
                mCurrentEnergy += amount;
            }
        
        }
        /// <summary>
        /// Is the child of the game object a hero? 
        /// If it hits this (the hero class), it is true
        /// </summary>
        /// <returns>boolean true all the time</ret
        /// )urns>
        public override bool isHero()
        {
            return true;
        }

        /// <summary>
        /// Update for a game controller action
        /// </summary>
        /// <param name="con"></param>
        public void ControlUpdate(GameController con)
        {
            if (mAliveState == mAlive.alive)
            {
                UpdateMovement(con.LeftDir.X, con.LeftDir.Y);   // update position
                TryJumping(con.mAPress);        // check to see if we are jumping, then jump
                TryAttackPri(con.mXPress);      // check to see if we are attacking, then pri attack
                TryAttackSec(con.mBPress);      // check to see if we are attacking, then sec attack
                //TryAttackAOE(con.mYPress);      // check to see if we are attacking, then tri attack
                TryAttackTri(con.mYPress);
                
                //else
                  //  RequestLife = false;
            }
            if (con.mBackPress)
                RequestLife = true;
            else
                RequestLife = false;
        }

        public void ReviveHero()
        {
            mAliveState = mAlive.alive;
            MakeActive();
            AddEnergy(100);
            Addhealth(100);
            mShadow.AddToAutoDrawSet();
            SetInitialPosition(XNACS1Base.World.WorldMin.X + (XNACS1Base.World.WorldDimension.X / 2), XNACS1Base.World.WorldMin.Y + (XNACS1Base.World.WorldDimension.Y / 2));
        }

        /// <summary>
        /// After movement is made, make sure we have the right texture on
        /// the hero
        /// </summary>
        public void UpdateTextures()
        {
            switch (mLookState)
            {
                case looking.lookLeft:
                    switch (mJumpState)
                    {
                        #region jumping, looking left
                        case jump.jumping:
                            if (IsAttacking())
                                Texture = mAttackJumpLeft;
                            else
                                Texture = mJumpingLeft;
                            break;
                        #endregion
                        #region not jumping, looking left
                        case jump.notJump:
                            if (IsMovingLeft())
                            {
                                if (mWalkingCounter <= 5)
                                {
                                    if (IsAttacking())
                                        Texture = mAttackWalkingLeft_01;
                                    else
                                        Texture = mWalkingLeft_01;
                                }
                                else if (mWalkingCounter <= 10)
                                {
                                    if (IsAttacking())
                                        Texture = mAttackWalkingLeft_02;
                                    else
                                        Texture = mWalkingLeft_02;

                                }
                                mWalkingCounter++;
                                if (mWalkingCounter == 10)
                                    mWalkingCounter = 0;
                            }
                            if (IsNotMoving())
                            {
                                if (IsAttacking())
                                    Texture = mAttackLeft;
                                else
                                    Texture = mStandingLeft;
                            }
                            break; // not jumping
                        #endregion
                    }
                    break; // look left

                case looking.lookRight:
                    switch (mJumpState)
                    {
                        #region jumping, looking right
                        case jump.jumping:
                            if (IsAttacking())
                                Texture = mAttackJumpRight;
                            else
                                Texture = mJumpingRight;
                            break;
                        #endregion
                        #region not jumping, looking right
                        case jump.notJump:
                            if (IsMovingRight())
                            {
                                if (mWalkingCounter <= 5)
                                {
                                    if (IsAttacking())
                                        Texture = mAttackWalkingRight_01;
                                    else
                                        Texture = mWalkingRight_01;
                                }
                                else if (mWalkingCounter <= 10)
                                {
                                    if (IsAttacking())
                                        Texture = mAttackWalkingRight_02;
                                    else
                                        Texture = mWalkingRight_02;

                                }
                                mWalkingCounter++;
                                if (mWalkingCounter == 10)
                                    mWalkingCounter = 0;
                            }
                            if (IsNotMoving())
                                if (IsAttacking())
                                    Texture = mAttackRight;
                                else
                                    Texture = mStandingRight;
                            break;
                        #endregion
                    }
                    break; // look right
            }
            UpdateShadow(); // move the shadow in the right place
        }

        private void CheckHealth()
        {
            if (mHealth <= 0)
            {
               
                if (BlinkTimer <= 0)
                {

                    if (NumberOfBlinks <= 0)
                    {
                        MakeNotActive();
                        Dead();
                        
                        mAliveState = mAlive.notAlive;
                    }
                    else
                    {
                        //change the visibility
                        if (Visible)
                            Visible = false;
                        else
                            Visible = true;

                        BlinkTimer = BlinkRate;
                        NumberOfBlinks--;
                    }
                }
                else
                BlinkTimer--;
               

            }
        }       

        /// <summary>
        /// Try to attack if there is a button pressed.
        /// </summary>
        /// <param name="buttonXPressed"></param>
        public virtual void TryAttackPri(bool buttonXPressed)
        {
            //i want to attack and am able to
            if (buttonXPressed && mCurrentPriAttackDelay <= 0 && UseEnergy(mPriEnergyCost))
            {
                mCurrentPriAttackDelay = mPriAttackDelay;
                mPriAttackState = attack.attacking;

                // critial shots
                if (XNACS1Base.RandomInt(0, 4) == 2 && !critLast)
                {
                    critLast = !critLast;
                    AttackCrit();
                }
                else
                {
                    critLast = !critLast;
                    AttackPri();
                }
            }//i am currently attacking
            else// if (currentattackDelay >= 0)
            {
                mCurrentPriAttackDelay--;
                if (mCurrentPriAttackDelay <= (mPriAttackDelay/2))
                    mPriAttackState = attack.notAttack;
            }
            //else
                
        }
        public void TryAttackSec(bool buttonBPressed)
        {
            //i want to attack and am able to
            if (buttonBPressed && mCurrentSecAttackDelay <= 0 && UseEnergy(mSecEnergyCost))
            {
                mCurrentSecAttackDelay = mSecAttackDelay;
                mSecAttackState = attack.attacking;
                AttackSec();
            }//i am currently attacking
            else// if (currentattackDelay >= 0)
            {
                mCurrentSecAttackDelay--;
                if (mCurrentSecAttackDelay <= (mSecAttackDelay / 2))
                    mSecAttackState = attack.notAttack;
            }
            //else

        }
        public virtual void TryAttackTri(bool buttonYPressed)
        {
            //i want to attack and am able to
            if (buttonYPressed && mCurrentTriAttackDelay <= 0 && CheckEnergy(mTriEnergyCost))
            {
                mCurrentTriAttackDelay = mTriAttackDelay;
                mTriAttackState = attack.attacking;
                AttackTri();
            }//i am currently attacking
            else// if (currentattackDelay >= 0)
            {
                mCurrentTriAttackDelay--;
                if (mCurrentTriAttackDelay <= (mTriAttackDelay / 2))
                    mTriAttackState = attack.notAttack;
            }
            //else
        }
        public void TryAttackAOE(bool buttonYPressed)
        {
            //i want to attack and am able to
            if (buttonYPressed && mCurrentAOEAttackDelay <= 0 && UseEnergy(mAOEEnergyCost))
            {
                mCurrentAOEAttackDelay = mAOEAttackDelay;
                mAOEAttackState = attack.attacking;
                AttackAOE();
            }//i am currently attacking
            else// if (currentattackDelay >= 0)
            {
                mCurrentAOEAttackDelay--;
                if (mCurrentAOEAttackDelay <= (mAOEAttackDelay / 2))
                    mAOEAttackState = attack.notAttack;
            }
            //else
        }

        /// <summary>
        /// When we are attacking, pick out the next shot, and shoot it
        /// in the right direction
        /// </summary>
        protected void AttackPri()
        {
            //retrive the next shot from the attack manager
            PriShot attack = mAttacks.GetNextNegPri();
            attack.Texture = mAttackPri;
            attack.setRange(8);
            XNACS1Base.PlayACue("herofire");
            if(mLookState == looking.lookLeft)
                attack.Fire(mPosition.X - Width /2 , mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this,mPriShootRange, mPriShootDamage );
            else
                attack.Fire(mPosition.X + Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this,mPriShootRange, mPriShootDamage);
           
        }
        protected void AttackCrit()
        {
            //retrive the next shot from the attack manager
            CritShot attack = mAttacks.GetNextNegCrit();
            attack.Texture = mAttackCrit;
            attack.setRange(8);
            XNACS1Base.PlayACue("herofire");

            if (mLookState == looking.lookLeft)
                attack.Fire(mPosition.X - Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, mCritShootRange, mCritShootDamage);
            else
                attack.Fire(mPosition.X + Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, mCritShootRange, mCritShootDamage);

        }
        protected void AttackSec()
        {

            //retrive the next shot from the attack manager
            SecShot attack = mAttacks.GetNextNegSec();
            attack.Texture = mAttackSec;
            attack.setRange(8);
            XNACS1Base.PlayACue("herofire");

            if (mLookState == looking.lookLeft)
                attack.Fire(mPosition.X - Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, mSecShootRange, mSecShootDamage);
            else
                attack.Fire(mPosition.X + Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, mSecShootRange, mSecShootDamage);

        }
        /// <summary>
        /// When we are attacking, pick out the next shot, and shoot it
        /// in the right direction
        /// </summary>
        /// 
        protected virtual void AttackTri()
        {
            //retrive the next shot from the attack manager
            
        }
        protected virtual void AttackSpecial()
        {
        }
        protected virtual void AttackSpecial(int count)
        {
        }
        public virtual bool IsAttackSpecial()
        {
            return mIsAttackSpecial;
        }
        protected virtual void AttackSpecialUpdate()
        {
        }

        protected void AttackAOE()
        {
            //retrive the next shot from the attack manager
            AOEShot attack = mAttacks.GetNextNegAOE();
            attack.Texture = mAttackAOE;
            attack.setRange(6);

            if (mLookState == looking.lookLeft)
                attack.Fire(mPosition.X - Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, mAOEShootRange, mAOEShootDamage);
            else
                attack.Fire(mPosition.X + Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (mLookState == looking.lookRight), this, mAOEShootRange, mAOEShootDamage);

        }

        protected bool UseEnergy(float amount)
        {
            if (mCurrentEnergy - amount >= 0)
            {
                mCurrentEnergy -= amount;
                return true;
            
            }
            return false;
        
        
        }
        protected bool CheckEnergy(float amount)
        {
            if (mCurrentEnergy - amount >= 0)
            {
                return true;

            }
            return false;


        }

        /// <summary>
        /// Check to see if we are attacking. True if attacking
        /// </summary>
        /// <returns></returns>
        private bool IsAttacking()
        {
            return (mPriAttackState == attack.attacking || mSecAttackState == attack.attacking);
        }

        public float GetPercentHealth()
        {
            return (float)(mHealth) / (float)(mResetHealth);
        }
        public float GetPercentEnergy()
        {
            return (float)(mCurrentEnergy / mMaxEnergy);
        }
        public void UpdateHealth(int decrease)
        {
            if (!mImmune)
            {
                // set tint-changing values
                tintChanged = true;
                mTintingColor = Color.DarkRed;
                tintTimer = 5;

                mHealth += decrease;
            }
            //XNACS1Base.PlayACue("hit");
        }

        /// <summary>
        /// UpdateTint: Controls tint changes related to object collisions with hero.
        /// If the tint has already been changed and the timer runs down it will remove
        /// the tint from the texture. Also increments the timer on the tint change.
        /// Note: The trigger for the tintchange happens in UpdateHealth
        /// </summary>
        private void UpdateTint()
        {
            if (tintChanged)
            {
                // continue counting how long the texturetint has been effected
                tintTimer--;

                // if the tint color hasn't been changed, change it
                if (TextureTintColor == Color.White)
                    TextureTintColor = mTintingColor;

                // if tinting is over reset to normal state
                if (tintTimer == 0)
                {
                    tintChanged = false;
                    TextureTintColor = Color.White;
                }
            }
        }

        private void UpdateEmit()
        {
            // update emitter vars for health emitter
            if (healthEmitting)
            {
                // count down
                mHealthEmitTimer--;
                // clean up if timer is up
                if (mHealthEmitTimer == 0)
                {
                    healthEmitting = false;
                    if (mHealthEmit != null)
                    {
                        mHealthEmit.RemoveEmitterFromUpdateSet();
                        mHealthEmit.RemoveFromAutoDrawSet();
                    }
                    mHealthEmit = null;
                }
            }
            // update emitter vars for energy emitter
            if (energyEmitting)
            {
                // count down
                mEnergyEmitTimer--;
                // clean up if timer is up
                if (mEnergyEmitTimer == 0)
                {
                    energyEmitting = false;
                    if (mEnergyEmit != null)
                    {
                        mEnergyEmit.RemoveEmitterFromUpdateSet();
                        mEnergyEmit.RemoveFromAutoDrawSet();
                    }
                    mEnergyEmit = null;
                }
            }
        }

        public virtual void Dead()
        {
            mCurrentEnergy = 0;
            mHealth = 0;
            RemoveFromAutoDrawSet();
            mShadow.RemoveFromAutoDrawSet();
            MakeNotActive();
            mIsAttackSpecial = false;
        }

        public void CollideDamage()
        {
            if (!mImmune)
            {
                mHealth -= 1;
                tintChanged = true;
                mTintingColor = Color.DarkRed;
                tintTimer = 5;
            }
        }

        public void PushBack()
        {
            SetImmune();
            

        }
    }
}

