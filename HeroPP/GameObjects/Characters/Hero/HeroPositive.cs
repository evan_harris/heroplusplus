﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    class HeroPositive : Hero
    {
        public HeroPositive()
        {
            mPriShootDamage = 1;
            mCritShootDamage = 3;
            mSecShootDamage = -1;
            mTriShootDamage = 2;
            mAOEShootDamage = 1;

          
            #region textures for the positive hero
            mStandingRight =     "pos_RN_standing";
            mStandingLeft =      "pos_LN_standing";
            mJumpingRight =      "pos_RN_jumping";
            mJumpingLeft =       "pos_LN_jumping";
            mWalkingLeft_01 =    "pos_LN_walking_01";
            mWalkingLeft_02 =    "pos_LN_walking_02";
            mWalkingRight_01 =   "pos_RN_walking_01";
            mWalkingRight_02 =   "pos_RN_walking_02";
            mAttackLeft =        "pos_LS_standing";
            mAttackRight =       "pos_RS_standing";
            mAttackWalkingLeft_01 =  "pos_LS_walking_01";
            mAttackWalkingLeft_02 =  "pos_LS_walking_02";
            mAttackWalkingRight_01 = "pos_RS_walking_01";
            mAttackWalkingRight_02 = "pos_RS_walking_02";
            mAttackJumpLeft =    "pos_LS_jumping";
            mAttackJumpRight =   "pos_RS_jumping";
            mAttackPri = "pos_shot";
            mAttackCrit = "shot3_red";
            mAttackSec = "neg_shot";
            mAttackTri = "shot3_red";
            #endregion

            #region hero stuff
            Texture = mStandingRight;
            Width = 12 * .85f;
            Height = 15 * .85f;
            mCenterToBot = Height / 2;

            CenterX = 10;
            CenterY = 10;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 4.5f;
            mShadow.Radius = mShadowRadius;
            #endregion
        }
        protected override void AttackTri()
        {
            
            //retrive the next shot from the attack manager
            XNACS1Base.PlayACue("shotSpecial");
            TriShot attack = mAttacks.GetNextNegTri();
            float shootspeed = 1;
            float degree = 0;
            
            attack.setRange(6);
            //shooting right
            while (degree <= 360)
            {
                attack = mAttacks.GetNextNegTri();
                degree = degree + 10;
                attack.Fire(mPosition.X - Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (float)Math.Cos(degree) * shootspeed, (float)Math.Sin(degree) * shootspeed, this, mTriShootRange, mTriShootDamage, mMaxZ);

        
            }
        }
        public override void TryAttackTri(bool buttonYPressed)
        {
            
            //i want to attack and am able to
            if (buttonYPressed && mCurrentTriAttackDelay <= 0 && UseEnergy(mTriEnergyCost))
            {
                UpdateHealth(-10);//health decrease per shot
                mCurrentTriAttackDelay = mTriAttackDelay;
                mTriAttackState = attack.attacking;
                AttackTri();
            }//i am currently attacking
            else// if (currentattackDelay >= 0)
            {
                mCurrentTriAttackDelay--;
                if (mCurrentTriAttackDelay <= (mTriAttackDelay / 2))
                    mTriAttackState = attack.notAttack;
            }
            //else
        }
    }
}
