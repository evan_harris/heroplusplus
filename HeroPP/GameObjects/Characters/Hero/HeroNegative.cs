﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNACS1Lib;
namespace Hero_Namespace
{
    class HeroNegative : Hero
    {
        private XNACS1Rectangle mCounter;
        private int mSlowDownCounter = -1;
        private bool mTriShotMade = false;

        public HeroNegative()
        {
            mPriShootDamage = -1;
            mCritShootDamage = -3;
            mSecShootDamage = -1;
            mTriShootDamage = -3;
            mAOEShootDamage = -1;
            mTriEnergyCost = 98;

            #region textures for the negative hero
            mStandingRight = "neg_RN_standing";
            mStandingLeft = "neg_LN_standing";
            mJumpingRight = "neg_RN_jumping";
            mJumpingLeft = "neg_LN_jumping";
            mWalkingLeft_01 = "neg_LN_walking_01";
            mWalkingLeft_02 = "neg_LN_walking_02";
            mWalkingRight_01 = "neg_RN_walking_01";
            mWalkingRight_02 = "neg_RN_walking_02";
            mAttackLeft = "neg_LS_standing";
            mAttackRight = "neg_RS_standing";
            mAttackWalkingLeft_01 = "neg_LS_walking_01";
            mAttackWalkingLeft_02 = "neg_LS_walking_02";
            mAttackWalkingRight_01 = "neg_RS_walking_01";
            mAttackWalkingRight_02 = "neg_RS_walking_02";
            mAttackJumpLeft = "neg_LS_jumping";
            mAttackJumpRight = "neg_RS_jumping";
            mAttackPri = "neg_shot";
            mAttackSec = "neg_shot";
            mAttackTri = "shot3_blue";
            mAttackCrit = "shot3_blue";
            
            #endregion

            #region hero stuff
            Texture = mStandingRight;
            Width = 12 * .85f;
            Height = 15 * .85f;
            mCenterToBot = Height / 2;

            CenterX = 10;
            CenterY = 10;
            mPosition.X = CenterX;
            mPosition.Y = 0;
            mPosition.Z = CenterY - mCenterToBot;
            
            #endregion

            #region shadow stuff
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;
            mShadow.Texture = mShadowTexture;
            mShadowRadius = 4.5f;
            mShadow.Radius = mShadowRadius;
            #endregion

            mCounter = new XNACS1Rectangle();
            mCounter.CenterX = CenterX;
            mCounter.CenterY = CenterY + 8;
            mCounter.Width = 0;
            mCounter.Height = 0;
            mCounter.LabelColor = Microsoft.Xna.Framework.Color.White;
        }
        public override void TryAttackTri(bool buttonYPressed)
        {

            //i want to attack and am able to
            if (buttonYPressed && mCurrentTriAttackDelay <= 0 && UseEnergy(mTriEnergyCost))
            {
                UpdateHealth(-10);//health decrease per shot
                mCurrentTriAttackDelay = mTriAttackDelay;
                mTriAttackState = attack.attacking;
                AttackTri();
            }//i am currently attacking
            else// if (currentattackDelay >= 0)
            {
                mCurrentTriAttackDelay--;
                if (mCurrentTriAttackDelay <= (mTriAttackDelay / 2))
                    mTriAttackState = attack.notAttack;
            }
            //else
        }
     
        protected override void AttackTri()
        {
            //retrive the next shot from the attack manager
            XNACS1Base.PlayACue("shotSpecial");
            TriShot attack;
            float shootspeed = 1;
            float degree = 0;

           
            //shooting right
            while (degree <= 360)
            {
                attack = mAttacks.GetNextNegTri();
                attack.Texture = mAttackTri;
                attack.setRange(6);
                degree = degree + 10;
                attack.Fire(mPosition.X - Width / 2, mPosition.Y + mPosition.Z + mCenterToBot - 2.8f, mPosition.Z, (float)Math.Cos(degree) * shootspeed, (float)Math.Sin(degree) * shootspeed, this, mTriShootRange, mTriShootDamage, mMaxZ);


            }
        }
        public override void Update()
        {
            base.Update();
            mCounter.CenterX = CenterX;
            mCounter.CenterY = CenterY + 8;
            if (mSlowDownCounter > 0)
            {
                int count = mSlowDownCounter / 40;
                mCounter.Label = "Slowz Timer: " + count;
            }
            else
            {
                mCounter.Label = ""; 
            }
        }
        public override void BringToTop()
        {
            base.BringToTop();
            mCounter.TopOfAutoDrawSet();
        }
        public override void Dead()
        {
            mCounter.Label = "";
            mSlowDownCounter = 0;
            mCurrentEnergy = 0;
            mHealth = 0;
            RemoveFromAutoDrawSet();
            mShadow.RemoveFromAutoDrawSet();
            MakeNotActive();
            mIsAttackSpecial = false;
        }
    }
}
