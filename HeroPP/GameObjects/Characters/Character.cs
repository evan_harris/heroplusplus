﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

// ----------------------------------------------------------------------------
// Class:   Character
// A Character is derived from GameObject, GameObject is derived from 
// a Rectangle.
// ----------------------------------------------------------------------------

namespace Hero_Namespace
{
    abstract class Character : GameObject
    {
        #region position variables
        protected Vector3 mInitPos;  // keep track of init position before updating
        protected Vector3 mMovePos;  // move vector
        #endregion

        #region variables of where the character can move
        public float mMinZ = 3;      // these are set elsewhere
        public float mMaxZ = 40;
        #endregion

        #region attack variables for all characters
        protected AttackManager mAttacks;
        #endregion

        #region states of the character
        public enum jump  { notJump, jumping };         // is the character jumping
        public jump mJumpState { get; set; }

        public enum moveHoriz { moveLeft, moveRight, notMoving };   // move to the left and right
        public moveHoriz mMoveHorizState { get; set; }

        public enum moveVert { moveUp, moveDown, notMoving };       // move to the left and right
        public moveVert mMoveVertState { get; set; }

        public enum looking { lookLeft, lookRight };    // which direction is the character looking
        public looking mLookState { get; set; }
        protected int mWalkingCounter;
        #endregion

        #region character movement variables
        protected float mCenterToBot;        // distance from the center to the bottom
        protected float mInitVelocity = 2f;  // jump variable
        protected float mJumpVelocity;       // jump variable
        protected float mSpeed = 0.5f;       // running speed
        protected float mJumpHeight = 0.5f;
        protected bool mImmune = false;
        protected int mImmuneCounter = 80;
        #endregion

        #region character textures as strings
        protected String mStandingRight;
        protected String mStandingLeft;
        protected String mJumpingRight;
        protected String mJumpingLeft;
        protected String mWalkingLeft_01;
        protected String mWalkingLeft_02;
        protected String mWalkingRight_01;
        protected String mWalkingRight_02;
        #endregion

        #region shadow of the character
        public XNACS1Circle mShadow = new XNACS1Circle();
        protected String mShadowTexture = "shadow";
        protected float mShadowRadius;
        #endregion

        /// <summary>
        /// character constructor that sets up the states and sets the 
        /// position to all zero
        /// </summary>
        public Character()
        {
            #region set up the states of character
            mJumpState = jump.notJump;       // character is not jumping
            mLookState = looking.lookRight;  // character is looking right
            mMoveHorizState = moveHoriz.notMoving;   // character is not moving
            #endregion

            #region set up the character variables, all zero 
            mPosition = new Vector3(0f, 0f, 5f);
            mMovePos = new Vector3(0f, 0f, 0f);
            mCenterToBot = Height / 2;
            mJumpVelocity = 0;
            mWalkingCounter = 0;
            #endregion 
        }

        /// <summary>
        /// Used for camera control
        /// </summary>
        public void ClampCharacterWorldBound()
        {
            XNACS1Base.World.ClampAtWorldBound(mShadow);
            XNACS1Base.World.ClampAtWorldBound(this);
            this.mPosition.X = CenterX;
        }
        virtual public bool ObjCollided(Character rhs)
        {
            return rhs.mShadow.Collided(mShadow);
            
        }

        public override void BringToTop()
        {
            if(mShadow.IsInAutoDrawSet())
                mShadow.TopOfAutoDrawSet();
              base.BringToTop();
        
        }

        /// <summary>
        /// Set the initial position of the character per the x and z position
        /// Make sure we draw it in the right Center position and throw a shadow on
        /// the character
        /// </summary>
        /// <param name="posx">the float where the character will exist in x axis</param>
        /// <param name="posz">the float where the character will exist in z axis</param>
        public virtual void SetInitialPosition(float posx, float posz)
        {
            #region update the position
            mPosition.X = posx;                  
            mPosition.Z = posz;
            #endregion

            #region update the Center
            PositionCenter();
            #endregion

            UpdateShadow();
        
        }

        public override bool isCharacter()
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attack"></param>
        public void SetAttackMgr(AttackManager attack)
        {
            mAttacks = attack;
        }

        public abstract bool isHero();

        public void UpdateMovement(float MoveX, float MoveY)
        {

            MoveLeftRight(MoveX);
            MoveUpDown(MoveY);
            
        }
        
        /// <summary>
        /// Update the left and right position by a delta 
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void MoveLeftRight(float delta)
        {
            mInitPos.X = mPosition.X;
            mPosition.X += delta;
            if (delta > 0)
            {
                mMoveHorizState = moveHoriz.moveRight;
                mLookState = looking.lookRight;
            }
            else if (delta < 0)
            {
                mMoveHorizState = moveHoriz.moveLeft;
                mLookState = looking.lookLeft;
            }
            else
                mMoveHorizState = moveHoriz.notMoving;

        }
        /// <summary>
        /// Update the up and down position by a delta 
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void MoveUpDown(float delta)
        {
            mInitPos.Z = mPosition.Z;
            mPosition.Z += delta;
            if (Math.Abs(delta) > 0)
            {
                if (mLookState == looking.lookLeft)
                    mMoveHorizState = moveHoriz.moveLeft;
                else if (mLookState == looking.lookRight)
                    mMoveHorizState = moveHoriz.moveRight;
            }
            
        }
        /// <summary>
        /// Check to see if we need to move the character by making him jump
        /// </summary>
        /// <param name="buttonAPressed"></param>
        public void TryJumping(bool buttonAPressed)
        {
            if (buttonAPressed)
            {
                mJumpState = jump.jumping;
            }

            if (mJumpState == jump.jumping)
            {
                Jump();
            }
        }
        /// <summary>
        /// Jumping motion
        /// </summary>
        private void Jump()
        {
            if (mJumpVelocity == 0)
            {
                mJumpVelocity = mInitVelocity;
            }
            mPosition.Y += (float)(mJumpHeight*(Math.Sign(mJumpVelocity) * Math.Pow(mJumpVelocity, 2)));
            mJumpVelocity -= 0.15f;

            if (mJumpVelocity <= -mInitVelocity)
            {
                mJumpState = jump.notJump;
                mPosition.Y = 0;
                mJumpVelocity = 0;
            }

        }
        /// <summary>
        /// Bound the character to the world given a minimum and maximum coordinates
        /// (bottom left and top right part of the world)
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void KeepInWorld()
        {
            NormalizeMotion();

            if (mPosition.Z < mMinZ)
            {
                mPosition.Z = mMinZ;
                CenterY = mPosition.Z + mCenterToBot;
            }
            if (mPosition.Z > mMaxZ)
            {
                mPosition.Z = mMaxZ;
                CenterY = mPosition.Z + mCenterToBot;
            }
            PositionCenter();
        }
        /// <summary>
        /// Use the updated X and Z to normalize the ground motion
        /// </summary>
        private void NormalizeMotion()
        {
            mMovePos.X = mPosition.X - mInitPos.X;
            mMovePos.Z = mPosition.Z - mInitPos.Z;
            if (mMovePos.X > 0 && mMovePos.Z > 0)
                mMovePos.Normalize();
            mMovePos *= mSpeed;
            mPosition.X = mInitPos.X + mMovePos.X;
            mPosition.Z = mInitPos.Z + mMovePos.Z;
        }
        /// /// <summary>
        /// Update the character Center location
        /// </summary>
        protected void PositionCenter()
        {
            CenterX = mPosition.X;
            CenterY = mPosition.Z + mPosition.Y + mCenterToBot;
        }
        /// <summary>
        /// Update the character textures due to the movement
        /// </summary>



        protected bool IsMovingLeft()
        {
            return (mMoveHorizState == moveHoriz.moveLeft);
        }
        protected bool IsMovingRight()
        {
            return (mMoveHorizState == moveHoriz.moveRight);
        }
        protected bool IsNotMoving()
        {
            return (mMoveHorizState == moveHoriz.notMoving);
        }
        protected void UpdateShadow()
        {
            #region shadow update
            mShadow.CenterX = mPosition.X;
            mShadow.CenterY = mPosition.Z + 0.5f;

            //if (mJumpState == jump.jumping)
                mShadow.Radius = mShadowRadius * (1 - mPosition.Y * 0.025f);
            //else
                //mShadow.Radius = mShadowRadius;

            //XNACS1Base.EchoToTopStatus("x = " + CenterX + " y = " + CenterY);
            #endregion
        }

        public void SetImmune()
        {
            mImmune = true;
        }

        public void UndoImmune()
        {
            mImmune = false;
        }

        public bool IsImmune()
        {
            return mImmune;
        }

        public int ImmuneCounterLeft()
        {
            return mImmuneCounter;
        }

        public void DecrementImmuneCounter()
        {
            mImmuneCounter -= 1;
        }

        public void ResetImmuneCounter()
        {
            mImmuneCounter = 80;
        }
    }
}
