﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNACS1Lib;


namespace Hero_Namespace
{
    class PickEmit
    {
        protected XNACS1ParticleEmitter.FireEmitter mEmitter;

        protected float mFireHeight = 20f;
        protected float mFireWidth = 5f;
        protected int particleLife = 30;
        protected float particleSize = 0.5f;
        
        public void addParticles(Vector2 Center)
        {
            mEmitter = new XNACS1ParticleEmitter.FireEmitter(Center, 30, 0.5f, "test", Color.AliceBlue, 20f, 5f, new Vector2(0, 5));
        }
        public PickEmit(String texture)
        {
            Vector2 center = new Vector2(-20, -20);
        }
        public void Update(Vector2 move)
        {
            if (mEmitter.Visible)
            {
                mEmitter.Center = move;
                mEmitter.EmitFrequency = 20;
            }
        }
        public void RemoveParticle()
        {
            mEmitter.EmitFrequency = 0;
            mEmitter.CenterY = -20;
            mEmitter.CenterX = -20;
            mEmitter.Visible = false;

            mEmitter.RemoveFromAutoDrawSet();
            mEmitter = null;
        }
        public void MakeActive(Vector2 center, String texture, Color color)
        {
            mEmitter = new XNACS1ParticleEmitter.FireEmitter(center, 30, 0.5f, texture, color, 50f, 5f, new Vector2(0, 5));
            mEmitter.Visible = true;
        }

        public bool IsActive()
        {
            return (mEmitter != null);
        }
    }
}
