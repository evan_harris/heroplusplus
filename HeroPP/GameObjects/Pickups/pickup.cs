﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hero_Namespace
{
    class pickup : GameObject
    {
        protected float EnergyContents = 0;
        protected float HealthContents = 0;

        public pickup()
        { }

        public void GetPickup(Hero inhero)
        {
            MakeNotActive();
            inhero.AddEnergy(EnergyContents);
            inhero.Addhealth(HealthContents);
            
        }

    }
}
