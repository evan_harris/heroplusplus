﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNACS1Lib;

namespace Hero_Namespace
{
    static class CollisionMath
    {
        
        /// <summary>
        /// Object 1 collides with object 2. Move the center of the object 2 out
        /// </summary>
        /// <param name="obj1">the current object</param>
        /// <param name="obj2">the object colliding with current object</param>
        public static void Bounce(GameObject obj1, GameObject obj2)
        {
            float variableWidth = 0.3f;
            #region the object 1 is to the left object 2
            if (obj1.mPosition.X <= obj2.mPosition.X)
            {

                obj2.mPosition.X = obj1.mPosition.X + (obj1.Width + obj2.Width) * variableWidth;
                
            }
            #endregion
            #region the object 1 is to the right of object 2
            else // obj1.CenterX > obj2.CenterX
            {
                obj2.mPosition.X = obj1.mPosition.X - (obj1.Width + obj2.Width) * variableWidth;
            }
            #endregion
        }
        public static void Bounce2(GameObject obj1, GameObject obj2)
        {
            float variableWidth = 0.8f;
            #region the object 1 is to the left object 2
            if (obj1.mPosition.X <= obj2.mPosition.X)
            {

                obj2.mPosition.X = obj1.mPosition.X + (obj1.Width + obj2.Width) * variableWidth;

            }
            #endregion
            #region the object 1 is to the right of object 2
            else // obj1.CenterX > obj2.CenterX
            {
                obj2.mPosition.X = obj1.mPosition.X - (obj1.Width + obj2.Width) * variableWidth;
            }
            #endregion
        }

        static public void pushcharfromchar(Character char1, Character char2)
        {
            Vector2 wantdist = new Vector2();
            Vector2 circdirect = new Vector2();
            if (char1.mPosition.Y != char2.mPosition.Y)
                return;
           
            if (char1.mShadow.Center == char2.mShadow.Center)
                circdirect.Y = 1;
            else
                circdirect = (char1.mShadow.Center - char2.mShadow.Center);
 circdirect.Normalize();
            wantdist = (circdirect * (char1.mShadow.Radius + char2.mShadow.Radius));
            //change the center by the difference in the needed position and the have poisition
            char1.mPosition.X += ((wantdist.X - (char1.Center.X - char2.Center.X)) * 0.7f);
            char1.mPosition.Z += ((wantdist.Y - (char1.Center.Y - char2.Center.Y)) * 0.7f);

            
            char2.mPosition.X += (-(wantdist.X - (char1.Center.X - char2.Center.X)) * 0.7f);
            char2.mPosition.Z += (-(wantdist.Y - (char1.Center.Y - char2.Center.Y)) * 0.7f);


        }
    }
}
