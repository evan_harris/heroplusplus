﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XNACS1Lib;

namespace Hero_Namespace
{
    class GameController
    {
        private Vector2 mLeftDir, mRightDir; 

        public bool mAPress, mBPress, mXPress, mYPress,
                    mStartPress, mBackPress = false;

        public bool mAisDown, mBisDown, mXisDown, mYisDown,
            mStartisDown, mBackisDown = false;
        
        
        public GameController()
        {
            mLeftDir = new Vector2();
            mRightDir = new Vector2();
        }

        public GameController Copy()
        {
            GameController conCopy = new GameController();

            conCopy.mLeftDir = mLeftDir;
            conCopy.mRightDir = mRightDir;
            conCopy.mAPress = mAPress;
            conCopy.mBPress = mBPress;
            conCopy.mXPress = mXPress;
            conCopy.mYPress = mYPress;
            conCopy.mStartPress = mStartPress;
            conCopy.mBackPress = mBackPress;
            conCopy.mAisDown = mAisDown;
            conCopy.mBisDown = mBisDown;
            conCopy.mXisDown = mXisDown;
            conCopy.mYisDown = mYisDown;
            conCopy.mStartisDown = mStartisDown;
            conCopy.mBackisDown = mBackisDown;

            return conCopy;
        }

        public void Update(Microsoft.Xna.Framework.Input.GamePadState controller)
        {          
                mLeftDir = controller.ThumbSticks.Left;
          
                mRightDir = controller.ThumbSticks.Right;

                if (mAPress && controller.IsButtonDown(Buttons.A))
                    mAPress = false;
                else
               mAPress = controller.IsButtonDown(Buttons.A);

                if (mBPress && controller.IsButtonDown(Buttons.B))
                    mBPress = false;
                else
                    mBPress = controller.IsButtonDown(Buttons.B);

                if (mXPress && controller.IsButtonDown(Buttons.X))
                    mXPress = false;
                else
                    mXPress = controller.IsButtonDown(Buttons.X);

                if (mYPress && controller.IsButtonDown(Buttons.Y))
                    mYPress = false;
                else
                    mYPress = controller.IsButtonDown(Buttons.Y);         
          
            mStartPress = controller.IsButtonDown(Buttons.Start);
            mBackPress = controller.IsButtonDown(Buttons.Back);

            mAisDown = controller.IsButtonDown(Buttons.A);
            mBisDown = controller.IsButtonDown(Buttons.B);
            mXisDown = controller.IsButtonDown(Buttons.X);
            mYisDown = controller.IsButtonDown(Buttons.Y);
            mStartisDown = controller.IsButtonDown(Buttons.Start);
            mBackisDown = controller.IsButtonDown(Buttons.Back);
        }

        public void Update(Microsoft.Xna.Framework.Input.KeyboardState controller)
        {
            // fake the thumbsticks if left and right are pressed at the same time it = 0
            int pushedDown = 0;
            pushedDown += (controller.IsKeyDown(Keys.A)) ? -1 : 0;
            pushedDown += (controller.IsKeyDown(Keys.D)) ? 1 : 0;
            mLeftDir.X = pushedDown;

            pushedDown = 0;
            pushedDown += (controller.IsKeyDown(Keys.S)) ? -1 : 0;
            pushedDown += (controller.IsKeyDown(Keys.W)) ? 1 : 0;
            mLeftDir.Y = pushedDown;
            
            pushedDown = 0;
            pushedDown += (controller.IsKeyDown(Keys.Left)) ? -1 : 0;
            pushedDown += (controller.IsKeyDown(Keys.Right)) ? 1 : 0;
            mRightDir.X = pushedDown;

            pushedDown = 0;
            pushedDown += (controller.IsKeyDown(Keys.Down)) ? -1 : 0;
            pushedDown += (controller.IsKeyDown(Keys.Up)) ? 1 : 0;
            mRightDir.Y = pushedDown;

            if (mAPress && controller.IsKeyDown(Keys.K))
                mAPress = false;
            else
                mAPress = controller.IsKeyDown(Keys.K);

            if (mBPress && controller.IsKeyDown(Keys.L))
                mBPress = false;
            else
                mBPress = controller.IsKeyDown(Keys.L);

            if (mXPress && controller.IsKeyDown(Keys.J))
                mXPress = false;
            else
                mXPress = controller.IsKeyDown(Keys.J);

            if (mYPress && controller.IsKeyDown(Keys.I))
                mYPress = false;
            else
                mYPress = controller.IsKeyDown(Keys.I);
            
            mStartPress = controller.IsKeyDown(Keys.F2);
            mBackPress = controller.IsKeyDown(Keys.F1);

            mAisDown = controller.IsKeyDown(Keys.K);
            mBisDown = controller.IsKeyDown(Keys.L);
            mXisDown = controller.IsKeyDown(Keys.J);
            mYisDown = controller.IsKeyDown(Keys.I);
            mStartisDown = controller.IsKeyDown(Keys.F2);
            mBackisDown = controller.IsKeyDown(Keys.F2);
        }

        public Vector2 LeftDir
        {
            get { return mLeftDir; }
            set { mLeftDir = value; }
        }
        public Vector2 RightDir
        {
            get { return mRightDir; }
            set { mRightDir = value; }
        }

    
    }
}
