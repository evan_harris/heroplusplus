using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XNACS1Lib;
/*
AHH, hero++, the game to end all games...
 * its like the future of gaming even!


*/
namespace Hero_Namespace
{

    public class HeroPP : XNACS1Base
    {
        private GameManager herogame;
        private GameController control1;
        private GameController control2;

        protected override void InitializeWorld()
        {

            //controller 1 stuff
            control1 = new GameController();
            control2 = new GameController();

            XNACS1Base.SetAppWindowPixelDimension(true, 800, 600);
            float ViewWidth = XNACS1Lib.XNACS1Base.World.WorldDimension.X / XNACS1Lib.XNACS1Base.World.WorldDimension.Y * 65;
            World.SetWorldCoordinate(new Vector2(0, 0), ViewWidth);


            //make new game, set controller reference


            herogame = new GameManager(control1, control2, null, null);
            //use setup controllers to link herogame to controllers




        }

        protected override void UpdateWorld()
        {
            if (herogame.ShouldExit())
                this.Exit();
            //take controller inputs and send them down to the game manager
            //links controller with controller 1

            // Use the keyboard for PC or the keyboard for XBOX
            control1.Update(Microsoft.Xna.Framework.Input.GamePad.GetState(PlayerIndex.One));
            control2.Update(Microsoft.Xna.Framework.Input.GamePad.GetState(PlayerIndex.Two));

            //update game manager
            //The update will run the entire game
            herogame.Update();
        }
        //------------------private working functions---------------------------
    }
}
